<?php

namespace App\Http\Controllers\admin;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnswersController extends Controller
{
    public function index($question_id)
    {
        $question=Question::find($question_id);
        $answers=Answer::where('question_id',$question_id)->get();

        return view('admin.answers.index',compact('answers','question'));
    }
    public function show($question_id,$id)
    {
        $question=Question::find($question_id);
        $answer=Answer::find($id);

        return view('admin.answers.show',compact('question','answer'));
    }
    public function destroy($question_id,$id)
    {
        $answer=Answer::where('id',$id)->first();

        $answer->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));
    }
}
