@extends('admin.layouts.app')

@section('title')
     تفاصيل عضو
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">Home</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/users')}}" class="m-menu__link">
            <span class="m-menu__link-text">الاعضاء</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تفاصيل عضو</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                       تفاصيل عضو
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($user,['route' => ['users.show' , $user->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">

{{--            @include('admin.users.form')--}}

            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">الاسم: </label>
                <div class="col-lg-10{{ $errors->has('f_name') ? ' has-danger' : '' }}">
                    {!! Form::text('fullName',old('fullName'),['class'=>'form-control m-input','autofocus','disabled','placeholder'=> 'الاسم' ]) !!}

                </div>

            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">اسم المستخدم: </label>
                <div class="col-lg-10{{ $errors->has('l_name') ? ' has-danger' : '' }}">
                    {!! Form::text('userName',old('userName'),['class'=>'form-control m-input','disabled','placeholder'=> 'اسم المستخدم' ]) !!}

                </div>

            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">البريد الالكتروني: </label>
                <div class="col-lg-10{{ $errors->has('email') ? ' has-danger' : '' }}">
                    {!! Form::text('email',old('email'),['class'=>'form-control m-input','disabled','placeholder'=> 'البريد الالكترونى' ]) !!}
                    @if ($errors->has('email'))
                        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
                    @endif
                </div>

            </div>
            @if($user->photo)
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">Profile Picture: </label>
                <div class="col-lg-10{{ $errors->has('photo') ? ' has-danger' : '' }}">
                    <img
                            data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                            alt=""
                            src="{{$user->photo}}"
                            style="height: 150px; width: 150px"
                            data-holder-rendered="true">


                </div>

            </div>
            @endif






        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

