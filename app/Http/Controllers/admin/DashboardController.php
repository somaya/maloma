<?php

namespace App\Http\Controllers\admin;


use App\Models\Answer;
use App\Models\Category;
use App\Models\Question;
use App\Models\Report;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $questions=Question::all()->count();
        $answers=Answer::all()->count();
        $users=User::where('role',0)->count();
        $admins=User::where('role',1)->count();
        $reports=Report::all()->count();
        $categories=Category::all()->count();
        return view('admin.index',compact('questions','answers','users','admins','reports','categories'));
    }
}
