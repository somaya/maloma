<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::group(['middleware'=>['cors']],function() {
Route::post('/register', 'HomeController@register');
Route::post('/login', 'HomeController@login');
Route::post('/verifyUser', 'HomeController@verifyUser');


Route::get('/logout','HomeController@logoutApi');
Route::post('/facebook', 'HomeController@facebook');
Route::post('/twitter', 'HomeController@twitter');
Route::post('/google', 'HomeController@google');
Route::post('/getResetToken', 'HomeController@getResetToken');
Route::post('/checkResetToken', 'HomeController@checkResetToken');
Route::post('/reset', 'HomeController@reset');
Route::post('/updatePassword', 'HomeController@updatePassword');
Route::post('/editUserInfo', 'HomeController@editUserInfo');
Route::get('/getUserInfo', 'HomeController@getUserInfo');
Route::get('/getUserProfile/{id}', 'HomeController@getUserProfile');
Route::get('/turnOnOffNotification', 'HomeController@turnOnOffNotification');
Route::get('/aboutUs', 'HomeController@aboutUs');
//lists
Route::get('/getCountries', 'HomeController@getCountries');
Route::get('/getEducationDegrees', 'HomeController@getEducationDegrees');
Route::get('/getCategories', 'HomeController@getCategories');
Route::get('/getCity/{id}', 'HomeController@getCity');

Route::get('/questions', 'HomeController@questions');
//Route::get('/questions', 'HomeController@questions');
Route::post('/questions/create', 'HomeController@addQuestion');
Route::post('/addReport', 'HomeController@addReport');

Route::get('/categoryQuestions/{id}', 'HomeController@categoryQuestions');
//Route::get('/categoryQuestions/{id}', 'HomeController@categoryQuestions');

Route::get('/question/{id}', 'HomeController@questionDetails');
//Route::get('/question/{id}', 'HomeController@questionDetails');

Route::post('/search', 'HomeController@search');
Route::post('/filter', 'HomeController@filter');

Route::get('/replies/{id}', 'HomeController@replies');

Route::get('/myQuestions', 'HomeController@myQuestions');
Route::delete('/myQuestions/all', 'HomeController@deleteAllQuestions');
Route::delete('/myQuestions/{id}', 'HomeController@deleteOneQuestion');

Route::get('/saves', 'HomeController@saves');
Route::delete('/saves/all', 'HomeController@deleteAllSaves');
Route::post('/saves/create', 'HomeController@addToSaves');

Route::post('/favourites/create', 'HomeController@addTofav');

Route::get('/myAnswers', 'HomeController@myAnswers');
Route::delete('/myAnswers/all', 'HomeController@deleteAllAnswers');
Route::delete('/myAnswers/{id}', 'HomeController@deleteOneAnswer');
Route::post('/myAnswers/create', 'HomeController@addAnswer');
Route::post('/replies/create', 'HomeController@addReply');
Route::post('/uploadFile', 'HomeController@uploadFile');

Route::get('/myDrafts', 'HomeController@myDrafts');
Route::delete('/myDrafts/all', 'HomeController@deleteAllDrafts');
Route::post('/myDrafts/create', 'HomeController@publishDraft');

Route::post('/sendMessage', 'HomeController@sendMessage');
Route::get('/getMyChats', 'HomeController@getMyChats');
Route::delete('/deleteAllChats', 'HomeController@deleteAllChats');
Route::delete('/deleteOneChat/{id}', 'HomeController@deleteOneChat');
Route::get('/getChatDetails/{id}', 'HomeController@getMessageDetails');

//notification
Route::get('/notifications', 'HomeController@notifications');
Route::get('/unreadNotifications', 'HomeController@unreadNotifications');
Route::get('/deleteNotification', 'HomeController@deleteNotification');
Route::get('/deleteOneNotification/{id}', 'HomeController@deleteOneNotification');
//});