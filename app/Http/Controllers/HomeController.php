<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\educationDegree;
use App\Models\Like;
use App\Models\Media;
use App\Models\Message;
use App\Models\MobileData;
use App\Models\PasswordReset;
use App\Models\Question;
use App\Models\QuestionCity;
use App\Models\Reply;
use App\Models\Report;
use App\Models\Save;
use App\Models\Setting;
use App\Models\userCategory;
use App\Notifications\answerQuestion;
use App\Notifications\likeAnswer;
use App\Notifications\likeQuestion;
use App\Notifications\sendMessage;
use App\User;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Notification;

class HomeController extends Controller
{
    public function register(Request $request)
    {
        if (!$request->userName)
            return response()->json(['status'=>404,'error' => 'userName required']);
        if (!$request->fullName)
            return response()->json(['status'=>404,'error' => 'full name required']);
//        if (!$request->phone)
//            return response()->json(['status'=>404,'error' => 'phone required']);
        if (!$request->email)
            return response()->json(['status'=>404,'error' => 'email required']);
        if (!$request->password)
            return response()->json(['status'=>404,'error' => 'password required']);
        if (!$request->gender)
            return response()->json(['status'=>404,'error' => 'gender required']);
        if (!$request->birthDate)
            return response()->json(['status'=>404,'error' => 'birthDate required']);
        if (!$request->country_id)
            return response()->json(['status'=>404,'error' => 'country_id required']);
        if (!$request->city_id)
            return response()->json(['status'=>404,'error' => 'city_id required']);
        if (!$request->password)
            return response()->json(['status'=>404,'error' => 'password required']);
        if (!$request->educationDegree_id)
            return response()->json(['status'=>404,'error' => 'educationDegree_id required']);
        if (!$request->specialization)
            return response()->json(['status'=>404,'error' => 'specialization required']);
        if (!$request->yearsOfExperience)
            return response()->json(['status'=>404,'error' => 'yearsOfExperience required']);
        if (!$request->categories)
            return response()->json(['status'=>404,'error' => 'categories required']);

        $oldemail=User::where('email',$request->email)->first();
        if($oldemail){
            return response()->json(['status'=>404,'error'=>'Email Already Exist']);
        }
        $digits=4;
        $verify=rand(pow(10, $digits-1), pow(10, $digits)-1);
        $user=new User();
        $user->userName=$request->userName;
        $user->fullName=$request->fullName;
        $user->phone=$request->phone;
        $user->email=$request->email;
        $user->gender=$request->gender;
        $user->country_id=$request->country_id;
        $user->city_id=$request->city_id;
        $user->birthDate=date('Y-m-d', strtotime($request->birthDate));
        $user->educationDegree_id=$request->educationDegree_id;
        $user->specialization=$request->specialization;
        $user->yearsOfExperience=$request->yearsOfExperience;
        $user->aboutYourSelf=$request->aboutYourSelf;
        $user->password=bcrypt($request->password);
        $user->token=bin2hex(random_bytes(20));
        $user->activeCode=$verify;
        $user->save();
        foreach ($request->categories as $category){
            userCategory::create([
                'user_id'=>$user->id,
                'category_id'=>$category

            ]);
        }


        $data = [
            'name' => $user->fullName,
            'subject' => 'كود تأكيد حسابك',
            'verify' => $verify
        ];

        Mail::send('emails.verify', $data, function ($message) use ($user) {
            $message->from('info@m3lomhapp.com', 'verify@maloma')
                ->to($user->email)
                ->subject('Verify Email, maloma App');
        });




        return response()->json(['status'=>200,'success' =>'Registered Successfully']);



    }
    public function verifyUser(Request $request){
        $token=$request->code;
        $verifyUser = User::where('activeCode', $token)->first();
        if($verifyUser){

            $verifyUser->email_verified_at=Carbon::now();;
            $verifyUser->save();


            return response()->json(['status'=>200,'success' => 'your account verified successfully']);
        }else{
            return response()->json(['status'=>404,'error' => 'invalid token']);
        }
    }

    private function receiveData($user_id, $mac,$token)
    {
        if (!$user_id)
            return response()->json(['status'=>404,'error' => 'user_id required']);
        if (!$mac)
            return response()->json(['status'=>404,'error' => 'mac required']);
        if (!$token)
            return response()->json(['status'=>404,'error' => 'token required']);
        $user = User::where('id', $user_id)->first();
        if (!$user)
            return response()->json(['status'=>404,'error' => 'invalid user_id']);

        $old_phone_data = MobileData::where('mobile_mac', $mac)->first();
        if (!$old_phone_data) {
            MobileData::create([
                'mobile_mac'=>$mac,
                'mobile_token'=>$token,
                'user_id'=>$user->id
            ]);
        } else {
            $old_phone_data->mobile_token = $token;
            $old_phone_data->user_id = $user->id;
            $old_phone_data->save();
        }

    }
    public function login(Request $request){


        $user = User::where('email', $request->email)->with(['country'=>function($q){
            $q->select(['id','name']);

        }])->with(['city'=>function($q){
            $q->select(['id','name']);

        }])->with(['educationDegree'=>function($q){
            $q->select(['id','name']);

        }])->first();


        if (Auth::attempt(['email' => $request->email,'password' => $request->password,'ban'=>0])) {
            if (\auth()->user()->email_verified_at == null) {
                \auth()->logout();
                return response()->json(['status'=>404,'error' => 'your email not verified']);
            }
            
            $questionNumbers=Question::where('user_id',$user->id)->count();
            $answerNumbers=Answer::where('user_id',$user->id)->where('saveAsDraft',0)->count();
            $replyNumbers=Reply::where('user_id',$user->id)->count();
            $user['questionNumbers']=$questionNumbers;
            $user['answerNumbers']=$answerNumbers;
            $user['replyNumbers']=$replyNumbers;



            $this->receiveData(Auth::user()->id, $request->mac, $request->token);


            if (Auth::user()->token)
                return response()->json(['status'=>200,'token' => Auth::user()->token,'user'=>$user]);
            else {
                Auth::user()->token =bin2hex(random_bytes(20));
                Auth::user()->save();
                return response()->json(['status'=>200,'token' => Auth::user()->token,'user'=>$user]);
            }
        }
        return response()->json(['status'=>404,'error' => 'incorrect credentials']);

    }
    public function logoutApi()
    {
        $token=Input::get('token');
        $user=User::where('tokens',$token)->first();
        $user->AauthAcessToken()->delete();
        return response()->json(['status'=>200,'success'=>'logout Successfully']);
    }
    public function facebook(Request $request)
    {
        if ($request->facebook_id) {
            $user = User::where('provider_id', $request->facebook_id)->with(['country'=>function($q){
                $q->select(['id','name']);

            }])->with(['city'=>function($q){
                $q->select(['id','name']);

            }])->with(['educationDegree'=>function($q){
                $q->select(['id','name']);

            }])->first();
            if (!$request->token)
                return response()->json(['status'=>404,'error' => 'token required']);
            if (!$request->mac)
                return response()->json(['status'=>404,'error' => 'mac required']);

            if ($user) {
                $this->receiveData($user->id, $request->mac,$request->token);
                $questionNumbers=Question::where('user_id',$user->id)->count();
                $answerNumbers=Answer::where('user_id',$user->id)->where('saveAsDraft',0)->count();
                $replyNumbers=Reply::where('user_id',$user->id)->count();
                $user['questionNumbers']=$questionNumbers;
                $user['answerNumbers']=$answerNumbers;
                $user['replyNumbers']=$replyNumbers;
                return response()->json(['status'=>200,'token' => $user->token,'user'=>$user]);
            } else {
                if (!$request->f_name)
                    return response()->json(['status'=>404,'error' => 'first name required']);
                if (!$request->l_name)
                    return response()->json(['status'=>404,'error' => 'last name required']);

                $users = new User();
                $users->provider_id = $request->facebook_id;
                $users->fullName = $request->f_name .' '.$request->l_name;
                $users->email = $request->email;
                $users->token =bin2hex(random_bytes(20));
                $users->email_verified_at=Carbon::now();
                $users->save();
                $users=User::where('id',$users->id)->with(['country'=>function($q){
                    $q->select(['id','name']);

                }])->with(['city'=>function($q){
                    $q->select(['id','name']);

                }])->with(['educationDegree'=>function($q){
                    $q->select(['id','name']);

                }])->first();

                $this->receiveData($users->id, $request->mac,$request->token);
                $questionNumbers=Question::where('user_id',$users->id)->count();
                $answerNumbers=Answer::where('user_id',$users->id)->where('saveAsDraft',0)->count();
                $replyNumbers=Reply::where('user_id',$users->id)->count();
                $users['questionNumbers']=$questionNumbers;
                $users['answerNumbers']=$answerNumbers;
                $users['replyNumbers']=$replyNumbers;
                return response()->json(['status'=>200,'token' => $users->token,'user'=>$users]);
            }
        } else
            return response()->json(['status'=>404,'error' => 'facebook id required']);
    }
    public function twitter(Request $request)
    {
        if ($request->twitter_id) {
            $user = User::where('provider_id', $request->twitter_id)->first();
            if (!$request->token)
                return response()->json(['status'=>404,'error' => 'token_Required']);
            if (!$request->mac)
                return response()->json(['status'=>404,'error' => 'mac_Required']);

            if ($user) {
                $this->receiveData($user->id, $request->mac,$request->token);
                return response()->json(['status'=>200,'token' => $user->token,'user'=>$user]);
            } else {
                if (!$request->f_name)
                    return response()->json(['status'=>404,'error' => 'first name required']);
                if (!$request->l_name)
                    return response()->json(['status'=>404,'error' => 'last name required']);

                $users = new User();
                $users->provider_id = $request->twitter_id;
                $users->fullName = $request->f_name .' '.$request->l_name;
                $users->token =bin2hex(random_bytes(20));
                $users->email_verified_at=Carbon::now();
                $users->save();
                $this->receiveData($users->id, $request->mac,$request->token);
                return response()->json(['status'=>200,'token' => $users->token,'user'=>$user]);
            }
        } else
            return response()->json(['status'=>404,'error' => 'twitter id required']);
    }
    public function google(Request $request)
    {
        if ($request->google_id) {
            $user = User::where('provider_id', $request->google_id)->with(['country'=>function($q){
                $q->select(['id','name']);

            }])->with(['city'=>function($q){
                $q->select(['id','name']);

            }])->with(['educationDegree'=>function($q){
                $q->select(['id','name']);

            }])->first();
            if (!$request->token)
                return response()->json(['status'=>404,'error' => 'token_Required']);
            if (!$request->mac)
                return response()->json(['status'=>404,'error' => 'mac_Required']);

            if ($user) {
                $this->receiveData($user->id, $request->mac,$request->token);
                $questionNumbers=Question::where('user_id',$user->id)->count();
                $answerNumbers=Answer::where('user_id',$user->id)->where('saveAsDraft',0)->count();
                $replyNumbers=Reply::where('user_id',$user->id)->count();
                $user['questionNumbers']=$questionNumbers;
                $user['answerNumbers']=$answerNumbers;
                $user['replyNumbers']=$replyNumbers;
                return response()->json(['status'=>200,'token' => $user->token,'user'=>$user]);
            } else {
                if (!$request->f_name)
                    return response()->json(['status'=>404,'error' => 'first name required']);
                if (!$request->l_name)
                    return response()->json(['status'=>404,'error' => 'last name required']);

                $users = new User();
                $users->provider_id = $request->google_id;
                $users->fullName = $request->f_name .' '.$request->l_name;
                $users->token =bin2hex(random_bytes(20));
                $users->email_verified_at=Carbon::now();
                $users->save();
                $users=User::where('id',$users->id)->with(['country'=>function($q){
                    $q->select(['id','name']);

                }])->with(['city'=>function($q){
                    $q->select(['id','name']);

                }])->with(['educationDegree'=>function($q){
                    $q->select(['id','name']);

                }])->first();
                $this->receiveData($users->id, $request->mac,$request->token);
                $questionNumbers=Question::where('user_id',$users->id)->count();
                $answerNumbers=Answer::where('user_id',$users->id)->where('saveAsDraft',0)->count();
                $replyNumbers=Reply::where('user_id',$users->id)->count();
                $users['questionNumbers']=$questionNumbers;
                $users['answerNumbers']=$answerNumbers;
                $users['replyNumbers']=$replyNumbers;
                return response()->json(['status'=>200,'token' => $users->token,'user'=>$users]);
            }
        } else
            return response()->json(['status'=>404,'error' => 'google id required']);
    }
    public function getResetToken(Request $request)
    {
        if(! $request->email)
            return response()->json(['status'=>404,'msg'=> 'email required']);
        $user = User::where('email', $request->email)->first();
        if (!$user)
            return response()->json(['status'=>404,'error'=> 'invalid email']);
         PasswordReset::where('email', $request->email)->delete();
        $digits=4;

            $reset_password = PasswordReset::create([
                'email' => $request->email,
                'token' =>rand(pow(10, $digits-1), pow(10, $digits)-1)
            ]);
            $data = [
                'name' => $user->fullName,
                'subject' => 'كود استرجاع كلمة المرور',
                'token' => $reset_password->token
            ];

        Mail::send('emails.resetPassword', $data, function ($message) use ($user) {
            $message->from('smtp@maloma.com', 'resetpassword@maloma')
                ->to($user->email)
                ->subject('Reset password, maloma App');
        });
        return response()->json(['status'=>200,'success' => 'Code sent to your mail, please follow mail.']);

    }


    public function checkResetToken(Request $request)
    {
        if (!$request->code)
            return response()->json(['status'=>404,'error' => 'code required']);
        if (!$request->email)
            return response()->json(['status'=>404,'error' => 'email required']);
        $reset_password = PasswordReset::where('email', $request->email)
            ->where('token', $request->code)->first();
        if (!$reset_password)
            return response()->json(['status'=>404,'error' => 'invalid code']);
        else
            return response()->json(['status'=>200,'success' => 'valid code']);

    }
    public function reset(Request $request)
    {
        if (!$request->email)
            return response()->json(['status'=>404,'error' => 'email required']);

        if (!$request->new_password)
            return response()->json(['status'=>404,'error' => 'new password required']);
        $user = User::where('email', $request->email)->first();
        $reset_password = PasswordReset::where('email', $request->email)
            ->first();

        if ($reset_password == NULL || $user == null) {
            return response()->json(['status'=>404,'error'=> 'invalid data']);
        } else {
            $user->password = bcrypt($request->new_password);
            $user->save();
            PasswordReset::destroy($reset_password->id);
            return response()->json(['status'=>200,'success' => 'password reset successfully']);
        }

    }
    public function updatePassword(Request $request)
    {
        if (!$request->token)
            return response()->json(['status'=>404,'error' => 'token required']);
        if (!$request->old_password)
            return response()->json(['status'=>404,'error' => 'old password required']);
        if (!$request->new_password)
            return response()->json(['status'=>404,'error' => 'new password required']);
        $user = User::where('token', $request->token)->first();
        if($user){
            if (! Hash::check($request->old_password,$user->password)) {
                return response()->json(['status' => 404, 'error' => 'incorrect old password']);
            }else {

                $user->password = bcrypt($request->new_password);
                $user->save();
                return response()->json(['status' => 200, 'success' => 'password updated successfully']);
            }
        }else{
            return response()->json(['status'=>404,'error' => 'invalid data']);
        }




    }
    public function editUserInfo(Request $request){
        if (!$request->token)
            return response()->json(['status'=>404,'error' => 'token required']);

        $user = User::where('token', $request->token)->with(['country'=>function($q){
            $q->select(['id','name']);

        }])->with(['city'=>function($q){
            $q->select(['id','name']);

        }])->with(['educationDegree'=>function($q){
            $q->select(['id','name']);

        }])->first();



        if($user){


            if($request->fullName)
                $user->update(['fullName'=>$request->fullName]);
            if($request->userName)
                $user->update(['userName'=>$request->userName]);
            if($request->country_id)
                $user->update(['country_id'=>$request->country_id]);
            if($request->city_id)
                $user->update(['city_id'=>$request->city_id]);
            if($request->educationDegree_id)
                $user->update(['educationDegree_id'=>$request->educationDegree_id]);
            if($request->yearsOfExperience)
                $user->update(['yearsOfExperience'=>$request->yearsOfExperience]);
            if($request->aboutYourSelf)
                $user->update(['aboutYourSelf'=>$request->aboutYourSelf]);
            if($request->specialization)
                $user->update(['specialization'=>$request->specialization]);
            if($request->email && $request->email!=$user->email) {
                $oldemail=User::where('email',$request->email)->first();
                if (!$oldemail) {
                    $user->update(['email' => $request->email]);
                }else{
                    return response()->json(['status'=>404,'error'=>'email already exist']);


                }
            }
            if($request->phone){
                $user->phone=$request->phone;
                $user->save();
            }

            if($request->password)
                $user->update(['password'=>bcrypt($request->password)]);
            //profile picture
            if ($request->photo) {

                $user->photo =$request->photo;
                $user->save();

            }
            $questionNumbers=Question::where('user_id',$user->id)->count();
            $answerNumbers=Answer::where('user_id',$user->id)->where('saveAsDraft',0)->count();
            $replyNumbers=Reply::where('user_id',$user->id)->count();
            $user['questionNumbers']=$questionNumbers;
            $user['answerNumbers']=$answerNumbers;
            $user['replyNumbers']=$replyNumbers;


            return response()->json(['status'=>200,'token' =>$request->token,'user'=>$user]);
        }
        else{
            return response()->json(['status'=>404,'error' =>'invalid data']);
        }
    }

    public function getUserInfo(){
       $token= Input::get('token');



        $user=User::where('token',$token)->with(['country'=>function($q){
            $q->select(['id','name']);

        }])->with(['city'=>function($q){
            $q->select(['id','name']);

        }])->with(['educationDegree'=>function($q){
            $q->select(['id','name']);

        }])->first();
        $questionNumbers=Question::where('user_id',$user->id)->count();
        $answerNumbers=Answer::where('user_id',$user->id)->where('saveAsDraft',0)->count();
        $replyNumbers=Reply::where('user_id',$user->id)->count();
        $user['questionNumbers']=$questionNumbers;
        $user['answerNumbers']=$answerNumbers;
        $user['replyNumbers']=$replyNumbers;

        return response()->json(['status'=>200,'token' => $token,'user'=>$user]);

    }
    public function getUserProfile($id){


        $user=User::where('id',$id)->with(['country'=>function($q){
            $q->select(['id','name']);

        }])->with(['city'=>function($q){
            $q->select(['id','name']);

        }])->with(['educationDegree'=>function($q){
            $q->select(['id','name']);

        }])->first();
        $questions=Question::where('user_id',$user->id)->select(['id','questionText','photo','created_at'])->orderBy('created_at','desc')->get();
        $questions->transform(function ($i)use($id) {
            $answerNumbers=Answer::where('question_id',$i->id)->where('saveAsDraft',0)->count();
            $saved=Save::where('question_id',$i->id)->where('user_id',$id)->exists();
            $likeNumbers=Like::where('ref_id',$i->id)->where('type',1)->count();
            $liked = Like::where('ref_id', $i->id)->where('type', 1)->where('user_id',$id)->exists();

            $i->answerNumbers = $answerNumbers;
            $i->likeNumbers = $likeNumbers;
            $i->saved = $saved;
            $i->liked = $liked;
            return $i;
        });

        $answers=Answer::where('user_id',$user->id)->where('saveAsDraft',0)->select(['id','answerText','photo','question_id','created_at'])
            ->with(['question'=>function($q){
                $q->select(['id','questionText','user_id'])->with(['user'=>function($q){
                    $q->select(['id','photo']);
                }]);

            }])->orderBy('created_at','desc')->get();

        $replies=Reply::where('user_id',$user->id)->select(['id','replyText','answer_id','created_at'])
            ->with(['answer'=>function($q){
                $q->select(['id','answerText','user_id','question_id'])->where('saveAsDraft',0)->with(['user'=>function($q){
                    $q->select(['id','photo']);
                }])->with(['question'=>function($q){
                    $q->select(['id','questionText']);
                }]);

            }])->orderBy('created_at','desc')->get();
        $user['questionNumbers']=$questions->count();
        $user['answerNumbers']=$answers->count();
        $user['replyNumbers']=$replies->count();

        return response()->json(['status'=>200,'token' => $user->token,'user'=>$user,'questions'=>$questions,'answers'=>$answers,'replies'=>$replies]);

    }
    public function aboutUs(){
        $setting=Setting::first();
        return response()->json(['status'=>200,'aboutUs'=>$setting->aboutUs]);
    }
    public function turnOnOffNotification()
    {
        $token= Input::get('token');
        $user=User::where('token',$token)->first();
        if($user->notifiable==1)
        {
            $user->notifiable=0;
            $user->save();
            return response()->json(['status'=>200,'msg'=>'Notifications Turn OFF']);

        }
        if($user->notifiable==0)
        {
            $user->notifiable=1;
            $user->save();
            return response()->json(['status'=>200,'msg'=>'Notifications Turn ON']);

        }

    }
//    -----------------------------
//Lists
    public  function getCountries(){
        $countries=Country::select(['id','name'])->get();
        return response()->json(['status'=>200,'countries' => $countries]);
    }
    public function getCity($id){
        $cities = City::where('country_id', $id)->select(['id','name'])->get();
        return response()->json(['status'=>200,'cities' => $cities]);
    }
    public  function getEducationDegrees(){
        $educationDegrees=educationDegree::select(['id','name'])->get();
        return response()->json(['status'=>200,'educationDegrees' => $educationDegrees]);
    }
    public  function getCategories(){
        $categories=Category::select(['id','name','photo'])->get();
        $categories->transform(function ($i) {
            $i->photo=URL::to('/').$i->photo;
            return $i;

        });

        return response()->json(['status'=>200,'categories' => $categories]);
    }
//    --------------------------------
    public function customPaginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, ['path'=>url('http://localhost:8000/api/filter')]);
    }
    public function questions(){
        $token= Input::get('token');
        $questions=Question::select(['id','questionText','photo','user_id','category_id','saveAsDraft','created_at'])->where('saveAsDraft',0)->with(['user'=>function($q){
            $q->select(['id','fullName','photo','specialization','educationDegree_id'])->with(['educationDegree'=>function($q){
                $q->select(['id','name']);
            }]);
        }])->with(['category'=>function($q){
            $q->select(['id','name']);
        }])->orderBy('created_at','desc')->paginate(10);

        $questions->transform(function ($i)use($token) {
            $answerNumbers=Answer::where('question_id',$i->id)->where('saveAsDraft',0)->count();
            $likeNumbers=Like::where('ref_id',$i->id)->where('type',1)->count();
            $i->answerNumbers = $answerNumbers;
            $i->likeNumbers = $likeNumbers;
            if ($token!=null)
            {
                $user=User::where('token',$token)->first();
                $saved = Save::where('question_id', $i->id)->where('user_id', $user->id)->exists();
                $liked = Like::where('ref_id', $i->id)->where('type', 1)->where('user_id', $user->id)->exists();

                $i->saved = $saved;
                $i->liked = $liked;
            }
            else{
                $i->saved = false;
                $i->liked = false;
            }
            return $i;
        });
        return response()->json(['status'=>200,'questions'=>$questions]);



    }
    public function addQuestion(Request $request)
    {
        if (!$request->token)
            return response()->json(['status'=>404,'error' => 'token required']);
        if (!$request->questionText)
            return response()->json(['status'=>404,'error' => 'questionText required']);
        $user=User::where('token',$request->token)->first();
        if ($user)
        {

            $question= Question::create([
                'user_id'=>$user->id,
                'questionText'=>$request->questionText,
            ]);
            if ($request->photo) {
//                $imageName = str_random(10).'.'.$request->file('photo')->extension();
//                $request->file('photo')->move(
//                    base_path() . '/public/uploads/questions/', $imageName
//                );
                $question->photo =$request->photo;
                $question->save();

            }
            if ($request->category_id){
                $question->update(['category_id'=>$request->category_id]);
            }
            if ($request->cityIds){
                foreach ($request->cityIds as $cityId){
                    QuestionCity::create([
                        'question_id'=>$question->id,
                        'city_id'=>$cityId
                    ]);

                }
            }
//            if ($request->saveAsDraft==1){
//                $question->saveAsDraft=$request->saveAsDraft;
//                $question->save();
//            }



            return response()->json(['status'=>200,'success' => 'question added Successfully']);

        }else{
            return response()->json(['status'=>404,'error' => 'invalid data!']);

        }
    }
    public function addReport(Request $request)
    {
        if (!$request->token)
            return response()->json(['status'=>404,'error' => 'token required']);
        if (!$request->question_id)
            return response()->json(['status'=>404,'error' => 'question_id required']);
        if (!$request->message)
            return response()->json(['status'=>404,'error' => 'message required']);
        $user=User::where('token',$request->token)->first();
        if ($user)
        {

            $question= Report::create([
                'user_id'=>$user->id,
                'question_id'=>$request->question_id,
                'message'=>$request->message,
            ]);



            return response()->json(['status'=>200,'success' => 'Report added Successfully']);

        }else{
            return response()->json(['status'=>404,'error' => 'invalid data!']);

        }
    }
    public function search(Request $request){

        if($request->name)
        {
            $questions=Question::select(['id','questionText','photo','user_id','category_id','saveAsDraft','created_at'])->where('saveAsDraft',0)->with(['user'=>function($q){
                $q->select(['id','fullName','photo','specialization','educationDegree_id'])->with(['educationDegree'=>function($q){
                    $q->select(['id','name']);
                }]);
            }])->with(['category'=>function($q){
                $q->select(['id','name']);
            }]);
            $name=$request->name;
            $questions=$questions->where(function($query) use ($name){
                $query->where('questionText', 'like', '% ' . $name . '%');
                $query->orWhere('questionText', 'like', '% ' . $name);
                $query->orWhere('questionText', 'like', $name . '%');
                $query->orWhere('questionText', 'like', '% ' . $name . '%');
                $query->orWhere('questionText', 'like', '% ' . $name);
                $query->orWhere('questionText', 'like', $name . '%');
            });
            $questions=$questions->orderBy('created_at','desc')->get();

        }else{
            return response()->json(['status'=>404,'error'=>'name reuired']);

        }

        return response()->json(['status'=>200,'questions'=>$questions]);



    }
    public function filter(Request $request){
        $questions=Question::select(['id','questionText','photo','user_id','category_id','saveAsDraft','created_at'])->where('saveAsDraft',0)->with(['user'=>function($q){
            $q->select(['id','fullName','photo','specialization','educationDegree_id'])->with(['educationDegree'=>function($q){
                $q->select(['id','name']);
            }]);
        }])->with(['category'=>function($q){
            $q->select(['id','name']);
        }]);

        if($request->cities)
        {
            $cities=$request->cities;
            $questions=$questions->whereHas('cities',function ($q) use($cities){
                $q->whereIn('city_id',$cities);

            });;
        }
        if($request->categories)
        {
            $categories=$request->categories;
            $questions=$questions->whereIn('category_id',$categories);
        }
        if($request->sortBy==1)
        {
            $questions=$questions->orderBy('created_at','desc');

        }
        if($request->sortBy==2)
        {
            $questions=$questions->orderBy('created_at','asc');

        }else{
            $questions=$questions->orderBy('created_at','desc');


        }

        $questions=$questions->get();
        $count=$questions->count();
        $token=$request->token;
        $questions->transform(function ($i)use($token) {
            $answerNumbers=Answer::where('question_id',$i->id)->where('saveAsDraft',0)->count();
            $likeNumbers=Like::where('ref_id',$i->id)->where('type',1)->count();
            $i->answerNumbers = $answerNumbers;
            $i->likeNumbers = $likeNumbers;
            if ($token!=null)
            {
                $user=User::where('token',$token)->first();
                $saved = Save::where('question_id', $i->id)->where('user_id', $user->id)->exists();
                $liked = Like::where('ref_id', $i->id)->where('type', 1)->where('user_id', $user->id)->exists();

                $i->saved = $saved;
                $i->liked = $liked;
            }
            else{
                $i->saved = false;
                $i->liked = false;
            }
            return $i;
        });
        $questions=$this->customPaginate($questions);

        return response()->json(['status'=>200,'count'=>$count,'questions'=>$questions]);



    }
    public function questionDetails($id){
        $token= Input::get('token');
        $question=Question::where('id',$id)->select(['id','questionText','photo','user_id','category_id','created_at'])->with(['user'=>function($q){
            $q->select(['id','fullName','photo','specialization','educationDegree_id'])->with(['educationDegree'=>function($q){
                $q->select(['id','name']);
            }]);
        }])->with(['category'=>function($q){
            $q->select(['id','name']);
        }])->first();
        if ($question) {

            $answers = Answer::where('question_id', $id)->where('saveAsDraft', 0)->select(['id', 'answerText', 'photo', 'user_id', 'created_at'])
                ->with(['user' => function ($q) {
                    $q->select(['id', 'fullName', 'photo']);
                }])->get();

            $answers->transform(function ($i) use ($token) {
                $replyNumbers = Reply::where('answer_id', $i->id)->count();
                $likeNumbers = Like::where('ref_id', $i->id)->where('type', 2)->count();
                $i->replyNumbers = $replyNumbers;
                $i->likeNumbers = $likeNumbers;
                if ($token != null) {
                    $user = User::where('token', $token)->first();
                    $liked = Like::where('ref_id', $i->id)->where('type', 2)->where('user_id', $user->id)->exists();

                    $i->liked = $liked;
                } else {
                    $i->liked = false;
                }
                return $i;
            });
            $likeNumbers = Like::where('ref_id', $id)->where('type', 1)->count();
            $question->answerNumbers = $answers->count();
            $question->likeNumbers = $likeNumbers;
            if ($token != null) {
                $user = User::where('token', $token)->first();
                $liked = Like::where('ref_id', $id)->where('type', 1)->where('user_id', $user->id)->exists();
                $saved = Save::where('question_id', $id)->where('user_id', $user->id)->exists();


                $question->liked = $liked;
                $question->saved = $saved;
            } else {
                $question->liked = false;
                $question->saved = false;
            }
            $question->answers = $answers;

            return response()->json(['status' => 200, 'question' => $question]);
        }else{
            return response()->json(['status' => 400, 'error' => 'not found']);


        }



    }
    public function replies($id){
            $replies=Reply::where('answer_id',$id)->select(['id','replyText','user_id','created_at'])
                ->with(['user'=>function($q){
                $q->select(['id','fullName','photo']);
            }])->get();


        return response()->json(['status'=>200,'replies'=>$replies]);



    }
    public function myQuestions()
    {
        $token= Input::get('token');
        $user=User::where('token',$token)->first();

        $questions=Question::where('user_id',$user->id)->select(['id','questionText','photo','user_id','category_id','saveAsDraft','created_at'])->where('saveAsDraft',0)
            ->with(['category'=>function($q){
            $q->select(['id','name']);
        }])->orderBy('created_at','desc')->paginate(10);
        $questions->transform(function ($i)use($user) {
            $answerNumbers=Answer::where('question_id',$i->id)->where('saveAsDraft',0)->count();
            $likeNumbers=Like::where('ref_id',$i->id)->where('type',1)->count();
            $i->answerNumbers = $answerNumbers;
            $i->likeNumbers = $likeNumbers;
            $liked = Like::where('ref_id', $i->id)->where('type', 1)->where('user_id', $user->id)->exists();
            $i->liked=$liked;


            return $i;
        });
        return response()->json(['status'=>200,'questions'=>$questions]);



    }
    public function deleteAllQuestions()
    {
        $token= Input::get('token');
        $user=User::where('token',$token)->first();

       $questions= Question::where('user_id',$user->id)->get();
       foreach ($questions as $question)
       {
          $answers= Answer::where('question_id',$question->id)->get();
          foreach ($answers as $answer){
              Reply::where('answer_id',$answer->id)->delete();
              $answer->delete();
          }
           $question->delete();

       }

        return response()->json(['status'=>200,'success'=>'All Questions Deleted Successfully']);



    }
    public function deleteOneQuestion($id)
    {
        $token= Input::get('token');
        $user=User::where('token',$token)->first();

       $question= Question::where('user_id',$user->id)->where('id',$id)->first();
        $answers= Answer::where('question_id',$question->id)->get();
        foreach ($answers as $answer){
            Reply::where('answer_id',$answer->id)->delete();
            $answer->delete();
        }
        $question->delete();

        return response()->json(['status'=>200,'success'=>'Question Deleted Successfully']);



    }
    public function categoryQuestions($id){
        $token= Input::get('token');
        $questions=Question::where('category_id',$id)->select(['id','questionText','photo','user_id','category_id','saveAsDraft','created_at'])->where('saveAsDraft',0)->with(['user'=>function($q){
            $q->select(['id','fullName','photo','specialization','educationDegree_id'])->with(['educationDegree'=>function($q){
                $q->select(['id','name']);
            }]);
        }])->with(['category'=>function($q){
            $q->select(['id','name']);
        }])->orderBy('created_at','desc')->paginate(10);

        $questions->transform(function ($i)use($token) {
            $answerNumbers=Answer::where('question_id',$i->id)->where('saveAsDraft',0)->count();
            $likeNumbers=Like::where('ref_id',$i->id)->where('type',1)->count();
            $i->answerNumbers = $answerNumbers;
            $i->likeNumbers = $likeNumbers;
            if ($token!=null)
            {
                $user=User::where('token',$token)->first();
                $saved = Save::where('question_id', $i->id)->where('user_id', $user->id)->exists();
                $liked = Like::where('ref_id', $i->id)->where('type', 1)->where('user_id', $user->id)->exists();

                $i->saved = $saved;
                $i->liked = $liked;
            }
            else{
                $i->saved = false;
                $i->liked = false;
            }
            return $i;
        });
        return response()->json(['status'=>200,'questions'=>$questions]);



    }
    public function saves(){
        $token= Input::get('token');
        $user=User::where('token',$token)->first();
        $ids=Save::where('user_id',$user->id)->pluck('question_id')->toArray();
        $questions=Question::whereIn('id',$ids)->select(['id','questionText','photo','user_id','category_id','created_at'])->with(['user'=>function($q){
            $q->select(['id','fullName','photo','specialization','educationDegree_id'])->with(['educationDegree'=>function($q){
                $q->select(['id','name']);
            }]);
        }])->with(['category'=>function($q){
            $q->select(['id','name']);
        }])->orderBy('created_at','desc')->paginate(10);

        $questions->transform(function ($i)use($user) {
            $answerNumbers=Answer::where('question_id',$i->id)->where('saveAsDraft',0)->count();
            $likeNumbers=Like::where('ref_id',$i->id)->where('type',1)->count();
            $i->answerNumbers = $answerNumbers;
            $i->likeNumbers = $likeNumbers;


                $saved = Save::where('question_id', $i->id)->where('user_id', $user->id)->exists();
                $liked = Like::where('ref_id', $i->id)->where('type', 1)->where('user_id', $user->id)->exists();

                $i->saved = $saved;
                $i->liked = $liked;


            return $i;
        });
        return response()->json(['status'=>200,'questions'=>$questions]);



    }
    public function deleteAllSaves()
    {
        $token= Input::get('token');
        $user=User::where('token',$token)->first();

        Save::where('user_id',$user->id)->delete();

        return response()->json(['status'=>200,'success'=>'All Saved Questions Deleted Successfully']);



    }
    public function addToSaves(Request $request)
    {
        if (!$request->token)
            return response()->json(['status'=>404,'error' => 'token required']);
        if (!$request->question_id)
            return response()->json(['status'=>404,'error' => 'question_id required']);
        $user=User::where('token',$request->token)->first();
        if ($user)
        {
            $saved=Save::where('question_id',$request->question_id)->where('user_id',$user->id)->first();
            if ($saved){
                $saved->delete();
                return response()->json(['status'=>200,'success' => 'Removed From Saves Successfully']);

            }else{
                Save::create([
                    'question_id'=>$request->question_id,
                    'user_id'=>$user->id,
                ]);
                return response()->json(['status'=>200,'success' => 'Added To Saves Successfully']);

            }
        }else{
            return response()->json(['status'=>404,'error' => 'invalid data!']);

        }
    }
    public function addTofav(Request $request)
    {
        if (!$request->token)
            return response()->json(['status'=>404,'error' => 'token required']);
        if (!$request->ref_id)
            return response()->json(['status'=>404,'error' => 'ref_id required']);
        if (!$request->type)
            return response()->json(['status'=>404,'error' => 'type required']);
        $user=User::where('token',$request->token)->first();
        if ($user)
        {
            $liked=Like::where('ref_id',$request->ref_id)->where('user_id',$user->id)->where('type',$request->type)->first();
            if ($liked){
                $liked->delete();
                return response()->json(['status'=>200,'success' => 'Removed From Favourites Successfully']);

            }else{
                Like::create([
                    'ref_id'=>$request->ref_id,
                    'type'=>$request->type,
                    'user_id'=>$user->id,
                ]);
                //question notification
                if ($request->type==1)
                {
                    $question=Question::find($request->ref_id);
                    if($user->id!=$question->user->id)
                    {
                        if ($question->user->notifiable) {
                            $message = 'قام العضو ' . $user->fullName . ' بالاعجاب بسؤالك';
                            Notification::send($question->user, new likeQuestion($message, $question->id));
                            $phone_datas = MobileData::where('user_id', $question->user->id)
                                ->get(); // user who receive notification
                            $title = 'الاعجاب بسؤال';
                            $arr = [
                                'title' => $title,
                                'body' => $message,
                                'sound' => 'default',
                                'note_type' => 'likeQuestion',
                                'id' => $question->id,
                                'content_available' => true,
                                'priority' => 'high'
                            ];

                            foreach ($phone_datas as $phone_data) {
                                Firebase_notifications_fcm($phone_data->mobile_token, $title, $message, $arr);
                            }


                        }
                    }

                }
                //like answer
                if ($request->type==2)
                {
                    $answer=Answer::find($request->ref_id);
                    if($user->id!=$answer->user->id)
                    {
                        if ($answer->user->notifiable) {
                            $message = 'قام العضو ' . $user->fullName . ' بالاعجاب باجابتك';
                            Notification::send($answer->user, new likeAnswer($message, $answer->id));
                            $phone_datas = MobileData::where('user_id', $answer->user->id)
                                ->get(); // user who receive notification
                            $title = 'الاعجاب باجابه';
                            $arr = [
                                'title' => $title,
                                'body' => $message,
                                'sound' => 'default',
                                'note_type' => 'likeAnswer',
                                'id' => $answer->id,
                                'content_available' => true,
                                'priority' => 'high'
                            ];

                            foreach ($phone_datas as $phone_data) {
                                Firebase_notifications_fcm($phone_data->mobile_token, $title, $message, $arr);
                            }


                        }
                    }

                }
                return response()->json(['status'=>200,'success' => 'Added To Favourites Successfully']);

            }
        }else{
            return response()->json(['status'=>404,'error' => 'invalid data!']);

        }
    }
    public function myAnswers(){
        $token= Input::get('token');

        $user=User::where('token',$token)->first();
        $answers=Answer::where('user_id',$user->id)->where('saveAsDraft',0)->select(['id','answerText','photo','user_id','question_id','created_at'])->with(['question'=>function($q){
            $q->select(['id','questionText','photo','user_id'])->with(['user'=>function($q){
                $q->select(['id','photo']);
            }]);
        }])->orderBy('created_at','desc')->paginate(10);
        return response()->json(['status'=>200,'answers'=>$answers]);



    }
    public function deleteAllAnswers()
    {
        $token= Input::get('token');
        $user=User::where('token',$token)->first();

        $answers=Answer::where('user_id',$user->id)->get();
        foreach ($answers as $answer){
            Reply::where('answer_id',$answer->id)->delete();
            $answer->delete();
        }

        return response()->json(['status'=>200,'success'=>'All Answers Deleted Successfully']);

    }
    public function deleteOneAnswer($id)
    {
        $token= Input::get('token');
        $user=User::where('token',$token)->first();

        $answer=Answer::where('user_id',$user->id)->where('id',$id)->first();
        Reply::where('answer_id',$answer->id)->delete();
        $answer->delete();

        return response()->json(['status'=>200,'success'=>'Answer Deleted Successfully']);



    }
    public function addAnswer(Request $request)
    {
        if (!$request->token)
            return response()->json(['status'=>404,'error' => 'token required']);
        if (!$request->question_id)
            return response()->json(['status'=>404,'error' => 'question_id required']);
        if (!$request->answerText)
            return response()->json(['status'=>404,'error' => 'answerText required']);
        $user=User::where('token',$request->token)->first();
        if ($user)

        {
            $question=Question::find($request->question_id);

           $answer= Answer::create([
                'user_id'=>$user->id,
                'question_id'=>$request->question_id,
                'answerText'=>$request->answerText,
            ]);
            if ($request->photo) {
//                $imageName = str_random(10).'.'.$request->file('photo')->extension();
//                $request->file('photo')->move(
//                    base_path() . '/public/uploads/answers/', $imageName
//                );
                $answer->photo =$request->photo;
                $answer->save();

            }
            if ($request->saveAsDraft==1){
                $answer->saveAsDraft=$request->saveAsDraft;
                $answer->save();
            }else{
                if($user->id!=$question->user->id)
                {
                    if ($question->user->notifiable) {
                        $message = 'قام العضو ' . $user->fullName . ' بالاجابه على سؤالك';
                        Notification::send($question->user, new answerQuestion($message, $question->id));
                        $phone_datas = MobileData::where('user_id', $question->user->id)
                            ->get(); // user who receive notification
                        $title = 'اجابه على سؤال';
                        $arr = [
                            'title' => $title,
                            'body' => $message,
                            'sound' => 'default',
                            'note_type' => 'answerQuestion',
                            'id' => $question->id,
                            'content_available' => true,
                            'priority' => 'high'
                        ];

                        foreach ($phone_datas as $phone_data) {
                            Firebase_notifications_fcm($phone_data->mobile_token, $title, $message, $arr);
                        }


                    }
                }


            }



            return response()->json(['status'=>200,'success' => 'answer added Successfully']);

        }else{
            return response()->json(['status'=>404,'error' => 'invalid data!']);

        }
    }
    public function addReply(Request $request)
    {
        if (!$request->token)
            return response()->json(['status'=>404,'error' => 'token required']);
        if (!$request->answer_id)
            return response()->json(['status'=>404,'error' => 'answer_id required']);
        if (!$request->replyText)
            return response()->json(['status'=>404,'error' => 'replyText required']);
        $user=User::where('token',$request->token)->first();
        if ($user)

        {
            $answer=Answer::find($request->answer_id);

           $reply= Reply::create([
                'user_id'=>$user->id,
                'answer_id'=>$request->answer_id,
                'replyText'=>$request->replyText,
            ]);


            return response()->json(['status'=>200,'success' => 'reply added Successfully']);

        }else{
            return response()->json(['status'=>404,'error' => 'invalid data!']);

        }
    }
    public function uploadFile(Request $request){
        if (!$request->file)
            return response()->json(['status'=>404,'error' => 'file required']);

        $imageName =Carbon::now()->timestamp. str_random(10).'.'.$request->file('file')->extension();
        $request->file('file')->move(
            base_path() . '/public/uploads/', $imageName

        );
        return response()->json(['status'=>200,'path' => '/uploads/'. $imageName]);



    }

    public function myDrafts(){
        $token= Input::get('token');
        $user=User::where('token',$token)->first();
        $answers=Answer::where('user_id',$user->id)->where('saveAsDraft',1)->select(['id','answerText','photo','user_id','question_id','created_at'])->with(['question'=>function($q){
            $q->select(['id','questionText','photo','user_id'])->with(['user'=>function($q){
                $q->select(['id','photo']);
            }]);
        }])->orderBy('created_at','desc')->paginate(10);
        return response()->json(['status'=>200,'drafts'=>$answers]);



    }
    public function deleteAllDrafts()
    {
        $token= Input::get('token');
        $user=User::where('token',$token)->first();

        Answer::where('user_id',$user->id)->where('saveAsDraft',1)->delete();

        return response()->json(['status'=>200,'success'=>'All Draft Answers Deleted Successfully']);

    }
    public function publishDraft(Request $request)
    {
        if (!$request->token)
            return response()->json(['status'=>404,'error' => 'token required']);
        if (!$request->draft_id)
            return response()->json(['status'=>404,'error' => 'draft_id required']);
        $user=User::where('token',$request->token)->first();
        if ($user)
        {

           $answer=Answer::find($request->draft_id);
           $answer->update(['saveAsDraft'=>0]);

           return response()->json(['status'=>200,'success' => 'Draft Published Successfully']);

        }else{
            return response()->json(['status'=>404,'error' => 'invalid data!']);

        }
    }

    public function sendMessage(Request $request)
    {
        if (!$request->token)
            return response()->json(['status'=>404,'error' => 'token required']);
        if (!$request->receiver_id)
            return response()->json(['status'=>404,'error' => 'receiver_id required']);
//        if (!$request->message)
//            return response()->json(['status'=>404,'error' => 'message required']);

        $user=User::where('token',$request->token)->first();
        if ($user)
        {
            $message=Message::create([
                'sender_id'=>$user->id,
                'receiver_id'=>$request->receiver_id,
                'message'=>$request->message,
                'reply_id'=>$request->reply_id

            ]);
            $sender=User::where('id',$message->sender_id)->select(['id','fullName','token','photo','educationDegree_id','specialization'])
                ->with(['educationDegree'=>function($q){
                    $q->select(['id','name']);
                }])->first();
            $message['sender']=$sender;

            if($request->type=='photo' ||$request->type=='record'||$request->type=='map' ||$request->type=='file' )
            {


                   $media= Media::create([
                        'message_id' => $message->id,
                        'media_file' => $request->media_file,
                        'type' => $request->type
                    ]);
//                }
                if($request->type=='photo'){
                    $body='ارسل اليك صوره';
                    $type='photo';

                }if($request->type=='record'){
                    $body='ارسل اليك مقطعا صوتيا';
                    $type='record';
                }
                if($request->type=='map'){
                    $body='ارسل اليك موقعا';
                    $type='map';
                }
                if($request->type=='file'){
                    $body='ارسل اليك ملف';
                    $type='file';
                }
                $message['medias']=$media;


            }
            else{
                $body=$message->message;
                $type='text';
                $message['medias']=null;

            }


            //notification
            $receiver=User::where('id',$request->receiver_id)->first();
//            $message = $request->message;
//                Notification::send($user, new sendMessage($message));
                $phone_datas = MobileData::where('user_id', $receiver->id)
                    ->get(); // user who receive notification
                $title = 'رساله';
                $arr = [
                    'title' => $title,
                    'body' => $body,
                    'sound' => 'default',
                    'type'=>$type,
                    'msg'=>$message,
                    'note_type' => 'sendMessage',
                    'content_available' => true,
                    'priority' => 'high'
                ];

                foreach ($phone_datas as $phone_data) {
                        Firebase_notifications_fcm($phone_data->mobile_token, $title, $body, $arr);
                }
                $reply=Message::where('reply_id',$message->id)->first();

            $message['reply']=$reply;


            return response()->json(['status'=>200,'success' => 'Message Sent Successfully','message'=>$message]);

        }else{
            return response()->json(['status'=>404,'error' => 'invalid data!']);

        }
    }
//    public function getMyChats(Request $request){
//        $token=$request->header('token');
//        $user = User::where('token', $token)->first();
//        if (!$user)
//            return response()->json(['status'=>'404','error' => 'invalid Token']);
//        $messages = Message::where('receiver_id', $user->id)
//            ->with(['sender'=>function($q){
//                $q->select(['id','fullName','token','photo','educationDegree_id','specialization'])
//                    ->with(['educationDegree'=>function($q){
//                    $q->select(['id','name']);
//                }])->get();
//            }])
//            ->get()->unique('sender_id')->values();
//        foreach ($messages as $message)
//        {
//
//            $message['since']=$message->created_at->diffForHumans();
//        }
//
//        return response()->json(['status'=>'200','messages' => $messages]);
//
//
//
//    }
    public function getMyChats(){
        $token= Input::get('token');

        $user = User::where('token', $token)->first();
        if (!$user)
            return response()->json(['status'=>404,'error' => 'invalid_Token']);
        $received_messages = Message::where('receiver_id', $user->id)->orderBy('created_at', 'desc')

            ->get()->unique('sender_id')->values();

//        $send_messages = Message::where('sender_id', $user->id)->orderBy('created_at', 'desc')
//            ->get()->unique('receiver_id')->values();

//        $send_messages->transform(function($i) {
//            $i->sender=User::where('id',$i->receiver_id)->select(['id','fullName','token','photo','educationDegree_id','specialization'])
//                ->with(['educationDegree'=>function($q){
//                    $q->select(['id','name']);
//                }])->first();
//            return $i;
//        });
        $received_messages->transform(function($i) {
            $i->sender=User::where('id',$i->sender_id)->select(['id','fullName','token','photo','educationDegree_id','specialization'])
                ->with(['educationDegree'=>function($q){
                    $q->select(['id','name']);
                }])->first();
            return $i;
        });
//        foreach($send_messages as $send_message) {
//            $received_messages->add($send_message);
//        }
        foreach ($received_messages as $message)
        {
            $message['since']=$message->created_at->diffForHumans();


        }

        return response()->json(['status'=>200,'messages' => $received_messages]);

    }

    public function deleteAllChats()
    {
        $token= Input::get('token');
        $user=User::where('token',$token)->first();

        Message::where('receiver_id',$user->id)->orWhere('sender_id',$user->id)->delete();

        return response()->json(['status'=>200,'success'=>'All Chats Deleted Successfully']);

    }
    public function deleteOneChat($id)
    {
        $token= Input::get('token');
        $user=User::where('token',$token)->first();

        Message::where('sender_id',$id)->where('receiver_id',$user->id)->delete();
        Message::where('receiver_id',$id)->where('sender_id',$user->id)->delete();


        return response()->json(['status'=>200,'success'=>'Chat Deleted Successfully']);



    }
    public function getMessageDetails($sender_id){
        $token= Input::get('token');
//        $sender_id=$request->sender_id;
        $user = User::where('token', $token)->first();
        if (!$user)
            return response()->json(['status'=>'404','error' => 'invalid Token']);
        $sender=User::find($sender_id);

        $messages= Message::where([['sender_id',$sender->id], ['receiver_id', $user->id]])
            ->orWhere(function ($query) use ($user,$sender) {
                return $query->where([['sender_id', $user->id], ['receiver_id',$sender->id]]);
            })
            ->with(['sender'=>function($q){
                $q->select(['id','fullName','token','photo'])->get();
            }])->with(['reply'=>function($q){
            }])->with(['medias'=>function($q){
                $q->select(['message_id','media_file','extension','type']);

            }])
            ->orderBy('created_at','asc')->get();

        $user->receivedMessages()
            ->where([['sender_id', $sender->id], ['seen', 0]])
            ->update(['seen' => true]);


        return response()->json(['status'=>'200','messages' => $messages]);

    }

    //notification
    public function notifications()
    {
        $token= Input::get('token');
        $user = User::where('token', $token)->first();
//        dd($user);

        $user->unreadNotifications->markAsRead();
        $notifications = \App\Models\Notification::where('notifiable_id', $user->id)
            ->where('notifiable_type', 'App\User')->select('unique_id','data','type','created_at')
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        foreach ($notifications as $notification) {
            $data = json_decode($notification->data);
            $notification['data']=$data;
                $notification['message'] = $data->message;

//            dd($notification);
            $notification['type'] = substr($notification->type,18);
            $notification['time']=$notification->created_at->format('h:i a');

        }
        return response()->json(['status'=>200,'notes'=>$notifications]);
    }
    public function deleteNotification(){
        $token= Input::get('token');
        $user = User::where('token', $token)->first();
        $notifications = \App\Models\Notification::where('notifiable_id', $user->id)->delete();
        return response()->json(['status'=>200,'success'=>'deleted successfully']);
    }
    public function deleteOneNotification($id){

        $note=  \App\Models\Notification::where('unique_id',$id)->first();
        if ($note) {
            $note->delete();
            return response()->json(['status' => 200, 'success' => 'deleted successfully']);
        }else{
            return response()->json(['status' => 400, 'success' => 'not found']);
        }
    }
    public function unreadNotifications()
    {
        $token= Input::get('token');
        $user = User::where('token', $token)->first();
        $count = $user->unreadNotifications->count();
        return response()->json(['status'=>200,'count'=>$count]);
    }





}
