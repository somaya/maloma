<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/dashboard" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-dashboard"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاحصائيات </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/countries" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الدول </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/cities" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المدن </span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/degrees" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-profile"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الدرجات العلمية </span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/users" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-user"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعضاء</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/admins" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-user-add"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المدراء</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/categories" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-grid-menu"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاقسام</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/questions" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-questions-circular-button"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاسئلة</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/reports" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-warning"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">البلاغات</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="/webadmin/settings/edit" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-settings"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعدادات</span>
            </span>
        </span>
    </a>
</li>
