<?php

namespace App\Http\Controllers\admin;

use App\Models\BankData;
use App\Models\CarOrder;
use App\Models\MobileData;
use App\Models\PropertyOrder;
use App\Models\Transfer;
use App\Notifications\BookingRequestReceived;
use App\Notifications\FromAdmin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Notification;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::where('role',0)->withTrashed()->get();

        return view('admin.users.index',compact('users'));
    }
    public function ban($id){
        $user=User::withTrashed()->where('id',$id)->first();
        if ($user->ban==0)
        {
            $user->ban = 1;
            $user->save();

            return redirect('/webadmin/users')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم الحظر بنجاح']));
        }else{
            $user->ban = 0;
            $user->save();
            return redirect('webadmin/users')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم فك الحظر بنجاح']));


        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'fullName' => 'required',
            'userName' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);
        $user=User::create([
            'fullName' => $request->fullName,
            'userName' => $request->userName,
            'email' => $request->email,
            'token'=>bin2hex(random_bytes(20)),
            'password' => Hash::make($request->password),
        ]);
        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = str_random(10).'.'.$request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $user->photo ='/uploads/profiles/'. $imageName;
            $user->save();

        }


        return redirect('/webadmin/users')->withFlashMessage(json_encode(['success'=>true,'msg'=>'User Added Successfully']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=User::withTrashed()->where('id',$id)->first();
        return view('admin.users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::withTrashed()->where('id',$id)->first();
        return view('admin.users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'fullName' => 'required',
            'userName' => 'required',

        ]);
        $user=User::withTrashed()->where('id',$id)->first();
        $user->update([
            'fullName'=>$request->fullName,
            'userName'=>$request->userName,
        ]);
        if ($request->email !=$user->email)
        {
            $this->validate($request, [
                'email' => 'email|unique:users',
            ]);
            $user->update([
                'email'=>$request->email
            ]);

        }
        if ($request->password !=''){
            $user->update([
                'password'=>Hash::make($request->password),
            ]);
        }
        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = str_random(10).'.'.$request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $user->photo ='/uploads/profiles/'. $imageName;
            $user->save();

        }

        return redirect('/webadmin/users')->withFlashMessage(json_encode(['success'=>true,'msg'=>'User Edited Successfully']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::where('id',$id)->first();

        $user->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'User Deleted Successfully']));
    }


}
