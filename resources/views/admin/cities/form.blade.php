<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم المدينه : </label>
    <div class="col-lg-10{{ $errors->has('name') ? ' has-danger' : '' }}">
        {!! Form::text('name',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم المدينه "]) !!}
        @if ($errors->has('name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>


</div>
<div class="form-group m-form__group row">
<label class="col-lg-2 col-form-label">الدوله : </label>
<div class="col-lg-10{{ $errors->has('country_id') ? ' has-danger' : '' }}">
    <select name="country_id" class="form-control">
        <option value="" disabled selected>اختر  الدوله</option>
        @foreach($countries as $country)
        <option value="{{$country->id}}" {{isset($city) && $city->country_id==$country->id?'selected':'' }} {{old('country_id')==$country->id? 'selected':''}}>{{$country->name}}</option>
            @endforeach

    </select>
    @if ($errors->has('country_id'))
        <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('country_id') }}</strong>
            </span>
    @endif
</div>
</div>
