<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Input;

Route::get('/', function () {

    return view('home');
});
Route::view('/api-docs', 'Docs.index');
Route::get('/webadmin/login', 'Auth\LoginController@showLoginForm');



Auth::routes();

Route::group(['prefix' => 'webadmin', 'middleware' => ['webadmin']], function () {
    Route::get('/dashboard', 'admin\DashboardController@index');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::resource('/degrees', 'admin\DegreeController');
    Route::resource('/countries', 'admin\CountriesController');
    Route::resource('/cities', 'admin\CitiesController');
    Route::resource('/users', 'admin\UsersController');
    Route::resource('/admins', 'admin\AdminsController');
    Route::resource('/categories', 'admin\CategoriesController');
    Route::resource('/questions', 'admin\QuestionsController');
    Route::resource('/question/{id}/answers', 'admin\AnswersController');
    Route::resource('/answer/{id}/replies', 'admin\RepliesController');
    Route::resource('/reports', 'admin\ReportsController');
    Route::get('/admins/{id}/admin', 'admin\AdminsController@admin');
    Route::get('/users/{id}/ban', 'admin\UsersController@ban');
    Route::get('/settings/edit', 'admin\SettingsController@edit');
    Route::post('/settings/update', 'admin\SettingsController@update');



});
Route::get('/getcode', function () {
    $video_id = Input::get('video_id');
    $code = Input::get('code');
    $code=\App\Models\Video_code::where('video_id',$video_id)->where('code',$code)->first();

    return response()->json($code);

});