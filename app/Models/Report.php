<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table='reports';
    protected $guarded=[];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }
}
