<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionCity extends Model
{
    protected $table='question_cities';
    protected $guarded=[];
}
