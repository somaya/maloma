<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class userCategory extends Model
{
    protected $table='userCategories';
    protected $guarded=[];
}
