<?php

namespace App\Http\Controllers\Auth;

use App\Models\Country;
use App\Models\Skill;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $countries=Country::all();
        $skills=Skill::all();
        return view('auth.register',compact('countries','skills'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'f_name' => ['required', 'string', 'max:255'],
            'l_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
            'title' => 'required',
            'phone' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'terms' => 'required',
            'photo' => 'required',
            'photo.*' => 'image|mimes:jpeg,png,jpg,gif,svg',

        ],[
            'f_name.required'=>'First  Name Required',
            'l_name.required'=>'Last  Name Required',
            'email.required'=>'Email Required',
            'email.email'=>'Email Entered Not Valid',
            'email.unique'=>'Email Entered Already Exist',
            'password.required'=>'Password Required',
            'photo.required'=>'Photo Required',
            'photo.image'=>'Must Upload Photo',
            'title.required'=>'User Title Required',
            'phone.required'=>'User Phone Required',
            'country_id.required'=>'User Country Required',
            'city_id.required'=>'User City Required',
            'terms.required'=>'Terms Not Checked',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $user=User::create([
            'f_name' => $data['f_name'],
            'l_name' => $data['l_name'],
            'email' => $data['email'],
            'title' => $data['title'],
            'phone' => $data['phone'],
            'country_id' => $data['country_id'],
            'city_id' => $data['city_id'],
            'facebook' => $data['facebook'],
            'instagram' => $data['instagram'],
            'linkedin' => $data['linkedin'],
            'achivements' => $data['achivements'],
            'aboutme' => $data['aboutme'],
            'password' => Hash::make($data['password']),
        ]);

        //photo
        if ($data['photo'][0]) {
            $imageName = str_random(10).'.'.$data['photo'][0]->extension();
            $data['photo'][0]->move(
                base_path() . '/public/uploads/profile/', $imageName
            );
            $user->photo ='/uploads/profile/'. $imageName;
            $user->save();

        }
        return $user;

    }
}
