<?php

namespace App\Http\Controllers\admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories=Category::all();

        return view('admin.categories.index',compact('categories'));
    }
    public function getSearchResult($status){
        if ($status==1){//verified
            $users=User::where('role',0)->where('can_rent',1)->where('national_id','<>',null)->withTrashed()->get();
        }
        if ($status==2){//waiting for verify
            $users=User::where('role',0)->where('can_rent',0)->where('national_id','<>',null)->withTrashed()->get();
        }
        if ($status==3){//unverified
            $users=User::where('role',0)->where('can_rent',0)->where('national_id',null)->withTrashed()->get();
        }
        $users->transform(function($i)  {
            $transfers=Transfer::where('user_id',$i->id)->sum('amount');
            $i->transfers=$transfers;
            return $i;


        });
        return response()->json($users);


    }
    public function verify($id){
        $user=User::withTrashed()->where('id',$id)->first();
        if ($user->can_rent==0)
        {
            $user->can_rent = 1;
            $user->save();
            $carOrders = CarOrder::where('status', 'Pending Verify')->where('user_id', $id)->get();
            $propertyOrders = PropertyOrder::where('status', 'Pending Verify')->where('user_id', $id)->get();
            foreach ($carOrders as $order) {
                $order->update(['status' => 'Requested']);
                $provider = $order->car->user;
                $data = [
                    'user' => $user,
                    'type' => 1
                ];

                Mail::send('emails.newBooking', $data, function ($message) use ($provider) {
                    $message->from('postmaster@renzo.app', 'renzo')
                        ->to($provider->email)
                        ->subject('Booking, renzo App');
                });
                //provider notification
                if ($provider->notifiable) {
                    $ar_message = 'لديك طلب جديد لاستئجار سيارتك';
                    $en_message = 'You have a new request to rent your car';
                    Notification::send($provider, new BookingRequestReceived($ar_message, $en_message, 1, $order->car->id));
                    $phone_datas = MobileData::where('user_id', $provider->id)
                        ->get(); // user who receive notification
                    $ar_title = 'حجز جديد';
                    $en_title = 'Booking Request';
                    $en_arr = [
                        'title' => 'Booking Request',
                        'body' => $en_message,
                        'sound' => 'default',
                        'type' => 1,
                        'ref_id' => $order->car->id,
                        'note_type' => 'BookingRequestReceived',
                        'content_available' => true,
                        'priority' => 'high'
                    ];
                    $ar_arr = [
                        'title' => 'حجز جديد',
                        'body' => $ar_message,
                        'sound' => 'default',
                        'type' => 1,
                        'ref_id' => $order->car->id,
                        'note_type' => 'BookingRequestReceived',
                        'content_available' => true,
                        'priority' => 'high'
                    ];
                    foreach ($phone_datas as $phone_data) {
                        if ($provider->current_language == 'ar')
                            Firebase_notifications_fcm($phone_data->mobile_token, $ar_title, $ar_message, $ar_arr);
                        if ($provider->current_language == 'en')
                            Firebase_notifications_fcm($phone_data->mobile_token, $en_title, $en_message, $en_arr);


                    }
                }


            }
            foreach ($propertyOrders as $order) {
                $order->update(['status' => 'Requested']);

                $provider = $order->property->user;
                $data = [
                    'user' => $user,
                    'type' => 2
                ];

                Mail::send('emails.newBooking', $data, function ($message) use ($provider) {
                    $message->from('postmaster@renzo.app', 'renzo')
                        ->to($provider->email)
                        ->subject('Booking, renzo App');
                });
                //notification
                if ($provider->notifiable) {
                    $ar_message = 'لديك طلب جديد لاستئجار عقارك';
                    $en_message = 'You have a new request to rent your property';
                    Notification::send($provider, new BookingRequestReceived($ar_message, $en_message, 2, $order->property->id));
                    $phone_datas = MobileData::where('user_id', $provider->id)
                        ->get(); // user who receive notification
                    $ar_title = 'حجز جديد';
                    $en_title = 'Booking Request';
                    $en_arr = [
                        'title' => 'Booking Request',
                        'body' => $en_message,
                        'sound' => 'default',
                        'type' => 2,
                        'ref_id' => $order->property->id,
                        'note_type' => 'BookingRequestReceived',
                        'content_available' => true,
                        'priority' => 'high'
                    ];
                    $ar_arr = [
                        'title' => 'حجز جديد',
                        'body' => $ar_message,
                        'sound' => 'default',
                        'type' => 2,
                        'ref_id' => $order->property->id,
                        'note_type' => 'BookingRequestReceived',
                        'content_available' => true,
                        'priority' => 'high'
                    ];
                    foreach ($phone_datas as $phone_data) {
                        if ($provider->current_language == 'ar')
                            Firebase_notifications_fcm($phone_data->mobile_token, $ar_title, $ar_message, $ar_arr);
                        if ($provider->current_language == 'en')
                            Firebase_notifications_fcm($phone_data->mobile_token, $en_title, $en_message, $en_arr);


                    }
                }


            }


            return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'User Verified, Can Rent Now']));
        }else{
            $user->can_rent = 0;
            $user->save();
            return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'Verify Removed Successfully']));


        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ]);


        $imageName = str_random(10).'.'.$request->file('photo')->extension();
        $request->file('photo')->move(
            base_path() . '/public/uploads/categories/', $imageName
        );

       Category::create([
            'name' => $request->name,
            'photo' => '/uploads/categories/'. $imageName,
        ]);



        return redirect('/webadmin/categories')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة القسم بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=Category::where('id',$id)->first();
        return view('admin.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $category=Category::where('id',$id)->first();

        if ($request->name !=$category->name)
        {

            $category->update([
                'name'=>$request->name
            ]);

        }

        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = str_random(10).'.'.$request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/categories/', $imageName
            );
            $category->photo ='/uploads/categories/'. $imageName;
            $category->save();

        }

        return redirect('/webadmin/categories')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم التعديل بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=Category::where('id',$id)->first();

        $category->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));
    }
}
