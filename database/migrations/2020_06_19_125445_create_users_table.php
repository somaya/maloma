<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullName');
            $table->string('userName')->nullable();
            $table->string('phone')->nullable();
            $table->string('provider_id')->nullable();
            $table->string('photo')->nullable();
            $table->string('activeCode')->nullable();
            $table->string('aboutYourSelf')->nullable();
            $table->integer('yearsOfExperience')->nullable();
            $table->date('birthDate')->nullable();
            $table->tinyInteger('role')->default(0);//1 for admin,  0 for user
            $table->tinyInteger('ban')->default(0);
            $table->tinyInteger('gender')->nullable();//1 for male,  2 for female
            $table->tinyInteger('notifiable')->default(1);
            $table->string('email')->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('token');

            $table->unsignedInteger('country_id')->nullable();
            $table->foreign('country_id')
                ->references('id')->on('countries')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedInteger('city_id')->nullable();
            $table->foreign('city_id')
                ->references('id')->on('cities')
                ->onDelete("cascade")
                ->onUpdate("cascade");

            $table->unsignedInteger('educationDegree_id')->nullable();
            $table->foreign('educationDegree_id')
                ->references('id')->on('educationDegrees')
                ->onDelete("cascade")
                ->onUpdate("cascade");
            $table->string('specialization')->nullable();

            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
