FORMAT: 1A
HOST:http://m3lomhapp.com/api

# Maloma API
This is Documention for Maloma app

# Group  user security
This group for user security

## App about us [/aboutUs]



+ Model

    + Body

            {
                "status": 200,
                "aboutUs": "aaaahjbjkkkkkkkkkkkkkkk"
            }

### App about us [GET]

+ Response 200 (application/json)

    [App about us][]    
## Register [/register]

+ Model

    + Body

            {
                "status": 200,
                "success": "Registered Successfully"
            }
### Register [POST]



+ Request (application/json)

    + Body

            {


            	"fullName":"somaya hegab",
            	"userName":"somayahegab",
            	"phone":12547898
            	"email":"somaya@gmail.com",
            	"password":"123456",
            	"country_id":1,
            	"city_id":1,
            	"gender":1 for male 2 for female,
            	"birthDate":"1993-06-1",
            	"educationDegree_id":1,
            	"specialization":"العلوم التطبيقيه",
            	"yearsOfExperience":1,
            	"aboutYourSelf":"about my self",
            	"categories":array of ids,
            	
            }




+ Response 200 (application/json)

    [Register][]
    
## uploadfile [/uploadFile]

+ Model

    + Body

            {
                "status": 200,
                "path": "/uploads/15954317680mmpaPUlDa.jpeg"
            }
### uploadfile [POST]



+ Request (application/json)

    + Body

            {


            	"file":"file",
            	
            	
            }




+ Response 200 (application/json)

    [uploadfile][]    
## Login With Facbook [/facebook]

+ Model

    + Body

            {
                "status": 200,
                "token": "9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f",
                "user": {
                    "id": 3,
                    "fullName": "fatma ali",
                    "userName": null,
                    "phone": null,
                    "photo": null,
                    "activeCode": null,
                    "aboutYourSelf": null,
                    "yearsOfExperience": null,
                    "birthDate": null,
                    "role": 0,
                    "gender": null,
                    "notifiable": 1,
                    "email": null,
                    "email_verified_at": "2020-06-19 19:50:44",
                    "token": "9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f",
                    "country_id": null,
                    "city_id": null,
                    "provider_id": "86542125242254",
                    "educationDegree_id": 1,
                    "specialization": null,
                    "deleted_at": null,
                    "created_at": "2020-06-19 19:50:44",
                    "updated_at": "2020-06-19 19:50:44",
                    "questionNumbers": 0,
                    "answerNumbers": 0,
                    "replyNumbers": 0,
                    "country": null,
                    "city": null,
                    "education_degree": {
                        "id": 1,
                        "name": "ماجستير"
                    }
                }
            }
### Login With Facbook [POST]



+ Request (application/json)

    + Body

            {


            	"facebook_id":"122385656018523",
            	"token":"hjnmujhnhgjjhghhj15j",
            	"mac":"vghn20562gh",
            	"f_name":"somaya",
            	"l_name":"hegab",
            	
            }




+ Response 200 (application/json)

    [Login With Facbook][]
## Login With google [/google]

+ Model

    + Body

            {
                "status": 200,
                "token": "9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f",
                "user": {
                    "id": 3,
                    "fullName": "fatma ali",
                    "userName": null,
                    "phone": null,
                    "photo": null,
                    "activeCode": null,
                    "aboutYourSelf": null,
                    "yearsOfExperience": null,
                    "birthDate": null,
                    "role": 0,
                    "gender": null,
                    "notifiable": 1,
                    "email": null,
                    "email_verified_at": "2020-06-19 19:50:44",
                    "token": "9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f",
                    "country_id": null,
                    "city_id": null,
                    "provider_id": "86542125242254",
                    "educationDegree_id": 1,
                    "specialization": null,
                    "deleted_at": null,
                    "created_at": "2020-06-19 19:50:44",
                    "updated_at": "2020-06-19 19:50:44",
                    "questionNumbers": 0,
                    "answerNumbers": 0,
                    "replyNumbers": 0,
                    "country": null,
                    "city": null,
                    "education_degree": {
                        "id": 1,
                        "name": "ماجستير"
                    }
                }
            }
### Login With google [POST]



+ Request (application/json)

    + Body

            {


            	"google_id":"122385656018523",
            	"token":"hjnmujhnhgjjhghhj15j",
            	"mac":"vghn20562gh",
            	"f_name":"somaya",
            	"l_name":"hegab",
            	
            }




+ Response 200 (application/json)

    [Login With google][]    


            
## verify user [/verifyUser]

+ Model

    + Body

            {
                "status": 200,
                "success": "Registered Successfully"
            }
### verify user [POST]



+ Request (application/json)

    + Body

            {


            	"code":"12452",
            	
            }




+ Response 200 (application/json)

    [verify user][]
      
 
## Login [/login]

+ Model

    + Body

            {
                "status": 200,
                "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                "user": {
                    "id": 2,
                    "fullName": "somaya hegab",
                    "userName": "somaya",
                    "phone": null,
                    "photo": null,
                    "activeCode": "143368",
                    "aboutYourSelf": "3",
                    "yearsOfExperience": 3,
                    "birthDate": "1993-06-01",
                    "role": 0,
                    "gender": 2,
                    "notifiable": 1,
                    "email": "somayahegab@gmail.com",
                    "email_verified_at": "2020-06-19 19:23:27",
                    "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                    "country_id": 1,
                    "city_id": 1,
                    "provider_id": null,
                    "educationDegree_id": 1,
                    "specialization": "الهندسه",
                    "deleted_at": null,
                    "created_at": "2020-06-19 17:05:01",
                    "updated_at": "2020-06-20 15:36:42",
                    "questionNumbers": 2,
                    "answerNumbers": 4,
                    "replyNumbers": 0,
                    "country": {
                        "id": 1,
                        "name": "السعودية"
                    },
                    "city": {
                        "id": 1,
                        "name": "جده"
                    },
                    "education_degree": {
                        "id": 1,
                        "name": "ماجستير"
                    }
                }
            }
### Login [POST]



+ Request (application/json)

    + Body

            {


          
            	"email" "somaya@gmail.com",
            	"password":"123456",
            	"token":"hjnmujhnhgjjhghhj15j",
                "mac":"vghn20562gh",
            }




+ Response 200 (application/json)

    [Login][]
+ Response 404 (application/json)

    + Body

            {
                "status": 404,
                "error": "invalid data"
            } 


    

            {
                "status": 404,
                "error": "Email Not verofied"
            }    


## Get Reset Token [/getResetToken]

+ Model

    + Body

            {
                "status": 200,
                "success": "Code sent to your mail, please follow mail."
            }
### Get Reset Token [POST]



+ Request (application/json)

    + Body

            {


          
            	"email" "somaya@gmail.com",
            	
            }




+ Response 200 (application/json)

    [Get Reset Token][]
    
## check reset token [/checkResetToken]

+ Model

    + Body

            {
                "status": 200,
                "success": "Valid Code"
            }
### check reset token [POST]

+ Request (application/json)

    + Body

            {
            	"code"=>895586
            	"email": "somaya@gmail.com",
            	
            }



+ Response 200 (application/json)

    [check reset token][]

+ Response 404 (application/json)

    + Body

            {
                "status": 404,
                "error": "Invalid Code"
            }            



## Reset Password [/reset]

+ Model

    + Body

            {
                "status": 200,
                "success": "password updated successfully."
            }
### Reset Password [POST]



+ Request (application/json)

    + Body

            {


          
            	"email" "somaya@gmail.com",
            	"new_password" :123456,
            	
            }




+ Response 200 (application/json)

    [Reset Password][]             

    
## getUserInfo [/getUserInfo]

send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                "status": 200,
                "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                "user": {
                    "id": 2,
                    "fullName": "somaya hegab",
                    "userName": "somaya",
                    "phone": null,
                    "photo": null,
                    "activeCode": "143368",
                    "aboutYourSelf": "3",
                    "yearsOfExperience": 3,
                    "birthDate": "1993-06-01",
                    "role": 0,
                    "gender": 2,
                    "notifiable": 1,
                    "email": "somayahegab@gmail.com",
                    "email_verified_at": "2020-06-19 19:23:27",
                    "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                    "country_id": 1,
                    "city_id": 1,
                    "provider_id": null,
                    "educationDegree_id": 1,
                    "specialization": "الهندسه",
                    "deleted_at": null,
                    "created_at": "2020-06-19 17:05:01",
                    "updated_at": "2020-06-20 15:36:42",
                    "questionNumbers": 2,
                    "answerNumbers": 4,
                    "replyNumbers": 0,
                    "country": {
                        "id": 1,
                        "name": "السعودية"
                    },
                    "city": {
                        "id": 1,
                        "name": "جده"
                    },
                    "education_degree": {
                        "id": 1,
                        "name": "ماجستير"
                    }
                }
            }
### getUserInfo [GET]

+ Response 200 (application/json)

    [getUserInfo][] 
    
## getUserProfile [/getUserProfile/{id}]

+ Parameters

     + id (required, integer) - id of user

+ Model

    + Body

            {
                "status": 200,
                "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                "user": {
                    "id": 2,
                    "fullName": "somaya hegab",
                    "userName": "somaya",
                    "phone": null,
                    "photo": null,
                    "activeCode": "143368",
                    "aboutYourSelf": "3",
                    "yearsOfExperience": 3,
                    "birthDate": "1993-06-01",
                    "role": 0,
                    "gender": 2,
                    "notifiable": 1,
                    "email": "somayahegab@gmail.com",
                    "email_verified_at": "2020-06-19 19:23:27",
                    "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                    "country_id": 1,
                    "city_id": 1,
                    "provider_id": null,
                    "educationDegree_id": 1,
                    "specialization": "الهندسه",
                    "deleted_at": null,
                    "created_at": "2020-06-19 17:05:01",
                    "updated_at": "2020-06-20 15:36:42",
                    "questionNumbers": 2,
                    "answerNumbers": 4,
                    "replyNumbers": 0,
                    "country": {
                        "id": 1,
                        "name": "السعودية"
                    },
                    "city": {
                        "id": 1,
                        "name": "جده"
                    },
                    "education_degree": {
                        "id": 1,
                        "name": "ماجستير"
                    }
                }
            }

### getUserProfile [GET]

+ Response 200 (application/json)

    [getUserProfile][] 
           
## logout [/logout]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                "status": 200,
                "success": "user logout successfully"
            }

### logout [GET]

+ Response 200 (application/json)

    [logout][]
     
               
    
## Update Password [/updatePassword]

+ Model

    + Body

            {
                "status": 200,
                "success": "password updated successfully."
            }
### Update Password [POST]



+ Request (application/json)

    + Body

            {


          
            	"old_password" 124521,
            	"new_password":"123456",
            	"token":"462d006c132170e104673592216573e8f23f0213"
            }




+ Response 200 (application/json)

    [Update Password][]

## Edit User Info [/editUserInfo]

+ Model

    + Body

            {
                "status": 200,
                "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                "user": {
                    "id": 2,
                    "fullName": "somaya hegab",
                    "userName": "somaya",
                    "phone": null,
                    "photo": null,
                    "activeCode": "143368",
                    "aboutYourSelf": "3",
                    "yearsOfExperience": 3,
                    "birthDate": "1993-06-01",
                    "role": 0,
                    "gender": 2,
                    "notifiable": 1,
                    "email": "somayahegab@gmail.com",
                    "email_verified_at": "2020-06-19 19:23:27",
                    "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                    "country_id": 1,
                    "city_id": 1,
                    "provider_id": null,
                    "educationDegree_id": 1,
                    "specialization": "الهندسه",
                    "deleted_at": null,
                    "created_at": "2020-06-19 17:05:01",
                    "updated_at": "2020-06-20 15:36:42",
                    "questionNumbers": 2,
                    "answerNumbers": 4,
                    "replyNumbers": 0,
                    "country": {
                        "id": 1,
                        "name": "السعودية"
                    },
                    "city": {
                        "id": 1,
                        "name": "جده"
                    },
                    "education_degree": {
                        "id": 1,
                        "name": "ماجستير"
                    }
                }
            }
### Edit User Info [POST]



+ Request (application/json)

    + Body

            {


          
            	"fullName" "somaya",
            	"userName" "hegab",
            	"phone":"123456",
            	"photo":photo file,
            	"specialization":1,
            	"country_id":1,
            	"city_id":1,
            	"educationDegree_id":1,
            	"yearsOfExperience":1,
            	"aboutYourSelf":"dummy data",
            	"email":"somaia@gmail.com"
            	"token":"462d006c132170e104673592216573e8f23f0213"
            }




+ Response 200 (application/json)

    [Edit User Info][] 
    
    

     
## turnOnOffNotification [/turnOnOffNotification]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                "status": 200,
                "success": "Notifications Turn OFF"
            }
### turnOnOffNotification [GET]

+ Response 200 (application/json)

    [turnOnOffNotification][]
               
# Group Lists
This group for lists       

## get Countries [/getCountries]



+ Model

    + Body

            {
                "status": 200,
                "countries": [
                    {
                        "id": 1,
                        "name": "السعودية"
                    }
                ]
            }

### get Countries [GET]

+ Response 200 (application/json)

    [get Countries][]
    

## get Cities [/getCity/{id}]

+ Parameters

     + id (required, integer) - id of area

+ Model

    + Body

            {
                "status": 200,
                "cities": [
                    {
                        "id": 1,
                        "name": "جده"
                    },
                    {
                        "id": 2,
                        "name": "الرياض"
                    }
                ]
            }

### get Cities [GET]

+ Response 200 (application/json)

    [get Cities][]     
    
## get Categories [/getCategories]



+ Model

    + Body

            {
                "status": 200,
                "categories": [
                    {
                        "id": 1,
                        "name": "الطب",
                        "photo": "/uploads/categories/lcvpauI1Wa.jpeg"
                    },
                    {
                        "id": 2,
                        "name": "الهندسه",
                        "photo": "/uploads/categories/LdDs4nZWGQ.jpeg"
                    },
                    {
                        "id": 3,
                        "name": "افراح",
                        "photo": "/uploads/categories/UuKwWH3QjW.jpeg"
                    }
                ]
            }

### get Categories [GET]

+ Response 200 (application/json)

    [get Categories][]
    
## get Education Degrees [/getEducationDegrees]


+ Model

    + Body

            {
                "status": 200,
                "educationDegrees": [
                    {
                        "id": 1,
                        "name": "ماجستير"
                    },
                    {
                        "id": 2,
                        "name": "دكتوراه"
                    }
                ]
            }

### get Education Degrees [GET]

+ Response 200 (application/json)

    [get Education Degrees][]        
      
# Group  cars
This group for questions
      
## add question [/questions/create]

+ Model

    + Body

            {
                "status": 200,
                "success": "question added Successfully"
            }
### add question [POST]



+ Request (application/json)

    + Body

            {


            	"token":"e8d8ff905b214b6ab0a9d08da0706bfd40582395"
                "cityIds[0]":1
                "cityIds[1]":2
                "questionText":ماهو صوت الاسد؟
                "category_id":1,
                "photo":file
              	
            }




+ Response 200 (application/json)

    [add question][]
    
## add Report [/addReport]

+ Model

    + Body

            {
                "status": 200,
                "success": "Report added Successfully"
            }
### add Report [POST]



+ Request (application/json)

    + Body

            {


            	"token":"e8d8ff905b214b6ab0a9d08da0706bfd40582395"
                "question_id":1
                "message": "غير لائق"
              	
            }




+ Response 200 (application/json)

    [add Report][]    
    

    
## questions list [/questions]

send in headers token parameter with logged in user token  value


+ Model

    + Body

            {
                "status": 200,
                "questions": {
                    "current_page": 1,
                    "data": [
                        {
                            "id": 3,
                            "questionText": "ماهو صوت الاسد؟",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-29 19:53:40",
                            "answerNumbers": 0,
                            "likeNumbers": 0,
                            "saved": false,
                            "liked": false,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null,
                                "specialization": "الهندسه",
                                "educationDegree_id": 1,
                                "education_degree": {
                                    "id": 1,
                                    "name": "ماجستير"
                                }
                            },
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        },
                        {
                            "id": 2,
                            "questionText": "ماهو صوت الاسد؟",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-25 18:24:14",
                            "answerNumbers": 0,
                            "likeNumbers": 0,
                            "saved": false,
                            "liked": false,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null,
                                "specialization": "الهندسه",
                                "educationDegree_id": 1,
                                "education_degree": {
                                    "id": 1,
                                    "name": "ماجستير"
                                }
                            },
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        },
                        {
                            "id": 1,
                            "questionText": "ماهو صوت الكلب",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-23 00:00:00",
                            "answerNumbers": 4,
                            "likeNumbers": 1,
                            "saved": false,
                            "liked": false,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null,
                                "specialization": "الهندسه",
                                "educationDegree_id": 1,
                                "education_degree": {
                                    "id": 1,
                                    "name": "ماجستير"
                                }
                            },
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        }
                    ],
                    "first_page_url": "http://localhost:8000/api/questions?page=1",
                    "from": 1,
                    "last_page": 1,
                    "last_page_url": "http://localhost:8000/api/questions?page=1",
                    "next_page_url": null,
                    "path": "http://localhost:8000/api/questions",
                    "per_page": 5,
                    "prev_page_url": null,
                    "to": 3,
                    "total": 3
                }
            }

### questions list [GET]

+ Response 200 (application/json)

    [questions list][]
## category Questions [/categoryQuestions/{id}]
send in headers token parameter with logged in user token  value

+ Parameters

     + id (required, integer) - id of category



+ Model

    + Body

            {
                "status": 200,
                "questions": {
                    "current_page": 1,
                    "data": [
                        {
                            "id": 3,
                            "questionText": "ماهو صوت الاسد؟",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-29 19:53:40",
                            "answerNumbers": 0,
                            "likeNumbers": 0,
                            "saved": false,
                            "liked": false,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null,
                                "specialization": "الهندسه",
                                "educationDegree_id": 1,
                                "education_degree": {
                                    "id": 1,
                                    "name": "ماجستير"
                                }
                            },
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        },
                        {
                            "id": 2,
                            "questionText": "ماهو صوت الاسد؟",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-25 18:24:14",
                            "answerNumbers": 0,
                            "likeNumbers": 0,
                            "saved": false,
                            "liked": false,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null,
                                "specialization": "الهندسه",
                                "educationDegree_id": 1,
                                "education_degree": {
                                    "id": 1,
                                    "name": "ماجستير"
                                }
                            },
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        },
                        {
                            "id": 1,
                            "questionText": "ماهو صوت الكلب",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-23 00:00:00",
                            "answerNumbers": 4,
                            "likeNumbers": 1,
                            "saved": true,
                            "liked": true,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null,
                                "specialization": "الهندسه",
                                "educationDegree_id": 1,
                                "education_degree": {
                                    "id": 1,
                                    "name": "ماجستير"
                                }
                            },
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        }
                    ],
                    "first_page_url": "http://localhost:8000/api/categoryQuestions/1/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1",
                    "from": 1,
                    "last_page": 1,
                    "last_page_url": "http://localhost:8000/api/categoryQuestions/1/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1",
                    "next_page_url": null,
                    "path": "http://localhost:8000/api/categoryQuestions/1/e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                    "per_page": 5,
                    "prev_page_url": null,
                    "to": 3,
                    "total": 3
                }
            }
### category Questions [GET]

+ Response 200 (application/json)

    [category Questions][]    
    
## question details [/question/{id}]
send in headers token parameter with logged in user token  value

+ Parameters

     + id (required, integer) - id of question
     

+ Model

    + Body

            {
                "status": 200,
                "question": {
                    "id": 1,
                    "questionText": "ماهو صوت الكلب",
                    "photo": null,
                    "user_id": 2,
                    "category_id": 1,
                    "created_at": "2020-06-23 00:00:00",
                    "answerNumbers": 4,
                    "likeNumbers": 1,
                    "liked": true,
                    "saved": true,
                    "answers": [
                        {
                            "id": 2,
                            "answerText": "نهيق",
                            "photo": null,
                            "user_id": 2,
                            "created_at": "2020-06-25 16:46:50",
                            "replyNumbers": 0,
                            "likeNumbers": 1,
                            "liked": true,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null
                            }
                        },
                        {
                            "id": 3,
                            "answerText": "نهيق",
                            "photo": null,
                            "user_id": 2,
                            "created_at": "2020-06-25 16:47:34",
                            "replyNumbers": 0,
                            "likeNumbers": 0,
                            "liked": false,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null
                            }
                        },
                        {
                            "id": 4,
                            "answerText": "نهيق",
                            "photo": null,
                            "user_id": 2,
                            "created_at": "2020-06-28 12:08:31",
                            "replyNumbers": 0,
                            "likeNumbers": 0,
                            "liked": false,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null
                            }
                        },
                        {
                            "id": 5,
                            "answerText": "klkkgl",
                            "photo": null,
                            "user_id": 2,
                            "created_at": "2020-06-28 12:37:18",
                            "replyNumbers": 0,
                            "likeNumbers": 0,
                            "liked": false,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null
                            }
                        }
                    ],
                    "user": {
                        "id": 2,
                        "fullName": "somaya hegab",
                        "photo": null,
                        "specialization": "الهندسه",
                        "educationDegree_id": 1,
                        "education_degree": {
                            "id": 1,
                            "name": "ماجستير"
                        }
                    },
                    "category": {
                        "id": 1,
                        "name": "الطب"
                    }
                }
            }

### question details [GET]

+ Response 200 (application/json)

    [question details][]

## my Questions [/myQuestions]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                "status": 200,
                "questions": {
                    "current_page": 1,
                    "data": [
                        {
                            "id": 3,
                            "questionText": "ماهو صوت الاسد؟",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-29 19:53:40",
                            "answerNumbers": 0,
                            "likeNumbers": 0,
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        },
                        {
                            "id": 2,
                            "questionText": "ماهو صوت الاسد؟",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-25 18:24:14",
                            "answerNumbers": 0,
                            "likeNumbers": 0,
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        },
                        {
                            "id": 1,
                            "questionText": "ماهو صوت الكلب",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-23 00:00:00",
                            "answerNumbers": 4,
                            "likeNumbers": 1,
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        }
                    ],
                    "first_page_url": "http://localhost:8000/api/myQuestions/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1",
                    "from": 1,
                    "last_page": 1,
                    "last_page_url": "http://localhost:8000/api/myQuestions/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1",
                    "next_page_url": null,
                    "path": "http://localhost:8000/api/myQuestions/e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                    "per_page": 5,
                    "prev_page_url": null,
                    "to": 3,
                    "total": 3
                }
            }
### my Questions [GET]

+ Response 200 (application/json)

    [my Questions][]        
   
    
    
## delete all my questions [/myQuestions/all]

send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                "status"=>200,
                "success"=>'All Questions Deleted Successfully'
            }

### delete all my questions [DELETE]

+ Response 200 (application/json)

    [delete all my questions][]
    
## delete one question [/myQuestions/{id}]
send in headers token parameter with logged in user token  value

+ Parameters

     + id (required, integer) - id of question

+ Model

    + Body

            {
                "status"=>200,
                "success"=>'Question Deleted Successfully'
            }
### delete one question [DELETE]

+ Response 200 (application/json)

    [delete one question][]    
        
    
    
## search questions [/search]

+ Model

    + Body

            {
                "status": 200,
                "questions": [
                    {
                        "id": 3,
                        "questionText": "ماهو صوت الاسد؟",
                        "photo": null,
                        "user_id": 2,
                        "category_id": 1,
                        "created_at": "2020-06-29 19:53:40",
                        "user": {
                            "id": 2,
                            "fullName": "somaya hegab",
                            "photo": null,
                            "specialization": "الهندسه",
                            "educationDegree_id": 1,
                            "education_degree": {
                                "id": 1,
                                "name": "ماجستير"
                            }
                        },
                        "category": {
                            "id": 1,
                            "name": "الطب"
                        }
                    },
                    {
                        "id": 2,
                        "questionText": "ماهو صوت الاسد؟",
                        "photo": null,
                        "user_id": 2,
                        "category_id": 1,
                        "created_at": "2020-06-25 18:24:14",
                        "user": {
                            "id": 2,
                            "fullName": "somaya hegab",
                            "photo": null,
                            "specialization": "الهندسه",
                            "educationDegree_id": 1,
                            "education_degree": {
                                "id": 1,
                                "name": "ماجستير"
                            }
                        },
                        "category": {
                            "id": 1,
                            "name": "الطب"
                        }
                    },
                    {
                        "id": 1,
                        "questionText": "ماهو صوت الكلب",
                        "photo": null,
                        "user_id": 2,
                        "category_id": 1,
                        "created_at": "2020-06-23 00:00:00",
                        "user": {
                            "id": 2,
                            "fullName": "somaya hegab",
                            "photo": null,
                            "specialization": "الهندسه",
                            "educationDegree_id": 1,
                            "education_degree": {
                                "id": 1,
                                "name": "ماجستير"
                            }
                        },
                        "category": {
                            "id": 1,
                            "name": "الطب"
                        }
                    }
                ]
            }
### search questions [POST]



+ Request (application/json)

    + Body

            {

            	"name":"ماهو ",
            	
            }




+ Response 200 (application/json)

    [search questions][]    
    
## filter questions [/filter]

+ Model

    + Body

            {
                "status": 200,
                "count": 3,
                "questions": {
                    "current_page": 1,
                    "data": [
                        {
                            "id": 3,
                            "questionText": "ماهو صوت الاسد؟",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-29 19:53:40",
                            "answerNumbers": 0,
                            "likeNumbers": 0,
                            "saved": false,
                            "liked": false,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null,
                                "specialization": "الهندسه",
                                "educationDegree_id": 1,
                                "education_degree": {
                                    "id": 1,
                                    "name": "ماجستير"
                                }
                            },
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        },
                        {
                            "id": 2,
                            "questionText": "ماهو صوت الاسد؟",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-25 18:24:14",
                            "answerNumbers": 0,
                            "likeNumbers": 0,
                            "saved": false,
                            "liked": false,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null,
                                "specialization": "الهندسه",
                                "educationDegree_id": 1,
                                "education_degree": {
                                    "id": 1,
                                    "name": "ماجستير"
                                }
                            },
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        },
                        {
                            "id": 1,
                            "questionText": "ماهو صوت الكلب",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-23 00:00:00",
                            "answerNumbers": 4,
                            "likeNumbers": 1,
                            "saved": false,
                            "liked": false,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null,
                                "specialization": "الهندسه",
                                "educationDegree_id": 1,
                                "education_degree": {
                                    "id": 1,
                                    "name": "ماجستير"
                                }
                            },
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        }
                    ],
                    "first_page_url": "http://localhost:8000/api/filter?page=1",
                    "from": 1,
                    "last_page": 1,
                    "last_page_url": "http://localhost:8000/api/filter?page=1",
                    "next_page_url": null,
                    "path": "http://localhost:8000/api/filter",
                    "per_page": 5,
                    "prev_page_url": null,
                    "to": 3,
                    "total": 3
                }
            }
### filter questions [POST]



+ Request (application/json)

    + Body

            {

            	"categories[0]":1,
            	"cities[0]":1,
            	"sortBy":1 for desc ,2 for asc,
            	"token":"e8d8ff905b214b6ab0a9d08da0706bfd40582395",
            	
            }




+ Response 200 (application/json)

    [filter questions][]
    
## add favourite  [/favourites/create]
this api for add or remove favourite for both question and answer 

+ Model

    + Body

            {
                "status": 200,
                "success": "Added To Favourites Successfully"
            }
### add favourite [POST]



+ Request (application/json)

    + Body

            {

            	"token":"e8d8ff905b214b6ab0a9d08da0706bfd40582395",
            	"ref_id":"id of question or answer",
            	"type":"1 for question 2 for answer",
            	
            }




+ Response 200 (application/json)

    [add favourite][]
## add to saves  [/saves/create]
this api for add or remove save question 

+ Model

    + Body

            {
                "status": 200,
                "success": "Added To saves Successfully"
            }
### add to saves [POST]



+ Request (application/json)

    + Body

            {

            	"token":"e8d8ff905b214b6ab0a9d08da0706bfd40582395",
            	"question_id":"id of question or answer",
            	
            }




+ Response 200 (application/json)

    [add to saves][]    
            
## get saves [/saves]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                "status": 200,
                "questions": {
                    "current_page": 1,
                    "data": [
                        {
                            "id": 1,
                            "questionText": "ماهو صوت الكلب",
                            "photo": null,
                            "user_id": 2,
                            "category_id": 1,
                            "created_at": "2020-06-23 00:00:00",
                            "answerNumbers": 4,
                            "likeNumbers": 1,
                            "saved": true,
                            "liked": true,
                            "user": {
                                "id": 2,
                                "fullName": "somaya hegab",
                                "photo": null,
                                "specialization": "الهندسه",
                                "educationDegree_id": 1,
                                "education_degree": {
                                    "id": 1,
                                    "name": "ماجستير"
                                }
                            },
                            "category": {
                                "id": 1,
                                "name": "الطب"
                            }
                        }
                    ],
                    "first_page_url": "http://localhost:8000/api/saves/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1",
                    "from": 1,
                    "last_page": 1,
                    "last_page_url": "http://localhost:8000/api/saves/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1",
                    "next_page_url": null,
                    "path": "http://localhost:8000/api/saves/e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                    "per_page": 5,
                    "prev_page_url": null,
                    "to": 1,
                    "total": 1
                }
            }
### get saves [GET]

+ Response 200 (application/json)

    [get saves][]    

## delete all saves [/saves/all]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                'status':200,
                'success':'All Saved Questions Deleted Successfully'
            }
### delete all saves [DELETE]

+ Response 200 (application/json)

    [delete all saves][]
        
## my answers [/myAnswers]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                "status": 200,
                "answers": {
                    "current_page": 1,
                    "data": [
                        {
                            "id": 5,
                            "answerText": "klkkgl",
                            "photo": null,
                            "user_id": 2,
                            "question_id": 1,
                            "created_at": "2020-06-28 12:37:18",
                            "question": {
                                "id": 1,
                                "questionText": "ماهو صوت الكلب",
                                "photo": null,
                                "user_id": 2,
                                "user": {
                                    "id": 2,
                                    "photo": null
                                }
                            }
                        },
                        {
                            "id": 4,
                            "answerText": "نهيق",
                            "photo": null,
                            "user_id": 2,
                            "question_id": 1,
                            "created_at": "2020-06-28 12:08:31",
                            "question": {
                                "id": 1,
                                "questionText": "ماهو صوت الكلب",
                                "photo": null,
                                "user_id": 2,
                                "user": {
                                    "id": 2,
                                    "photo": null
                                }
                            }
                        },
                        {
                            "id": 3,
                            "answerText": "نهيق",
                            "photo": null,
                            "user_id": 2,
                            "question_id": 1,
                            "created_at": "2020-06-25 16:47:34",
                            "question": {
                                "id": 1,
                                "questionText": "ماهو صوت الكلب",
                                "photo": null,
                                "user_id": 2,
                                "user": {
                                    "id": 2,
                                    "photo": null
                                }
                            }
                        },
                        {
                            "id": 2,
                            "answerText": "نهيق",
                            "photo": null,
                            "user_id": 2,
                            "question_id": 1,
                            "created_at": "2020-06-25 16:46:50",
                            "question": {
                                "id": 1,
                                "questionText": "ماهو صوت الكلب",
                                "photo": null,
                                "user_id": 2,
                                "user": {
                                    "id": 2,
                                    "photo": null
                                }
                            }
                        }
                    ],
                    "first_page_url": "http://localhost:8000/api/myAnswers/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1",
                    "from": 1,
                    "last_page": 1,
                    "last_page_url": "http://localhost:8000/api/myAnswers/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1",
                    "next_page_url": null,
                    "path": "http://localhost:8000/api/myAnswers/e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                    "per_page": 5,
                    "prev_page_url": null,
                    "to": 4,
                    "total": 4
                }
            }
### my answers [GET]

+ Response 200 (application/json)

    [my answers][]
            
## delete all my answers [/myAnswers/all]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                'status':200,
                'success':'All my answers  Deleted Successfully'
            }
### delete all my answers [DELETE]

+ Response 200 (application/json)

    [delete all my answers][] 
       
## delete one answer [/myAnswers/{id}]
send in headers token parameter with logged in user token  value

+ Parameters

     + id (required, integer) - id of answer

+ Model

    + Body

            {
                'status':200,
                'success':'my answer  Deleted Successfully'
            }
### delete one answer [DELETE]

+ Response 200 (application/json)

    [delete one answer][]
 
           
## add answer [/myAnswers/create]

+ Model

    + Body

            {
                "status": 200,
                "success": "Answer Added  Successfully"
            }
### add answer [POST]



+ Request (application/json)

    + Body

            {

            	"token":"e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                "question_id":"id of question or answer",
                "answerText":"صوت الكلب هو ",
                "photo":"optional ",
                "saveAsDraft":"1 for draft ,0 for publish ",
            	
            }




+ Response 200 (application/json)

    [add answer][]
    
## add reply [/replies/create]

+ Model

    + Body

            {
                "status": 200,
                "success": "reply Added  Successfully"
            }
### add reply [POST]



+ Request (application/json)

    + Body

            {

            	"token":"e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                "answer_id":"id of question or answer",
                "replyText":"صوت الكلب هو ",
            	
            }




+ Response 200 (application/json)

    [add reply][]    
## publish draft [/myDrafts/create]

+ Model

    + Body

            {
                "status": 200,
                "success": "Draft Published Successfully"
            }
### publish draft [POST]



+ Request (application/json)

    + Body

            {

            	"token":"e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                "draft_id":"id of question or answer",
   
            	
            }




+ Response 200 (application/json)

    [publish draft][]
         
## my drafts [/myDrafts]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                "status": 200,
                "drafts": {
                    "current_page": 1,
                    "data": [
                        {
                            "id": 2,
                            "answerText": "نهيق",
                            "photo": null,
                            "user_id": 2,
                            "question_id": 1,
                            "created_at": "2020-06-25 16:46:50",
                            "question": {
                                "id": 1,
                                "questionText": "ماهو صوت الكلب",
                                "photo": null,
                                "user_id": 2,
                                "user": {
                                    "id": 2,
                                    "photo": null
                                }
                            }
                        }
                    ],
                    "first_page_url": "http://localhost:8000/api/myDrafts/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1",
                    "from": 1,
                    "last_page": 1,
                    "last_page_url": "http://localhost:8000/api/myDrafts/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1",
                    "next_page_url": null,
                    "path": "http://localhost:8000/api/myDrafts/e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                    "per_page": 5,
                    "prev_page_url": null,
                    "to": 1,
                    "total": 1
                }
            }
### my drafts [GET]

+ Response 200 (application/json)

    [my drafts][]
            
## delete all my drafts [/myDrafts/all]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                'status':200,
                'success':'All my Drafts  Deleted Successfully'
            }
### delete all my drafts [DELETE]

+ Response 200 (application/json)

    [delete all my drafts][]
     
## answer replies [/replies/{id}]

+ Parameters

     + id (required, integer) - id of answer

+ Model

    + Body

            {
                "status": 200,
                "replies": [
                    {
                        "id": 1,
                        "replyText": "اجابه خاطئه",
                        "user_id": 4,
                        "created_at": "2020-06-30 00:00:00",
                        "user": {
                            "id": 4,
                            "fullName": "fatma ali",
                            "photo": null
                        }
                    }
                ]
            }
### answer replies [GET]

+ Response 200 (application/json)

    [answer replies][]
            
# Group  Message
This group for messages    
        
    
## send message [/sendMessage]

+ Model

    + Body

            {
                "status": 200,
                "success": "Message sent Successfully"
            }
### send message [POST]



+ Request (application/json)

    + Body

            {

            	"token":"ghfgghyuttyhjthj",
            	"receiver_id":"id of receiver",
            	"message":"text can be option in case send image or record",
            	"media_file":"array of images or file of record",
            	"extension":"extension of file",
            	"type":"send 1 for image, send 2 for record , 3 for file",
              	
            }




+ Response 200 (application/json)

    [send message][]
    
 
           
## get my chats [/getMyChats]

send in headers token parameter with logged in user token  value 


+ Model

    + Body

            {
                "status": 200,
                "messages": [
                    {
                        "id": 6,
                        "message": "بمناسبه سؤالك ..",
                        "sender_id": 3,
                        "receiver_id": 2,
                        "seen": 1,
                        "created_at": "2020-06-26 01:07:00",
                        "updated_at": "2020-06-26 13:39:31",
                        "sender": {
                            "id": 3,
                            "fullName": "fatma ali",
                            "token": "9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f",
                            "photo": null,
                            "educationDegree_id": 1,
                            "specialization": null,
                            "education_degree": {
                                "id": 1,
                                "name": "ماجستير"
                            }
                        },
                        "since": "منذ أسبوعين"
                    },
                    {
                        "id": 14,
                        "message": "vnhbhbhm",
                        "sender_id": 2,
                        "receiver_id": 3,
                        "seen": 0,
                        "created_at": "2020-07-11 20:29:40",
                        "updated_at": "2020-07-11 20:29:40",
                        "sender": {
                            "id": 3,
                            "fullName": "fatma ali",
                            "token": "9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f",
                            "photo": null,
                            "educationDegree_id": 1,
                            "specialization": null,
                            "education_degree": {
                                "id": 1,
                                "name": "ماجستير"
                            }
                        },
                        "since": "منذ 34 دقيقة"
                    }
                ]
            }
### get my chats [GET]

+ Response 200 (application/json)

    [get my chats][]
    
## get chat details [/getChatDetails/{id}]
send in headers token parameter with logged in user token  value

+ Parameters

     + id (required, integer) - id of another user


+ Model

    + Body

            {
                "status": "200",
                "messages": [
                    {
                        "id": 4,
                        "message": "السلام عليكم ",
                        "sender_id": 3,
                        "receiver_id": 2,
                        "seen": 1,
                        "created_at": "2020-06-26 01:05:00",
                        "updated_at": "2020-06-26 13:39:31",
                        "sender": {
                            "id": 3,
                            "fullName": "fatma ali",
                            "token": "9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f",
                            "photo": null
                        },
                        "medias": []
                    },
                    {
                        "id": 5,
                        "message": "وعليكم السلام  ",
                        "sender_id": 2,
                        "receiver_id": 3,
                        "seen": 0,
                        "created_at": "2020-06-26 01:06:00",
                        "updated_at": "2020-06-26 00:00:00",
                        "sender": {
                            "id": 2,
                            "fullName": "somaya hegab",
                            "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                            "photo": null
                        },
                        "medias": []
                    },
                    {
                        "id": 6,
                        "message": "بمناسبه سؤالك ..",
                        "sender_id": 3,
                        "receiver_id": 2,
                        "seen": 1,
                        "created_at": "2020-06-26 01:07:00",
                        "updated_at": "2020-06-26 13:39:31",
                        "sender": {
                            "id": 3,
                            "fullName": "fatma ali",
                            "token": "9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f",
                            "photo": null
                        },
                        "medias": []
                    },
                    {
                        "id": 7,
                        "message": "السلام عليكم",
                        "sender_id": 2,
                        "receiver_id": 3,
                        "seen": 0,
                        "created_at": "2020-06-27 12:50:56",
                        "updated_at": "2020-06-27 12:50:56",
                        "sender": {
                            "id": 2,
                            "fullName": "somaya hegab",
                            "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                            "photo": null
                        },
                        "medias": []
                    },
                    {
                        "id": 8,
                        "message": "السلام عليكم",
                        "sender_id": 2,
                        "receiver_id": 3,
                        "seen": 0,
                        "created_at": "2020-06-27 12:53:41",
                        "updated_at": "2020-06-27 12:53:41",
                        "sender": {
                            "id": 2,
                            "fullName": "somaya hegab",
                            "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                            "photo": null
                        },
                        "medias": []
                    },
                    {
                        "id": 9,
                        "message": "vnhbhbhm",
                        "sender_id": 2,
                        "receiver_id": 3,
                        "seen": 0,
                        "created_at": "2020-07-11 20:20:41",
                        "updated_at": "2020-07-11 20:20:41",
                        "sender": {
                            "id": 2,
                            "fullName": "somaya hegab",
                            "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                            "photo": null
                        },
                        "medias": []
                    },
                    {
                        "id": 10,
                        "message": "vnhbhbhm",
                        "sender_id": 2,
                        "receiver_id": 3,
                        "seen": 0,
                        "created_at": "2020-07-11 20:23:12",
                        "updated_at": "2020-07-11 20:23:12",
                        "sender": {
                            "id": 2,
                            "fullName": "somaya hegab",
                            "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                            "photo": null
                        },
                        "medias": []
                    },
                    {
                        "id": 11,
                        "message": "vnhbhbhm",
                        "sender_id": 2,
                        "receiver_id": 3,
                        "seen": 0,
                        "created_at": "2020-07-11 20:23:29",
                        "updated_at": "2020-07-11 20:23:29",
                        "sender": {
                            "id": 2,
                            "fullName": "somaya hegab",
                            "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                            "photo": null
                        },
                        "medias": []
                    },
                    {
                        "id": 12,
                        "message": "vnhbhbhm",
                        "sender_id": 2,
                        "receiver_id": 3,
                        "seen": 0,
                        "created_at": "2020-07-11 20:24:31",
                        "updated_at": "2020-07-11 20:24:31",
                        "sender": {
                            "id": 2,
                            "fullName": "somaya hegab",
                            "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                            "photo": null
                        },
                        "medias": []
                    },
                    {
                        "id": 13,
                        "message": "vnhbhbhm",
                        "sender_id": 2,
                        "receiver_id": 3,
                        "seen": 0,
                        "created_at": "2020-07-11 20:25:43",
                        "updated_at": "2020-07-11 20:25:43",
                        "sender": {
                            "id": 2,
                            "fullName": "somaya hegab",
                            "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                            "photo": null
                        },
                        "medias": []
                    },
                    {
                        "id": 14,
                        "message": "vnhbhbhm",
                        "sender_id": 2,
                        "receiver_id": 3,
                        "seen": 0,
                        "created_at": "2020-07-11 20:29:40",
                        "updated_at": "2020-07-11 20:29:40",
                        "sender": {
                            "id": 2,
                            "fullName": "somaya hegab",
                            "token": "e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                            "photo": null
                        },
                        "medias": [
                            {
                                "message_id": 14,
                                "media_file": "/uploads/medias/0bdf32bf463c0a0ca26920200711202940.jpg",
                                "extension": "jpg",
                                "type": 1
                            }
                        ]
                    }
                ]
            }
### get chat details [GET]

+ Response 200 (application/json)

    [get chat details][]
    
## delete all my chats [/deleteAllChats]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                'status':200,
                'success':'All my chats  Deleted Successfully'
            }
### delete all my chats [DELETE]

+ Response 200 (application/json)

    [delete all my chats][]
    
## delete one chat [/deleteOneChat/{id}]
send in headers token parameter with logged in user token  value

+ Parameters

     + id (required, integer) - id of another user

+ Model

    + Body

            {
                'status':200,
                'success':'my chat Deleted Successfully'
            }
### delete one chat [DELETE]

+ Response 200 (application/json)

    [delete one chat][]                      


# Group  notification
This group for notification



## get notification [/notifications]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                "status": 200,
                "notes": {
                    "current_page": 1,
                    "data": [
                        {
                            "unique_id": 2,
                            "data": {
                                "message": "قام العضو somaya hegab بالاجابه على سؤالك",
                                "question_id": 1
                            },
                            "type": "answerQuestion",
                            "created_at": "2020-06-30 13:51:19",
                            "message": "قام العضو somaya hegab بالاجابه على سؤالك",
                            "time": "01:51 pm"
                        },
                        {
                            "unique_id": 1,
                            "data": {
                                "message": "قام العضو somaya hegab بالاعجاب باجابتك",
                                "answer_id": 2
                            },
                            "type": "likeAnswer",
                            "created_at": "2020-06-29 21:02:57",
                            "message": "قام العضو somaya hegab بالاعجاب باجابتك",
                            "time": "09:02 pm"
                        }
                    ],
                    "first_page_url": "http://localhost:8000/api/notifications/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1",
                    "from": 1,
                    "last_page": 1,
                    "last_page_url": "http://localhost:8000/api/notifications/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1",
                    "next_page_url": null,
                    "path": "http://localhost:8000/api/notifications/e8d8ff905b214b6ab0a9d08da0706bfd40582395",
                    "per_page": 10,
                    "prev_page_url": null,
                    "to": 2,
                    "total": 2
                }
            }

### get notification [GET]

+ Response 200 (application/json)

    [get notification][]
## unread notification count [/unreadNotifications]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                "status": 200,
                "count": 2
            }

### unread notification count [GET]

+ Response 200 (application/json)

    [unread notification count][]

## delete notification [/deleteNotification]
send in headers token parameter with logged in user token  value

+ Model

    + Body

            {
                "status": 200,
                "success": "deleted successfully"
            }

### delete notification [GET]

+ Response 200 (application/json)

    [delete notification][]
    
## delete one notification [/deleteOneNotification/{id}]

+ Parameters

    + id (required, string) - unique id 
+ Model

    + Body

            {
                "status": 200,
                "success": "deleted successfully"
            }

### delete one notification [GET]

+ Response 200 (application/json)

    [delete one notification][]             
    
