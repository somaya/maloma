<?php

namespace App\Http\Controllers\admin;

use App\Models\Report;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportsController extends Controller
{
    public function index()
    {
        $reports=Report::all();
//        dd($reports[0]);

        return view('admin.reports.index',compact('reports'));
    }
    public function show($id)
    {
        $report=Report::where('id',$id)->first();
        return view('admin.reports.show',compact('report'));
    }
    public function destroy($id)
    {
        $report=Report::where('id',$id)->first();

        $report->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));
    }
}
