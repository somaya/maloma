
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الاسم: </label>
    <div class="col-lg-10{{ $errors->has('name') ? ' has-danger' : '' }}">
        {!! Form::text('name',old('name'),['class'=>'form-control m-input','autofocus','placeholder'=> 'الاسم ' ]) !!}
        @if ($errors->has('name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الصورة : </label>
    <div class="col-lg-10{{ $errors->has('photo') ? ' has-danger' : '' }}">

        <input type="file" name="photo"   class="form-control uploadinput">
        @if ($errors->has('photo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($category) && $category->photo)
<div class="row">




            <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

                <img
                        data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                        alt="First slide [800x4a00]"
                        src="{{$category->photo}}"
                        style="height: 150px; width: 150px"
                        data-holder-rendered="true">
            </div>

</div>
@endif





