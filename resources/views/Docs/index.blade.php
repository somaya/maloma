<!DOCTYPE html><html><head><meta charset="utf-8"><title>Maloma API</title><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"><style>@import url('https://fonts.googleapis.com/css?family=Roboto:400,700|Inconsolata|Raleway:200');.hljs-comment,.hljs-title{color:#8e908c}.hljs-variable,.hljs-attribute,.hljs-tag,.hljs-regexp,.ruby .hljs-constant,.xml .hljs-tag .hljs-title,.xml .hljs-pi,.xml .hljs-doctype,.html .hljs-doctype,.css .hljs-id,.css .hljs-class,.css .hljs-pseudo{color:#c82829}.hljs-number,.hljs-preprocessor,.hljs-pragma,.hljs-built_in,.hljs-literal,.hljs-params,.hljs-constant{color:#f5871f}.ruby .hljs-class .hljs-title,.css .hljs-rules .hljs-attribute{color:#eab700}.hljs-string,.hljs-value,.hljs-inheritance,.hljs-header,.ruby .hljs-symbol,.xml .hljs-cdata{color:#718c00}.css .hljs-hexcolor{color:#3e999f}.hljs-function,.python .hljs-decorator,.python .hljs-title,.ruby .hljs-function .hljs-title,.ruby .hljs-title .hljs-keyword,.perl .hljs-sub,.javascript .hljs-title,.coffeescript .hljs-title{color:#4271ae}.hljs-keyword,.javascript .hljs-function{color:#8959a8}.hljs{display:block;background:white;color:#4d4d4c;padding:.5em}.coffeescript .javascript,.javascript .xml,.tex .hljs-formula,.xml .javascript,.xml .vbscript,.xml .css,.xml .hljs-cdata{opacity:.5}.right .hljs-comment{color:#969896}.right .css .hljs-class,.right .css .hljs-id,.right .css .hljs-pseudo,.right .hljs-attribute,.right .hljs-regexp,.right .hljs-tag,.right .hljs-variable,.right .html .hljs-doctype,.right .ruby .hljs-constant,.right .xml .hljs-doctype,.right .xml .hljs-pi,.right .xml .hljs-tag .hljs-title{color:#c66}.right .hljs-built_in,.right .hljs-constant,.right .hljs-literal,.right .hljs-number,.right .hljs-params,.right .hljs-pragma,.right .hljs-preprocessor{color:#de935f}.right .css .hljs-rule .hljs-attribute,.right .ruby .hljs-class .hljs-title{color:#f0c674}.right .hljs-header,.right .hljs-inheritance,.right .hljs-name,.right .hljs-string,.right .hljs-value,.right .ruby .hljs-symbol,.right .xml .hljs-cdata{color:#b5bd68}.right .css .hljs-hexcolor,.right .hljs-title{color:#8abeb7}.right .coffeescript .hljs-title,.right .hljs-function,.right .javascript .hljs-title,.right .perl .hljs-sub,.right .python .hljs-decorator,.right .python .hljs-title,.right .ruby .hljs-function .hljs-title,.right .ruby .hljs-title .hljs-keyword{color:#81a2be}.right .hljs-keyword,.right .javascript .hljs-function{color:#b294bb}.right .hljs{display:block;overflow-x:auto;background:#1d1f21;color:#c5c8c6;padding:.5em;-webkit-text-size-adjust:none}.right .coffeescript .javascript,.right .javascript .xml,.right .tex .hljs-formula,.right .xml .css,.right .xml .hljs-cdata,.right .xml .javascript,.right .xml .vbscript{opacity:.5}body{color:black;background:white;font:400 14px / 1.42 'Roboto',Helvetica,sans-serif}header{border-bottom:1px solid #f2f2f2;margin-bottom:12px}h1,h2,h3,h4,h5{color:black;margin:12px 0}h1 .permalink,h2 .permalink,h3 .permalink,h4 .permalink,h5 .permalink{margin-left:0;opacity:0;transition:opacity .25s ease}h1:hover .permalink,h2:hover .permalink,h3:hover .permalink,h4:hover .permalink,h5:hover .permalink{opacity:1}.triple h1 .permalink,.triple h2 .permalink,.triple h3 .permalink,.triple h4 .permalink,.triple h5 .permalink{opacity:.15}.triple h1:hover .permalink,.triple h2:hover .permalink,.triple h3:hover .permalink,.triple h4:hover .permalink,.triple h5:hover .permalink{opacity:.15}h1{font:200 36px 'Raleway',Helvetica,sans-serif;font-size:36px}h2{font:200 36px 'Raleway',Helvetica,sans-serif;font-size:30px}h3{font-size:100%;text-transform:uppercase}h5{font-size:100%;font-weight:normal}p{margin:0 0 10px}p.choices{line-height:1.6}a{color:#428bca;text-decoration:none}li p{margin:0}hr.split{border:0;height:1px;width:100%;padding-left:6px;margin:12px auto;background-image:linear-gradient(to right, rgba(0,0,0,0) 20%, rgba(0,0,0,0.2) 51.4%, rgba(255,255,255,0.2) 51.4%, rgba(255,255,255,0) 80%)}dl dt{float:left;width:130px;clear:left;text-align:right;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;font-weight:700}dl dd{margin-left:150px}blockquote{color:rgba(0,0,0,0.5);font-size:15.5px;padding:10px 20px;margin:12px 0;border-left:5px solid #e8e8e8}blockquote p:last-child{margin-bottom:0}pre{background-color:#f5f5f5;padding:12px;border:1px solid #cfcfcf;border-radius:6px;overflow:auto}pre code{color:black;background-color:transparent;padding:0;border:none}code{color:#444;background-color:#f5f5f5;font:'Inconsolata',monospace;padding:1px 4px;border:1px solid #cfcfcf;border-radius:3px}ul,ol{padding-left:2em}table{border-collapse:collapse;border-spacing:0;margin-bottom:12px}table tr:nth-child(2n){background-color:#fafafa}table th,table td{padding:6px 12px;border:1px solid #e6e6e6}.text-muted{opacity:.5}.note,.warning{padding:.3em 1em;margin:1em 0;border-radius:2px;font-size:90%}.note h1,.warning h1,.note h2,.warning h2,.note h3,.warning h3,.note h4,.warning h4,.note h5,.warning h5,.note h6,.warning h6{font-family:200 36px 'Raleway',Helvetica,sans-serif;font-size:135%;font-weight:500}.note p,.warning p{margin:.5em 0}.note{color:black;background-color:#f0f6fb;border-left:4px solid #428bca}.note h1,.note h2,.note h3,.note h4,.note h5,.note h6{color:#428bca}.warning{color:black;background-color:#fbf1f0;border-left:4px solid #c9302c}.warning h1,.warning h2,.warning h3,.warning h4,.warning h5,.warning h6{color:#c9302c}header{margin-top:24px}nav{position:fixed;top:24px;bottom:0;overflow-y:auto}nav .resource-group{padding:0}nav .resource-group .heading{position:relative}nav .resource-group .heading .chevron{position:absolute;top:12px;right:12px;opacity:.5}nav .resource-group .heading a{display:block;color:black;opacity:.7;border-left:2px solid transparent;margin:0}nav .resource-group .heading a:hover{text-decoration:none;background-color:bad-color;border-left:2px solid black}nav ul{list-style-type:none;padding-left:0}nav ul a{display:block;font-size:13px;color:rgba(0,0,0,0.7);padding:8px 12px;border-top:1px solid #d9d9d9;border-left:2px solid transparent;overflow:hidden;text-overflow:ellipsis;white-space:nowrap}nav ul a:hover{text-decoration:none;background-color:bad-color;border-left:2px solid black}nav ul>li{margin:0}nav ul>li:first-child{margin-top:-12px}nav ul>li:last-child{margin-bottom:-12px}nav ul ul a{padding-left:24px}nav ul ul li{margin:0}nav ul ul li:first-child{margin-top:0}nav ul ul li:last-child{margin-bottom:0}nav>div>div>ul>li:first-child>a{border-top:none}.preload *{transition:none !important}.pull-left{float:left}.pull-right{float:right}.badge{display:inline-block;float:right;min-width:10px;min-height:14px;padding:3px 7px;font-size:12px;color:#000;background-color:#f2f2f2;border-radius:10px;margin:-2px 0}.badge.get{color:#70bbe1;background-color:#d9edf7}.badge.head{color:#70bbe1;background-color:#d9edf7}.badge.options{color:#70bbe1;background-color:#d9edf7}.badge.put{color:#f0db70;background-color:#fcf8e3}.badge.patch{color:#f0db70;background-color:#fcf8e3}.badge.post{color:#93cd7c;background-color:#dff0d8}.badge.delete{color:#ce8383;background-color:#f2dede}.collapse-button{float:right}.collapse-button .close{display:none;color:#428bca;cursor:pointer}.collapse-button .open{color:#428bca;cursor:pointer}.collapse-button.show .close{display:inline}.collapse-button.show .open{display:none}.collapse-content{max-height:0;overflow:hidden;transition:max-height .3s ease-in-out}nav{width:220px}.container{max-width:940px;margin-left:auto;margin-right:auto}.container .row .content{margin-left:244px;width:696px}.container .row:after{content:'';display:block;clear:both}.container-fluid nav{width:22%}.container-fluid .row .content{margin-left:24%}.container-fluid.triple nav{width:16.5%;padding-right:1px}.container-fluid.triple .row .content{position:relative;margin-left:16.5%;padding-left:24px}.middle:before,.middle:after{content:'';display:table}.middle:after{clear:both}.middle{box-sizing:border-box;width:51.5%;padding-right:12px}.right{box-sizing:border-box;float:right;width:48.5%;padding-left:12px}.right a{color:#428bca}.right h1,.right h2,.right h3,.right h4,.right h5,.right p,.right div{color:white}.right pre{background-color:#1d1f21;border:1px solid #1d1f21}.right pre code{color:#c5c8c6}.right .description{margin-top:12px}.triple .resource-heading{font-size:125%}.definition{margin-top:12px;margin-bottom:12px}.definition .method{font-weight:bold}.definition .method.get{color:#2e8ab8}.definition .method.head{color:#2e8ab8}.definition .method.options{color:#2e8ab8}.definition .method.post{color:#56b82e}.definition .method.put{color:#b8a22e}.definition .method.patch{color:#b8a22e}.definition .method.delete{color:#b82e2e}.definition .uri{word-break:break-all;word-wrap:break-word}.definition .hostname{opacity:.5}.example-names{background-color:#eee;padding:12px;border-radius:6px}.example-names .tab-button{cursor:pointer;color:black;border:1px solid #ddd;padding:6px;margin-left:12px}.example-names .tab-button.active{background-color:#d5d5d5}.right .example-names{background-color:#444}.right .example-names .tab-button{color:white;border:1px solid #666;border-radius:6px}.right .example-names .tab-button.active{background-color:#5e5e5e}#nav-background{position:fixed;left:0;top:0;bottom:0;width:16.5%;padding-right:14.4px;background-color:#fbfbfb;border-right:1px solid #f0f0f0;z-index:-1}#right-panel-background{position:absolute;right:-12px;top:-12px;bottom:-12px;width:48.6%;background-color:#333;z-index:-1}@media (max-width:1200px){nav{width:198px}.container{max-width:840px}.container .row .content{margin-left:224px;width:606px}}@media (max-width:992px){nav{width:169.4px}.container{max-width:720px}.container .row .content{margin-left:194px;width:526px}}@media (max-width:768px){nav{display:none}.container{width:95%;max-width:none}.container .row .content,.container-fluid .row .content,.container-fluid.triple .row .content{margin-left:auto;margin-right:auto;width:95%}#nav-background{display:none}#right-panel-background{width:48.6%}}.back-to-top{position:fixed;z-index:1;bottom:0;right:24px;padding:4px 8px;color:rgba(0,0,0,0.5);background-color:#f2f2f2;text-decoration:none !important;border-top:1px solid #d9d9d9;border-left:1px solid #d9d9d9;border-right:1px solid #d9d9d9;border-top-left-radius:3px;border-top-right-radius:3px}.resource-group{padding:12px;margin-bottom:12px;background-color:white;border:1px solid #d9d9d9;border-radius:6px}.resource-group h2.group-heading,.resource-group .heading a{padding:12px;margin:-12px -12px 12px -12px;background-color:#f2f2f2;border-bottom:1px solid #d9d9d9;border-top-left-radius:6px;border-top-right-radius:6px;white-space:nowrap;text-overflow:ellipsis;overflow:hidden}.triple .content .resource-group{padding:0;border:none}.triple .content .resource-group h2.group-heading,.triple .content .resource-group .heading a{margin:0 0 12px 0;border:1px solid #d9d9d9}nav .resource-group .heading a{padding:12px;margin-bottom:0}nav .resource-group .collapse-content{padding:0}.action{margin-bottom:12px;padding:12px 12px 0 12px;overflow:hidden;border:1px solid transparent;border-radius:6px}.action h4.action-heading{padding:6px 12px;margin:-12px -12px 12px -12px;border-bottom:1px solid transparent;border-top-left-radius:6px;border-top-right-radius:6px;overflow:hidden}.action h4.action-heading .name{float:right;font-weight:normal;padding:6px 0}.action h4.action-heading .method{padding:6px 12px;margin-right:12px;border-radius:3px;display:inline-block}.action h4.action-heading .method.get{color:#fff;background-color:#337ab7}.action h4.action-heading .method.head{color:#fff;background-color:#337ab7}.action h4.action-heading .method.options{color:#fff;background-color:#337ab7}.action h4.action-heading .method.put{color:#fff;background-color:#ed9c28}.action h4.action-heading .method.patch{color:#fff;background-color:#ed9c28}.action h4.action-heading .method.post{color:#fff;background-color:#5cb85c}.action h4.action-heading .method.delete{color:#fff;background-color:#d9534f}.action h4.action-heading code{color:#444;background-color:#f5f5f5;border-color:#cfcfcf;font-weight:normal;word-break:break-all;display:inline-block;margin-top:2px}.action dl.inner{padding-bottom:2px}.action .title{border-bottom:1px solid white;margin:0 -12px -1px -12px;padding:12px}.action.get{border-color:#bce8f1}.action.get h4.action-heading{color:#337ab7;background:#d9edf7;border-bottom-color:#bce8f1}.action.head{border-color:#bce8f1}.action.head h4.action-heading{color:#337ab7;background:#d9edf7;border-bottom-color:#bce8f1}.action.options{border-color:#bce8f1}.action.options h4.action-heading{color:#337ab7;background:#d9edf7;border-bottom-color:#bce8f1}.action.post{border-color:#d6e9c6}.action.post h4.action-heading{color:#5cb85c;background:#dff0d8;border-bottom-color:#d6e9c6}.action.put{border-color:#faebcc}.action.put h4.action-heading{color:#ed9c28;background:#fcf8e3;border-bottom-color:#faebcc}.action.patch{border-color:#faebcc}.action.patch h4.action-heading{color:#ed9c28;background:#fcf8e3;border-bottom-color:#faebcc}.action.delete{border-color:#ebccd1}.action.delete h4.action-heading{color:#d9534f;background:#f2dede;border-bottom-color:#ebccd1}</style></head><body class="preload"><a href="#top" class="text-muted back-to-top"><i class="fa fa-toggle-up"></i>&nbsp;Back to top</a><div class="container"><div class="row"><nav><div class="resource-group"><div class="heading"><div class="chevron"><i class="open fa fa-angle-down"></i></div><a href="#user-security">user security</a></div><div class="collapse-content"><ul><li><a href="#user-security-app-about-us-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>App about us</a></li><li><a href="#user-security-register-post"><span class="badge post"><i class="fa fa-plus"></i></span>Register</a></li><li><a href="#user-security-uploadfile-post"><span class="badge post"><i class="fa fa-plus"></i></span>uploadfile</a></li><li><a href="#user-security-login-with-facbook-post"><span class="badge post"><i class="fa fa-plus"></i></span>Login With Facbook</a></li><li><a href="#user-security-login-with-google-post"><span class="badge post"><i class="fa fa-plus"></i></span>Login With google</a></li><li><a href="#user-security-verify-user-post"><span class="badge post"><i class="fa fa-plus"></i></span>verify user</a></li><li><a href="#user-security-login-post"><span class="badge post"><i class="fa fa-plus"></i></span>Login</a></li><li><a href="#user-security-get-reset-token-post"><span class="badge post"><i class="fa fa-plus"></i></span>Get Reset Token</a></li><li><a href="#user-security-check-reset-token-post"><span class="badge post"><i class="fa fa-plus"></i></span>check reset token</a></li><li><a href="#user-security-reset-password-post"><span class="badge post"><i class="fa fa-plus"></i></span>Reset Password</a></li><li><a href="#user-security-getuserinfo-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>getUserInfo</a></li><li><a href="#user-security-getuserprofile-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>getUserProfile</a></li><li><a href="#user-security-logout-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>logout</a></li><li><a href="#user-security-update-password-post"><span class="badge post"><i class="fa fa-plus"></i></span>Update Password</a></li><li><a href="#user-security-edit-user-info-post"><span class="badge post"><i class="fa fa-plus"></i></span>Edit User Info</a></li><li><a href="#user-security-turnonoffnotification-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>turnOnOffNotification</a></li></ul></div></div><div class="resource-group"><div class="heading"><div class="chevron"><i class="open fa fa-angle-down"></i></div><a href="#lists">Lists</a></div><div class="collapse-content"><ul><li><a href="#lists-get-countries-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>get Countries</a></li><li><a href="#lists-get-cities-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>get Cities</a></li><li><a href="#lists-get-categories-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>get Categories</a></li><li><a href="#lists-get-education-degrees-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>get Education Degrees</a></li></ul></div></div><div class="resource-group"><div class="heading"><div class="chevron"><i class="open fa fa-angle-down"></i></div><a href="#cars">cars</a></div><div class="collapse-content"><ul><li><a href="#cars-add-question-post"><span class="badge post"><i class="fa fa-plus"></i></span>add question</a></li><li><a href="#cars-add-report-post"><span class="badge post"><i class="fa fa-plus"></i></span>add Report</a></li><li><a href="#cars-questions-list-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>questions list</a></li><li><a href="#cars-category-questions-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>category Questions</a></li><li><a href="#cars-question-details-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>question details</a></li><li><a href="#cars-my-questions-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>my Questions</a></li><li><a href="#cars-delete-all-my-questions-delete"><span class="badge delete"><i class="fa fa-times"></i></span>delete all my questions</a></li><li><a href="#cars-delete-one-question-delete"><span class="badge delete"><i class="fa fa-times"></i></span>delete one question</a></li><li><a href="#cars-search-questions-post"><span class="badge post"><i class="fa fa-plus"></i></span>search questions</a></li><li><a href="#cars-filter-questions-post"><span class="badge post"><i class="fa fa-plus"></i></span>filter questions</a></li><li><a href="#cars-add-favourite-post"><span class="badge post"><i class="fa fa-plus"></i></span>add favourite</a></li><li><a href="#cars-add-to-saves-post"><span class="badge post"><i class="fa fa-plus"></i></span>add to saves</a></li><li><a href="#cars-get-saves-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>get saves</a></li><li><a href="#cars-delete-all-saves-delete"><span class="badge delete"><i class="fa fa-times"></i></span>delete all saves</a></li><li><a href="#cars-my-answers-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>my answers</a></li><li><a href="#cars-delete-all-my-answers-delete"><span class="badge delete"><i class="fa fa-times"></i></span>delete all my answers</a></li><li><a href="#cars-delete-one-answer-delete"><span class="badge delete"><i class="fa fa-times"></i></span>delete one answer</a></li><li><a href="#cars-add-answer-post"><span class="badge post"><i class="fa fa-plus"></i></span>add answer</a></li><li><a href="#cars-add-reply-post"><span class="badge post"><i class="fa fa-plus"></i></span>add reply</a></li><li><a href="#cars-publish-draft-post"><span class="badge post"><i class="fa fa-plus"></i></span>publish draft</a></li><li><a href="#cars-my-drafts-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>my drafts</a></li><li><a href="#cars-delete-all-my-drafts-delete"><span class="badge delete"><i class="fa fa-times"></i></span>delete all my drafts</a></li><li><a href="#cars-answer-replies-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>answer replies</a></li></ul></div></div><div class="resource-group"><div class="heading"><div class="chevron"><i class="open fa fa-angle-down"></i></div><a href="#message">Message</a></div><div class="collapse-content"><ul><li><a href="#message-send-message-post"><span class="badge post"><i class="fa fa-plus"></i></span>send message</a></li><li><a href="#message-get-my-chats-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>get my chats</a></li><li><a href="#message-get-chat-details-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>get chat details</a></li><li><a href="#message-delete-all-my-chats-delete"><span class="badge delete"><i class="fa fa-times"></i></span>delete all my chats</a></li><li><a href="#message-delete-one-chat-delete"><span class="badge delete"><i class="fa fa-times"></i></span>delete one chat</a></li></ul></div></div><div class="resource-group"><div class="heading"><div class="chevron"><i class="open fa fa-angle-down"></i></div><a href="#notification">notification</a></div><div class="collapse-content"><ul><li><a href="#notification-get-notification-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>get notification</a></li><li><a href="#notification-unread-notification-count-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>unread notification count</a></li><li><a href="#notification-delete-notification-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>delete notification</a></li><li><a href="#notification-delete-one-notification-get"><span class="badge get"><i class="fa fa-arrow-down"></i></span>delete one notification</a></li></ul></div></div><p style="text-align: center; word-wrap: break-word;"><a href="http://m3lomhapp.com/api">http://m3lomhapp.com/api</a></p></nav><div class="content"><header><h1 id="top">Maloma API</h1></header><p>This is Documention for Maloma app</p>
<section id="user-security" class="resource-group"><h2 class="group-heading">user security <a href="#user-security" class="permalink">&para;</a></h2><p>This group for user security</p>
<div id="user-security-app-about-us" class="resource"><h3 class="resource-heading">App about us <a href="#user-security-app-about-us" class="permalink">&nbsp;&para;</a></h3><div id="user-security-app-about-us-get" class="action get"><h4 class="action-heading"><div class="name">App about us</div><a href="#user-security-app-about-us-get" class="method get">GET</a><code class="uri">/aboutUs</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/aboutUs</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">aboutUs</span>": <span class="hljs-value"><span class="hljs-string">"aaaahjbjkkkkkkkkkkkkkkk"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-register" class="resource"><h3 class="resource-heading">Register <a href="#user-security-register" class="permalink">&nbsp;&para;</a></h3><div id="user-security-register-post" class="action post"><h4 class="action-heading"><div class="name">Register</div><a href="#user-security-register-post" class="method post">POST</a><code class="uri">/register</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/register</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">fullName</span>":<span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
    "<span class="hljs-attribute">userName</span>":<span class="hljs-value"><span class="hljs-string">"somayahegab"</span></span>,
    "<span class="hljs-attribute">phone</span>":<span class="hljs-value"><span class="hljs-number">12547898</span>
    <span class="hljs-string">"email"</span>:<span class="hljs-string">"somaya@gmail.com"</span></span>,
    "<span class="hljs-attribute">password</span>":<span class="hljs-value"><span class="hljs-string">"123456"</span></span>,
    "<span class="hljs-attribute">country_id</span>":<span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">city_id</span>":<span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">gender</span>":<span class="hljs-value"><span class="hljs-number">1</span> for male <span class="hljs-number">2</span> for female</span>,
    "<span class="hljs-attribute">birthDate</span>":<span class="hljs-value"><span class="hljs-string">"1993-06-1"</span></span>,
    "<span class="hljs-attribute">educationDegree_id</span>":<span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">specialization</span>":<span class="hljs-value"><span class="hljs-string">"العلوم التطبيقيه"</span></span>,
    "<span class="hljs-attribute">yearsOfExperience</span>":<span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">aboutYourSelf</span>":<span class="hljs-value"><span class="hljs-string">"about my self"</span></span>,
    "<span class="hljs-attribute">categories</span>":<span class="hljs-value">array of ids</span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"Registered Successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-uploadfile" class="resource"><h3 class="resource-heading">uploadfile <a href="#user-security-uploadfile" class="permalink">&nbsp;&para;</a></h3><div id="user-security-uploadfile-post" class="action post"><h4 class="action-heading"><div class="name">uploadfile</div><a href="#user-security-uploadfile-post" class="method post">POST</a><code class="uri">/uploadFile</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/uploadFile</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">file</span>":<span class="hljs-value"><span class="hljs-string">"file"</span></span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"/uploads/15954317680mmpaPUlDa.jpeg"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-login-with-facbook" class="resource"><h3 class="resource-heading">Login With Facbook <a href="#user-security-login-with-facbook" class="permalink">&nbsp;&para;</a></h3><div id="user-security-login-with-facbook-post" class="action post"><h4 class="action-heading"><div class="name">Login With Facbook</div><a href="#user-security-login-with-facbook-post" class="method post">POST</a><code class="uri">/facebook</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/facebook</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">facebook_id</span>":<span class="hljs-value"><span class="hljs-string">"122385656018523"</span></span>,
    "<span class="hljs-attribute">token</span>":<span class="hljs-value"><span class="hljs-string">"hjnmujhnhgjjhghhj15j"</span></span>,
    "<span class="hljs-attribute">mac</span>":<span class="hljs-value"><span class="hljs-string">"vghn20562gh"</span></span>,
    "<span class="hljs-attribute">f_name</span>":<span class="hljs-value"><span class="hljs-string">"somaya"</span></span>,
    "<span class="hljs-attribute">l_name</span>":<span class="hljs-value"><span class="hljs-string">"hegab"</span></span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f"</span></span>,
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"fatma ali"</span></span>,
    "<span class="hljs-attribute">userName</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">activeCode</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">aboutYourSelf</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">yearsOfExperience</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">birthDate</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">role</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">gender</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">notifiable</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 19:50:44"</span></span>,
    "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f"</span></span>,
    "<span class="hljs-attribute">country_id</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">city_id</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">provider_id</span>": <span class="hljs-value"><span class="hljs-string">"86542125242254"</span></span>,
    "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 19:50:44"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 19:50:44"</span></span>,
    "<span class="hljs-attribute">questionNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">replyNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">country</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">city</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-login-with-google" class="resource"><h3 class="resource-heading">Login With google <a href="#user-security-login-with-google" class="permalink">&nbsp;&para;</a></h3><div id="user-security-login-with-google-post" class="action post"><h4 class="action-heading"><div class="name">Login With google</div><a href="#user-security-login-with-google-post" class="method post">POST</a><code class="uri">/google</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/google</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">google_id</span>":<span class="hljs-value"><span class="hljs-string">"122385656018523"</span></span>,
    "<span class="hljs-attribute">token</span>":<span class="hljs-value"><span class="hljs-string">"hjnmujhnhgjjhghhj15j"</span></span>,
    "<span class="hljs-attribute">mac</span>":<span class="hljs-value"><span class="hljs-string">"vghn20562gh"</span></span>,
    "<span class="hljs-attribute">f_name</span>":<span class="hljs-value"><span class="hljs-string">"somaya"</span></span>,
    "<span class="hljs-attribute">l_name</span>":<span class="hljs-value"><span class="hljs-string">"hegab"</span></span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f"</span></span>,
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"fatma ali"</span></span>,
    "<span class="hljs-attribute">userName</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">activeCode</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">aboutYourSelf</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">yearsOfExperience</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">birthDate</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">role</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">gender</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">notifiable</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 19:50:44"</span></span>,
    "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f"</span></span>,
    "<span class="hljs-attribute">country_id</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">city_id</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">provider_id</span>": <span class="hljs-value"><span class="hljs-string">"86542125242254"</span></span>,
    "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 19:50:44"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 19:50:44"</span></span>,
    "<span class="hljs-attribute">questionNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">replyNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">country</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">city</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-verify-user" class="resource"><h3 class="resource-heading">verify user <a href="#user-security-verify-user" class="permalink">&nbsp;&para;</a></h3><div id="user-security-verify-user-post" class="action post"><h4 class="action-heading"><div class="name">verify user</div><a href="#user-security-verify-user-post" class="method post">POST</a><code class="uri">/verifyUser</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/verifyUser</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">code</span>":<span class="hljs-value"><span class="hljs-string">"12452"</span></span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"Registered Successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-login" class="resource"><h3 class="resource-heading">Login <a href="#user-security-login" class="permalink">&nbsp;&para;</a></h3><div id="user-security-login-post" class="action post"><h4 class="action-heading"><div class="name">Login</div><a href="#user-security-login-post" class="method post">POST</a><code class="uri">/login</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/login</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    <span class="hljs-string">"email"</span> <span class="hljs-string">"somaya@gmail.com"</span>,
    <span class="hljs-string">"password"</span>:<span class="hljs-string">"123456"</span>,
    <span class="hljs-string">"token"</span>:<span class="hljs-string">"hjnmujhnhgjjhghhj15j"</span>,
    <span class="hljs-string">"mac"</span>:<span class="hljs-string">"vghn20562gh"</span>,
}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
    "<span class="hljs-attribute">userName</span>": <span class="hljs-value"><span class="hljs-string">"somaya"</span></span>,
    "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">activeCode</span>": <span class="hljs-value"><span class="hljs-string">"143368"</span></span>,
    "<span class="hljs-attribute">aboutYourSelf</span>": <span class="hljs-value"><span class="hljs-string">"3"</span></span>,
    "<span class="hljs-attribute">yearsOfExperience</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">birthDate</span>": <span class="hljs-value"><span class="hljs-string">"1993-06-01"</span></span>,
    "<span class="hljs-attribute">role</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">gender</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">notifiable</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"somayahegab@gmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 19:23:27"</span></span>,
    "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">country_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">city_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">provider_id</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 17:05:01"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-20 15:36:42"</span></span>,
    "<span class="hljs-attribute">questionNumbers</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
    "<span class="hljs-attribute">replyNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">country</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"السعودية"</span>
    </span>}</span>,
    "<span class="hljs-attribute">city</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"جده"</span>
    </span>}</span>,
    "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>404</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">404</span></span>,
    "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"invalid data"</span>
</span>} 

{
    "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">404</span></span>,
    "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Email Not verofied"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-get-reset-token" class="resource"><h3 class="resource-heading">Get Reset Token <a href="#user-security-get-reset-token" class="permalink">&nbsp;&para;</a></h3><div id="user-security-get-reset-token-post" class="action post"><h4 class="action-heading"><div class="name">Get Reset Token</div><a href="#user-security-get-reset-token-post" class="method post">POST</a><code class="uri">/getResetToken</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/getResetToken</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    <span class="hljs-string">"email"</span> <span class="hljs-string">"somaya@gmail.com"</span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"Code sent to your mail, please follow mail."</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-check-reset-token" class="resource"><h3 class="resource-heading">check reset token <a href="#user-security-check-reset-token" class="permalink">&nbsp;&para;</a></h3><div id="user-security-check-reset-token-post" class="action post"><h4 class="action-heading"><div class="name">check reset token</div><a href="#user-security-check-reset-token-post" class="method post">POST</a><code class="uri">/checkResetToken</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/checkResetToken</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    <span class="hljs-string">"code"</span>=&gt;<span class="hljs-number">895586</span>
    <span class="hljs-string">"email"</span>: <span class="hljs-string">"somaya@gmail.com"</span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"Valid Code"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>404</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">404</span></span>,
  "<span class="hljs-attribute">error</span>": <span class="hljs-value"><span class="hljs-string">"Invalid Code"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-reset-password" class="resource"><h3 class="resource-heading">Reset Password <a href="#user-security-reset-password" class="permalink">&nbsp;&para;</a></h3><div id="user-security-reset-password-post" class="action post"><h4 class="action-heading"><div class="name">Reset Password</div><a href="#user-security-reset-password-post" class="method post">POST</a><code class="uri">/reset</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/reset</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    <span class="hljs-string">"email"</span> <span class="hljs-string">"somaya@gmail.com"</span>,
    <span class="hljs-string">"new_password"</span> :<span class="hljs-number">123456</span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"password updated successfully."</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-getuserinfo" class="resource"><h3 class="resource-heading">getUserInfo <a href="#user-security-getuserinfo" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="user-security-getuserinfo-get" class="action get"><h4 class="action-heading"><div class="name">getUserInfo</div><a href="#user-security-getuserinfo-get" class="method get">GET</a><code class="uri">/getUserInfo</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/getUserInfo</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
    "<span class="hljs-attribute">userName</span>": <span class="hljs-value"><span class="hljs-string">"somaya"</span></span>,
    "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">activeCode</span>": <span class="hljs-value"><span class="hljs-string">"143368"</span></span>,
    "<span class="hljs-attribute">aboutYourSelf</span>": <span class="hljs-value"><span class="hljs-string">"3"</span></span>,
    "<span class="hljs-attribute">yearsOfExperience</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">birthDate</span>": <span class="hljs-value"><span class="hljs-string">"1993-06-01"</span></span>,
    "<span class="hljs-attribute">role</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">gender</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">notifiable</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"somayahegab@gmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 19:23:27"</span></span>,
    "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">country_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">city_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">provider_id</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 17:05:01"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-20 15:36:42"</span></span>,
    "<span class="hljs-attribute">questionNumbers</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
    "<span class="hljs-attribute">replyNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">country</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"السعودية"</span>
    </span>}</span>,
    "<span class="hljs-attribute">city</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"جده"</span>
    </span>}</span>,
    "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-getuserprofile" class="resource"><h3 class="resource-heading">getUserProfile <a href="#user-security-getuserprofile" class="permalink">&nbsp;&para;</a></h3><div id="user-security-getuserprofile-get" class="action get"><h4 class="action-heading"><div class="name">getUserProfile</div><a href="#user-security-getuserprofile-get" class="method get">GET</a><code class="uri">/getUserProfile/{id}</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/getUserProfile/<span class="hljs-attribute" title="id">id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>id</dt><dd><code>integer</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>id of user</p>
</dd></dl></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
    "<span class="hljs-attribute">userName</span>": <span class="hljs-value"><span class="hljs-string">"somaya"</span></span>,
    "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">activeCode</span>": <span class="hljs-value"><span class="hljs-string">"143368"</span></span>,
    "<span class="hljs-attribute">aboutYourSelf</span>": <span class="hljs-value"><span class="hljs-string">"3"</span></span>,
    "<span class="hljs-attribute">yearsOfExperience</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">birthDate</span>": <span class="hljs-value"><span class="hljs-string">"1993-06-01"</span></span>,
    "<span class="hljs-attribute">role</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">gender</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">notifiable</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"somayahegab@gmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 19:23:27"</span></span>,
    "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">country_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">city_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">provider_id</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 17:05:01"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-20 15:36:42"</span></span>,
    "<span class="hljs-attribute">questionNumbers</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
    "<span class="hljs-attribute">replyNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">country</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"السعودية"</span>
    </span>}</span>,
    "<span class="hljs-attribute">city</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"جده"</span>
    </span>}</span>,
    "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-logout" class="resource"><h3 class="resource-heading">logout <a href="#user-security-logout" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="user-security-logout-get" class="action get"><h4 class="action-heading"><div class="name">logout</div><a href="#user-security-logout-get" class="method get">GET</a><code class="uri">/logout</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/logout</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"user logout successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-update-password" class="resource"><h3 class="resource-heading">Update Password <a href="#user-security-update-password" class="permalink">&nbsp;&para;</a></h3><div id="user-security-update-password-post" class="action post"><h4 class="action-heading"><div class="name">Update Password</div><a href="#user-security-update-password-post" class="method post">POST</a><code class="uri">/updatePassword</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/updatePassword</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    <span class="hljs-string">"old_password"</span> <span class="hljs-number">124521</span>,
    <span class="hljs-string">"new_password"</span>:<span class="hljs-string">"123456"</span>,
    <span class="hljs-string">"token"</span>:<span class="hljs-string">"462d006c132170e104673592216573e8f23f0213"</span>
}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"password updated successfully."</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-edit-user-info" class="resource"><h3 class="resource-heading">Edit User Info <a href="#user-security-edit-user-info" class="permalink">&nbsp;&para;</a></h3><div id="user-security-edit-user-info-post" class="action post"><h4 class="action-heading"><div class="name">Edit User Info</div><a href="#user-security-edit-user-info-post" class="method post">POST</a><code class="uri">/editUserInfo</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/editUserInfo</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    <span class="hljs-string">"fullName"</span> <span class="hljs-string">"somaya"</span>,
    <span class="hljs-string">"userName"</span> <span class="hljs-string">"hegab"</span>,
    <span class="hljs-string">"phone"</span>:<span class="hljs-string">"123456"</span>,
    <span class="hljs-string">"photo"</span>:photo file,
    <span class="hljs-string">"specialization"</span>:<span class="hljs-number">1</span>,
    <span class="hljs-string">"country_id"</span>:<span class="hljs-number">1</span>,
    <span class="hljs-string">"city_id"</span>:<span class="hljs-number">1</span>,
    <span class="hljs-string">"educationDegree_id"</span>:<span class="hljs-number">1</span>,
    <span class="hljs-string">"yearsOfExperience"</span>:<span class="hljs-number">1</span>,
    <span class="hljs-string">"aboutYourSelf"</span>:<span class="hljs-string">"dummy data"</span>,
    <span class="hljs-string">"email"</span>:<span class="hljs-string">"somaia@gmail.com"</span>
    <span class="hljs-string">"token"</span>:<span class="hljs-string">"462d006c132170e104673592216573e8f23f0213"</span>
}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
  "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
    "<span class="hljs-attribute">userName</span>": <span class="hljs-value"><span class="hljs-string">"somaya"</span></span>,
    "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">activeCode</span>": <span class="hljs-value"><span class="hljs-string">"143368"</span></span>,
    "<span class="hljs-attribute">aboutYourSelf</span>": <span class="hljs-value"><span class="hljs-string">"3"</span></span>,
    "<span class="hljs-attribute">yearsOfExperience</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">birthDate</span>": <span class="hljs-value"><span class="hljs-string">"1993-06-01"</span></span>,
    "<span class="hljs-attribute">role</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">gender</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">notifiable</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span class="hljs-string">"somayahegab@gmail.com"</span></span>,
    "<span class="hljs-attribute">email_verified_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 19:23:27"</span></span>,
    "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">country_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">city_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">provider_id</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
    "<span class="hljs-attribute">deleted_at</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-19 17:05:01"</span></span>,
    "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-20 15:36:42"</span></span>,
    "<span class="hljs-attribute">questionNumbers</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
    "<span class="hljs-attribute">replyNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
    "<span class="hljs-attribute">country</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"السعودية"</span>
    </span>}</span>,
    "<span class="hljs-attribute">city</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"جده"</span>
    </span>}</span>,
    "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="user-security-turnonoffnotification" class="resource"><h3 class="resource-heading">turnOnOffNotification <a href="#user-security-turnonoffnotification" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="user-security-turnonoffnotification-get" class="action get"><h4 class="action-heading"><div class="name">turnOnOffNotification</div><a href="#user-security-turnonoffnotification-get" class="method get">GET</a><code class="uri">/turnOnOffNotification</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/turnOnOffNotification</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"Notifications Turn OFF"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div></section><section id="lists" class="resource-group"><h2 class="group-heading">Lists <a href="#lists" class="permalink">&para;</a></h2><p>This group for lists</p>
<div id="lists-get-countries" class="resource"><h3 class="resource-heading">get Countries <a href="#lists-get-countries" class="permalink">&nbsp;&para;</a></h3><div id="lists-get-countries-get" class="action get"><h4 class="action-heading"><div class="name">get Countries</div><a href="#lists-get-countries-get" class="method get">GET</a><code class="uri">/getCountries</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/getCountries</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">countries</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"السعودية"</span>
    </span>}
  ]
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="lists-get-cities" class="resource"><h3 class="resource-heading">get Cities <a href="#lists-get-cities" class="permalink">&nbsp;&para;</a></h3><div id="lists-get-cities-get" class="action get"><h4 class="action-heading"><div class="name">get Cities</div><a href="#lists-get-cities-get" class="method get">GET</a><code class="uri">/getCity/{id}</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/getCity/<span class="hljs-attribute" title="id">id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>id</dt><dd><code>integer</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>id of area</p>
</dd></dl></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">cities</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"جده"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الرياض"</span>
    </span>}
  ]
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="lists-get-categories" class="resource"><h3 class="resource-heading">get Categories <a href="#lists-get-categories" class="permalink">&nbsp;&para;</a></h3><div id="lists-get-categories-get" class="action get"><h4 class="action-heading"><div class="name">get Categories</div><a href="#lists-get-categories-get" class="method get">GET</a><code class="uri">/getCategories</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/getCategories</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">categories</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span></span>,
      "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-string">"/uploads/categories/lcvpauI1Wa.jpeg"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
      "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-string">"/uploads/categories/LdDs4nZWGQ.jpeg"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"افراح"</span></span>,
      "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-string">"/uploads/categories/UuKwWH3QjW.jpeg"</span>
    </span>}
  ]
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="lists-get-education-degrees" class="resource"><h3 class="resource-heading">get Education Degrees <a href="#lists-get-education-degrees" class="permalink">&nbsp;&para;</a></h3><div id="lists-get-education-degrees-get" class="action get"><h4 class="action-heading"><div class="name">get Education Degrees</div><a href="#lists-get-education-degrees-get" class="method get">GET</a><code class="uri">/getEducationDegrees</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/getEducationDegrees</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">educationDegrees</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"دكتوراه"</span>
    </span>}
  ]
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div></section><section id="cars" class="resource-group"><h2 class="group-heading">cars <a href="#cars" class="permalink">&para;</a></h2><p>This group for questions</p>
<div id="cars-add-question" class="resource"><h3 class="resource-heading">add question <a href="#cars-add-question" class="permalink">&nbsp;&para;</a></h3><div id="cars-add-question-post" class="action post"><h4 class="action-heading"><div class="name">add question</div><a href="#cars-add-question-post" class="method post">POST</a><code class="uri">/questions/create</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/questions/create</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">token</span>":<span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span>
    <span class="hljs-string">"cityIds[0]"</span>:<span class="hljs-number">1</span>
    <span class="hljs-string">"cityIds[1]"</span>:<span class="hljs-number">2</span>
    <span class="hljs-string">"questionText"</span>:ماهو صوت الاسد؟
    <span class="hljs-string">"category_id"</span>:<span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">photo</span>":<span class="hljs-value">file

</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"question added Successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-add-report" class="resource"><h3 class="resource-heading">add Report <a href="#cars-add-report" class="permalink">&nbsp;&para;</a></h3><div id="cars-add-report-post" class="action post"><h4 class="action-heading"><div class="name">add Report</div><a href="#cars-add-report-post" class="method post">POST</a><code class="uri">/addReport</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/addReport</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">token</span>":<span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span>
    <span class="hljs-string">"question_id"</span>:<span class="hljs-number">1</span>
    <span class="hljs-string">"message"</span>: <span class="hljs-string">"غير لائق"</span>

</span>}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"Report added Successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-questions-list" class="resource"><h3 class="resource-heading">questions list <a href="#cars-questions-list" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-questions-list-get" class="action get"><h4 class="action-heading"><div class="name">questions list</div><a href="#cars-questions-list-get" class="method get">GET</a><code class="uri">/questions</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/questions</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">questions</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">current_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الاسد؟"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-29 19:53:40"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">saved</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
          "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
            "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
          </span>}
        </span>}</span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الاسد؟"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-25 18:24:14"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">saved</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
          "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
            "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
          </span>}
        </span>}</span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الكلب"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-23 00:00:00"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">saved</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
          "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
            "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
          </span>}
        </span>}</span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>}
    ]</span>,
    "<span class="hljs-attribute">first_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/questions?page=1"</span></span>,
    "<span class="hljs-attribute">from</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/questions?page=1"</span></span>,
    "<span class="hljs-attribute">next_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/questions"</span></span>,
    "<span class="hljs-attribute">per_page</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">prev_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">to</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">3</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-category-questions" class="resource"><h3 class="resource-heading">category Questions <a href="#cars-category-questions" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-category-questions-get" class="action get"><h4 class="action-heading"><div class="name">category Questions</div><a href="#cars-category-questions-get" class="method get">GET</a><code class="uri">/categoryQuestions/{id}</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/categoryQuestions/<span class="hljs-attribute" title="id">id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>id</dt><dd><code>integer</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>id of category</p>
</dd></dl></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">questions</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">current_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الاسد؟"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-29 19:53:40"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">saved</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
          "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
            "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
          </span>}
        </span>}</span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الاسد؟"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-25 18:24:14"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">saved</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
          "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
            "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
          </span>}
        </span>}</span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الكلب"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-23 00:00:00"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">saved</span>": <span class="hljs-value"><span class="hljs-literal">true</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">true</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
          "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
            "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
          </span>}
        </span>}</span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>}
    ]</span>,
    "<span class="hljs-attribute">first_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/categoryQuestions/1/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1"</span></span>,
    "<span class="hljs-attribute">from</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/categoryQuestions/1/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1"</span></span>,
    "<span class="hljs-attribute">next_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/categoryQuestions/1/e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">per_page</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">prev_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">to</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">3</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-question-details" class="resource"><h3 class="resource-heading">question details <a href="#cars-question-details" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-question-details-get" class="action get"><h4 class="action-heading"><div class="name">question details</div><a href="#cars-question-details-get" class="method get">GET</a><code class="uri">/question/{id}</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/question/<span class="hljs-attribute" title="id">id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>id</dt><dd><code>integer</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>id of question</p>
</dd></dl></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">question</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الكلب"</span></span>,
    "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-23 00:00:00"</span></span>,
    "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
    "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">true</span></span>,
    "<span class="hljs-attribute">saved</span>": <span class="hljs-value"><span class="hljs-literal">true</span></span>,
    "<span class="hljs-attribute">answers</span>": <span class="hljs-value">[
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">answerText</span>": <span class="hljs-value"><span class="hljs-string">"نهيق"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-25 16:46:50"</span></span>,
        "<span class="hljs-attribute">replyNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">true</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
        "<span class="hljs-attribute">answerText</span>": <span class="hljs-value"><span class="hljs-string">"نهيق"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-25 16:47:34"</span></span>,
        "<span class="hljs-attribute">replyNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
        "<span class="hljs-attribute">answerText</span>": <span class="hljs-value"><span class="hljs-string">"نهيق"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-28 12:08:31"</span></span>,
        "<span class="hljs-attribute">replyNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
        "<span class="hljs-attribute">answerText</span>": <span class="hljs-value"><span class="hljs-string">"klkkgl"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-28 12:37:18"</span></span>,
        "<span class="hljs-attribute">replyNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
        </span>}
      </span>}
    ]</span>,
    "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
      "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
      "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
      </span>}
    </span>}</span>,
    "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
    </span>}
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-my-questions" class="resource"><h3 class="resource-heading">my Questions <a href="#cars-my-questions" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-my-questions-get" class="action get"><h4 class="action-heading"><div class="name">my Questions</div><a href="#cars-my-questions-get" class="method get">GET</a><code class="uri">/myQuestions</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/myQuestions</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">questions</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">current_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الاسد؟"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-29 19:53:40"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الاسد؟"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-25 18:24:14"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الكلب"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-23 00:00:00"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>}
    ]</span>,
    "<span class="hljs-attribute">first_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/myQuestions/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1"</span></span>,
    "<span class="hljs-attribute">from</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/myQuestions/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1"</span></span>,
    "<span class="hljs-attribute">next_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/myQuestions/e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">per_page</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">prev_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">to</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">3</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-delete-all-my-questions" class="resource"><h3 class="resource-heading">delete all my questions <a href="#cars-delete-all-my-questions" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-delete-all-my-questions-delete" class="action delete"><h4 class="action-heading"><div class="name">delete all my questions</div><a href="#cars-delete-all-my-questions-delete" class="method delete">DELETE</a><code class="uri">/myQuestions/all</code></h4><h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/myQuestions/all</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    <span class="hljs-string">"status"</span>=&gt;<span class="hljs-number">200</span>,
    <span class="hljs-string">"success"</span>=&gt;<span class="hljs-string">'All Questions Deleted Successfully'</span>
}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-delete-one-question" class="resource"><h3 class="resource-heading">delete one question <a href="#cars-delete-one-question" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-delete-one-question-delete" class="action delete"><h4 class="action-heading"><div class="name">delete one question</div><a href="#cars-delete-one-question-delete" class="method delete">DELETE</a><code class="uri">/myQuestions/{id}</code></h4><h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/myQuestions/<span class="hljs-attribute" title="id">id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>id</dt><dd><code>integer</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>id of question</p>
</dd></dl></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    <span class="hljs-string">"status"</span>=&gt;<span class="hljs-number">200</span>,
    <span class="hljs-string">"success"</span>=&gt;<span class="hljs-string">'Question Deleted Successfully'</span>
}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-search-questions" class="resource"><h3 class="resource-heading">search questions <a href="#cars-search-questions" class="permalink">&nbsp;&para;</a></h3><div id="cars-search-questions-post" class="action post"><h4 class="action-heading"><div class="name">search questions</div><a href="#cars-search-questions-post" class="method post">POST</a><code class="uri">/search</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/search</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">name</span>":<span class="hljs-value"><span class="hljs-string">"ماهو "</span></span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">questions</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الاسد؟"</span></span>,
      "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-29 19:53:40"</span></span>,
      "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
        "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
      </span>}
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الاسد؟"</span></span>,
      "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-25 18:24:14"</span></span>,
      "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
        "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
      </span>}
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الكلب"</span></span>,
      "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
      "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-23 00:00:00"</span></span>,
      "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
        "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
      </span>}
    </span>}
  ]
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-filter-questions" class="resource"><h3 class="resource-heading">filter questions <a href="#cars-filter-questions" class="permalink">&nbsp;&para;</a></h3><div id="cars-filter-questions-post" class="action post"><h4 class="action-heading"><div class="name">filter questions</div><a href="#cars-filter-questions-post" class="method post">POST</a><code class="uri">/filter</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/filter</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    <span class="hljs-string">"categories[0]"</span>:<span class="hljs-number">1</span>,
    <span class="hljs-string">"cities[0]"</span>:<span class="hljs-number">1</span>,
    <span class="hljs-string">"sortBy"</span>:<span class="hljs-number">1</span> <span class="hljs-keyword">for</span> desc ,<span class="hljs-number">2</span> <span class="hljs-keyword">for</span> asc,
    <span class="hljs-string">"token"</span>:<span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">count</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
  "<span class="hljs-attribute">questions</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">current_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الاسد؟"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-29 19:53:40"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">saved</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
          "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
            "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
          </span>}
        </span>}</span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الاسد؟"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-25 18:24:14"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
        "<span class="hljs-attribute">saved</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
          "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
            "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
          </span>}
        </span>}</span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الكلب"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-23 00:00:00"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">saved</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
          "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
            "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
          </span>}
        </span>}</span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>}
    ]</span>,
    "<span class="hljs-attribute">first_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/filter?page=1"</span></span>,
    "<span class="hljs-attribute">from</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/filter?page=1"</span></span>,
    "<span class="hljs-attribute">next_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/filter"</span></span>,
    "<span class="hljs-attribute">per_page</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">prev_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">to</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
    "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">3</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-add-favourite" class="resource"><h3 class="resource-heading">add favourite <a href="#cars-add-favourite" class="permalink">&nbsp;&para;</a></h3><p>this api for add or remove favourite for both question and answer</p>
<div id="cars-add-favourite-post" class="action post"><h4 class="action-heading"><div class="name">add favourite</div><a href="#cars-add-favourite-post" class="method post">POST</a><code class="uri">/favourites/create</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/favourites/create</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">token</span>":<span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">ref_id</span>":<span class="hljs-value"><span class="hljs-string">"id of question or answer"</span></span>,
    "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"1 for question 2 for answer"</span></span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"Added To Favourites Successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-add-to-saves" class="resource"><h3 class="resource-heading">add to saves <a href="#cars-add-to-saves" class="permalink">&nbsp;&para;</a></h3><p>this api for add or remove save question</p>
<div id="cars-add-to-saves-post" class="action post"><h4 class="action-heading"><div class="name">add to saves</div><a href="#cars-add-to-saves-post" class="method post">POST</a><code class="uri">/saves/create</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/saves/create</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">token</span>":<span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">question_id</span>":<span class="hljs-value"><span class="hljs-string">"id of question or answer"</span></span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"Added To saves Successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-get-saves" class="resource"><h3 class="resource-heading">get saves <a href="#cars-get-saves" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-get-saves-get" class="action get"><h4 class="action-heading"><div class="name">get saves</div><a href="#cars-get-saves-get" class="method get">GET</a><code class="uri">/saves</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/saves</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">questions</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">current_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الكلب"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">category_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-23 00:00:00"</span></span>,
        "<span class="hljs-attribute">answerNumbers</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
        "<span class="hljs-attribute">likeNumbers</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">saved</span>": <span class="hljs-value"><span class="hljs-literal">true</span></span>,
        "<span class="hljs-attribute">liked</span>": <span class="hljs-value"><span class="hljs-literal">true</span></span>,
        "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-string">"الهندسه"</span></span>,
          "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
            "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
          </span>}
        </span>}</span>,
        "<span class="hljs-attribute">category</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"الطب"</span>
        </span>}
      </span>}
    ]</span>,
    "<span class="hljs-attribute">first_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/saves/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1"</span></span>,
    "<span class="hljs-attribute">from</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/saves/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1"</span></span>,
    "<span class="hljs-attribute">next_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/saves/e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">per_page</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">prev_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">to</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">1</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-delete-all-saves" class="resource"><h3 class="resource-heading">delete all saves <a href="#cars-delete-all-saves" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-delete-all-saves-delete" class="action delete"><h4 class="action-heading"><div class="name">delete all saves</div><a href="#cars-delete-all-saves-delete" class="method delete">DELETE</a><code class="uri">/saves/all</code></h4><h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/saves/all</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    <span class="hljs-string">'status'</span>:<span class="hljs-number">200</span>,
    <span class="hljs-string">'success'</span>:<span class="hljs-string">'All Saved Questions Deleted Successfully'</span>
}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-my-answers" class="resource"><h3 class="resource-heading">my answers <a href="#cars-my-answers" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-my-answers-get" class="action get"><h4 class="action-heading"><div class="name">my answers</div><a href="#cars-my-answers-get" class="method get">GET</a><code class="uri">/myAnswers</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/myAnswers</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">answers</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">current_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
        "<span class="hljs-attribute">answerText</span>": <span class="hljs-value"><span class="hljs-string">"klkkgl"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">question_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-28 12:37:18"</span></span>,
        "<span class="hljs-attribute">question</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الكلب"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
            "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
          </span>}
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
        "<span class="hljs-attribute">answerText</span>": <span class="hljs-value"><span class="hljs-string">"نهيق"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">question_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-28 12:08:31"</span></span>,
        "<span class="hljs-attribute">question</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الكلب"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
            "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
          </span>}
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
        "<span class="hljs-attribute">answerText</span>": <span class="hljs-value"><span class="hljs-string">"نهيق"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">question_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-25 16:47:34"</span></span>,
        "<span class="hljs-attribute">question</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الكلب"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
            "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
          </span>}
        </span>}
      </span>},
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">answerText</span>": <span class="hljs-value"><span class="hljs-string">"نهيق"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">question_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-25 16:46:50"</span></span>,
        "<span class="hljs-attribute">question</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الكلب"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
            "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
          </span>}
        </span>}
      </span>}
    ]</span>,
    "<span class="hljs-attribute">first_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/myAnswers/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1"</span></span>,
    "<span class="hljs-attribute">from</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/myAnswers/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1"</span></span>,
    "<span class="hljs-attribute">next_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/myAnswers/e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">per_page</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">prev_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">to</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
    "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">4</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-delete-all-my-answers" class="resource"><h3 class="resource-heading">delete all my answers <a href="#cars-delete-all-my-answers" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-delete-all-my-answers-delete" class="action delete"><h4 class="action-heading"><div class="name">delete all my answers</div><a href="#cars-delete-all-my-answers-delete" class="method delete">DELETE</a><code class="uri">/myAnswers/all</code></h4><h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/myAnswers/all</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    <span class="hljs-string">'status'</span>:<span class="hljs-number">200</span>,
    <span class="hljs-string">'success'</span>:<span class="hljs-string">'All my answers  Deleted Successfully'</span>
}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-delete-one-answer" class="resource"><h3 class="resource-heading">delete one answer <a href="#cars-delete-one-answer" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-delete-one-answer-delete" class="action delete"><h4 class="action-heading"><div class="name">delete one answer</div><a href="#cars-delete-one-answer-delete" class="method delete">DELETE</a><code class="uri">/myAnswers/{id}</code></h4><h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/myAnswers/<span class="hljs-attribute" title="id">id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>id</dt><dd><code>integer</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>id of answer</p>
</dd></dl></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    <span class="hljs-string">'status'</span>:<span class="hljs-number">200</span>,
    <span class="hljs-string">'success'</span>:<span class="hljs-string">'my answer  Deleted Successfully'</span>
}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-add-answer" class="resource"><h3 class="resource-heading">add answer <a href="#cars-add-answer" class="permalink">&nbsp;&para;</a></h3><div id="cars-add-answer-post" class="action post"><h4 class="action-heading"><div class="name">add answer</div><a href="#cars-add-answer-post" class="method post">POST</a><code class="uri">/myAnswers/create</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/myAnswers/create</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">token</span>":<span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">question_id</span>":<span class="hljs-value"><span class="hljs-string">"id of question or answer"</span></span>,
    "<span class="hljs-attribute">answerText</span>":<span class="hljs-value"><span class="hljs-string">"صوت الكلب هو "</span></span>,
    "<span class="hljs-attribute">photo</span>":<span class="hljs-value"><span class="hljs-string">"optional "</span></span>,
    "<span class="hljs-attribute">saveAsDraft</span>":<span class="hljs-value"><span class="hljs-string">"1 for draft ,0 for publish "</span></span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"Answer Added  Successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-add-reply" class="resource"><h3 class="resource-heading">add reply <a href="#cars-add-reply" class="permalink">&nbsp;&para;</a></h3><div id="cars-add-reply-post" class="action post"><h4 class="action-heading"><div class="name">add reply</div><a href="#cars-add-reply-post" class="method post">POST</a><code class="uri">/replies/create</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/replies/create</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">token</span>":<span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">answer_id</span>":<span class="hljs-value"><span class="hljs-string">"id of question or answer"</span></span>,
    "<span class="hljs-attribute">replyText</span>":<span class="hljs-value"><span class="hljs-string">"صوت الكلب هو "</span></span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"reply Added  Successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-publish-draft" class="resource"><h3 class="resource-heading">publish draft <a href="#cars-publish-draft" class="permalink">&nbsp;&para;</a></h3><div id="cars-publish-draft-post" class="action post"><h4 class="action-heading"><div class="name">publish draft</div><a href="#cars-publish-draft-post" class="method post">POST</a><code class="uri">/myDrafts/create</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/myDrafts/create</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">token</span>":<span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">draft_id</span>":<span class="hljs-value"><span class="hljs-string">"id of question or answer"</span></span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"Draft Published Successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-my-drafts" class="resource"><h3 class="resource-heading">my drafts <a href="#cars-my-drafts" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-my-drafts-get" class="action get"><h4 class="action-heading"><div class="name">my drafts</div><a href="#cars-my-drafts-get" class="method get">GET</a><code class="uri">/myDrafts</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/myDrafts</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">drafts</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">current_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
      {
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">answerText</span>": <span class="hljs-value"><span class="hljs-string">"نهيق"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">question_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-25 16:46:50"</span></span>,
        "<span class="hljs-attribute">question</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">questionText</span>": <span class="hljs-value"><span class="hljs-string">"ماهو صوت الكلب"</span></span>,
          "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
          "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
          "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
            "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
            "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
          </span>}
        </span>}
      </span>}
    ]</span>,
    "<span class="hljs-attribute">first_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/myDrafts/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1"</span></span>,
    "<span class="hljs-attribute">from</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/myDrafts/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1"</span></span>,
    "<span class="hljs-attribute">next_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/myDrafts/e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">per_page</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
    "<span class="hljs-attribute">prev_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">to</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">1</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-delete-all-my-drafts" class="resource"><h3 class="resource-heading">delete all my drafts <a href="#cars-delete-all-my-drafts" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="cars-delete-all-my-drafts-delete" class="action delete"><h4 class="action-heading"><div class="name">delete all my drafts</div><a href="#cars-delete-all-my-drafts-delete" class="method delete">DELETE</a><code class="uri">/myDrafts/all</code></h4><h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/myDrafts/all</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    <span class="hljs-string">'status'</span>:<span class="hljs-number">200</span>,
    <span class="hljs-string">'success'</span>:<span class="hljs-string">'All my Drafts  Deleted Successfully'</span>
}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="cars-answer-replies" class="resource"><h3 class="resource-heading">answer replies <a href="#cars-answer-replies" class="permalink">&nbsp;&para;</a></h3><div id="cars-answer-replies-get" class="action get"><h4 class="action-heading"><div class="name">answer replies</div><a href="#cars-answer-replies-get" class="method get">GET</a><code class="uri">/replies/{id}</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/replies/<span class="hljs-attribute" title="id">id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>id</dt><dd><code>integer</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>id of answer</p>
</dd></dl></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">replies</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">replyText</span>": <span class="hljs-value"><span class="hljs-string">"اجابه خاطئه"</span></span>,
      "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-30 00:00:00"</span></span>,
      "<span class="hljs-attribute">user</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"fatma ali"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}
    </span>}
  ]
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div></section><section id="message" class="resource-group"><h2 class="group-heading">Message <a href="#message" class="permalink">&para;</a></h2><p>This group for messages</p>
<div id="message-send-message" class="resource"><h3 class="resource-heading">send message <a href="#message-send-message" class="permalink">&nbsp;&para;</a></h3><div id="message-send-message-post" class="action post"><h4 class="action-heading"><div class="name">send message</div><a href="#message-send-message-post" class="method post">POST</a><code class="uri">/sendMessage</code></h4><h4>Example URI</h4><div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/sendMessage</span></div><div class="title"><strong>Request</strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{

    "<span class="hljs-attribute">token</span>":<span class="hljs-value"><span class="hljs-string">"ghfgghyuttyhjthj"</span></span>,
    "<span class="hljs-attribute">receiver_id</span>":<span class="hljs-value"><span class="hljs-string">"id of receiver"</span></span>,
    "<span class="hljs-attribute">message</span>":<span class="hljs-value"><span class="hljs-string">"text can be option in case send image or record"</span></span>,
    "<span class="hljs-attribute">media_file</span>":<span class="hljs-value"><span class="hljs-string">"array of images or file of record"</span></span>,
    "<span class="hljs-attribute">extension</span>":<span class="hljs-value"><span class="hljs-string">"extension of file"</span></span>,
    "<span class="hljs-attribute">type</span>":<span class="hljs-value"><span class="hljs-string">"send 1 for image, send 2 for record , 3 for file"</span></span>,

}</code></pre><div style="height: 1px;"></div></div></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"Message sent Successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="message-get-my-chats" class="resource"><h3 class="resource-heading">get my chats <a href="#message-get-my-chats" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="message-get-my-chats-get" class="action get"><h4 class="action-heading"><div class="name">get my chats</div><a href="#message-get-my-chats-get" class="method get">GET</a><code class="uri">/getMyChats</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/getMyChats</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">messages</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">6</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"بمناسبه سؤالك .."</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-26 01:07:00"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-26 13:39:31"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"fatma ali"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">since</span>": <span class="hljs-value"><span class="hljs-string">"منذ أسبوعين"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">14</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"vnhbhbhm"</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:29:40"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:29:40"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"fatma ali"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">educationDegree_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">specialization</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
        "<span class="hljs-attribute">education_degree</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
          "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ماجستير"</span>
        </span>}
      </span>}</span>,
      "<span class="hljs-attribute">since</span>": <span class="hljs-value"><span class="hljs-string">"منذ 34 دقيقة"</span>
    </span>}
  ]
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="message-get-chat-details" class="resource"><h3 class="resource-heading">get chat details <a href="#message-get-chat-details" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="message-get-chat-details-get" class="action get"><h4 class="action-heading"><div class="name">get chat details</div><a href="#message-get-chat-details-get" class="method get">GET</a><code class="uri">/getChatDetails/{id}</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/getChatDetails/<span class="hljs-attribute" title="id">id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>id</dt><dd><code>integer</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>id of another user</p>
</dd></dl></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-string">"200"</span></span>,
  "<span class="hljs-attribute">messages</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">4</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"السلام عليكم "</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-26 01:05:00"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-26 13:39:31"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"fatma ali"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">medias</span>": <span class="hljs-value">[]
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">5</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"وعليكم السلام  "</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-26 01:06:00"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-26 00:00:00"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">medias</span>": <span class="hljs-value">[]
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">6</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"بمناسبه سؤالك .."</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-26 01:07:00"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-26 13:39:31"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"fatma ali"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"9f2bcf92e47ea14bb59ef77c53208bfec4f01e1f"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">medias</span>": <span class="hljs-value">[]
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">7</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"السلام عليكم"</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-27 12:50:56"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-27 12:50:56"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">medias</span>": <span class="hljs-value">[]
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">8</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"السلام عليكم"</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-27 12:53:41"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-27 12:53:41"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">medias</span>": <span class="hljs-value">[]
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">9</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"vnhbhbhm"</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:20:41"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:20:41"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">medias</span>": <span class="hljs-value">[]
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">10</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"vnhbhbhm"</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:23:12"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:23:12"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">medias</span>": <span class="hljs-value">[]
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">11</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"vnhbhbhm"</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:23:29"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:23:29"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">medias</span>": <span class="hljs-value">[]
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">12</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"vnhbhbhm"</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:24:31"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:24:31"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">medias</span>": <span class="hljs-value">[]
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">13</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"vnhbhbhm"</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:25:43"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:25:43"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">medias</span>": <span class="hljs-value">[]
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">14</span></span>,
      "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"vnhbhbhm"</span></span>,
      "<span class="hljs-attribute">sender_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
      "<span class="hljs-attribute">receiver_id</span>": <span class="hljs-value"><span class="hljs-number">3</span></span>,
      "<span class="hljs-attribute">seen</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:29:40"</span></span>,
      "<span class="hljs-attribute">updated_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-07-11 20:29:40"</span></span>,
      "<span class="hljs-attribute">sender</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">fullName</span>": <span class="hljs-value"><span class="hljs-string">"somaya hegab"</span></span>,
        "<span class="hljs-attribute">token</span>": <span class="hljs-value"><span class="hljs-string">"e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
        "<span class="hljs-attribute">photo</span>": <span class="hljs-value"><span class="hljs-literal">null</span>
      </span>}</span>,
      "<span class="hljs-attribute">medias</span>": <span class="hljs-value">[
        {
          "<span class="hljs-attribute">message_id</span>": <span class="hljs-value"><span class="hljs-number">14</span></span>,
          "<span class="hljs-attribute">media_file</span>": <span class="hljs-value"><span class="hljs-string">"/uploads/medias/0bdf32bf463c0a0ca26920200711202940.jpg"</span></span>,
          "<span class="hljs-attribute">extension</span>": <span class="hljs-value"><span class="hljs-string">"jpg"</span></span>,
          "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-number">1</span>
        </span>}
      ]
    </span>}
  ]
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="message-delete-all-my-chats" class="resource"><h3 class="resource-heading">delete all my chats <a href="#message-delete-all-my-chats" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="message-delete-all-my-chats-delete" class="action delete"><h4 class="action-heading"><div class="name">delete all my chats</div><a href="#message-delete-all-my-chats-delete" class="method delete">DELETE</a><code class="uri">/deleteAllChats</code></h4><h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/deleteAllChats</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    <span class="hljs-string">'status'</span>:<span class="hljs-number">200</span>,
    <span class="hljs-string">'success'</span>:<span class="hljs-string">'All my chats  Deleted Successfully'</span>
}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="message-delete-one-chat" class="resource"><h3 class="resource-heading">delete one chat <a href="#message-delete-one-chat" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="message-delete-one-chat-delete" class="action delete"><h4 class="action-heading"><div class="name">delete one chat</div><a href="#message-delete-one-chat-delete" class="method delete">DELETE</a><code class="uri">/deleteOneChat/{id}</code></h4><h4>Example URI</h4><div class="definition"><span class="method delete">DELETE</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/deleteOneChat/<span class="hljs-attribute" title="id">id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>id</dt><dd><code>integer</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>id of another user</p>
</dd></dl></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
    <span class="hljs-string">'status'</span>:<span class="hljs-number">200</span>,
    <span class="hljs-string">'success'</span>:<span class="hljs-string">'my chat Deleted Successfully'</span>
}</code></pre><div style="height: 1px;"></div></div></div></div></div></section><section id="notification" class="resource-group"><h2 class="group-heading">notification <a href="#notification" class="permalink">&para;</a></h2><p>This group for notification</p>
<div id="notification-get-notification" class="resource"><h3 class="resource-heading">get notification <a href="#notification-get-notification" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="notification-get-notification-get" class="action get"><h4 class="action-heading"><div class="name">get notification</div><a href="#notification-get-notification-get" class="method get">GET</a><code class="uri">/notifications</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/notifications</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">notes</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">current_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
      {
        "<span class="hljs-attribute">unique_id</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
        "<span class="hljs-attribute">data</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"قام العضو somaya hegab بالاجابه على سؤالك"</span></span>,
          "<span class="hljs-attribute">question_id</span>": <span class="hljs-value"><span class="hljs-number">1</span>
        </span>}</span>,
        "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"answerQuestion"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-30 13:51:19"</span></span>,
        "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"قام العضو somaya hegab بالاجابه على سؤالك"</span></span>,
        "<span class="hljs-attribute">time</span>": <span class="hljs-value"><span class="hljs-string">"01:51 pm"</span>
      </span>},
      {
        "<span class="hljs-attribute">unique_id</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
        "<span class="hljs-attribute">data</span>": <span class="hljs-value">{
          "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"قام العضو somaya hegab بالاعجاب باجابتك"</span></span>,
          "<span class="hljs-attribute">answer_id</span>": <span class="hljs-value"><span class="hljs-number">2</span>
        </span>}</span>,
        "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"likeAnswer"</span></span>,
        "<span class="hljs-attribute">created_at</span>": <span class="hljs-value"><span class="hljs-string">"2020-06-29 21:02:57"</span></span>,
        "<span class="hljs-attribute">message</span>": <span class="hljs-value"><span class="hljs-string">"قام العضو somaya hegab بالاعجاب باجابتك"</span></span>,
        "<span class="hljs-attribute">time</span>": <span class="hljs-value"><span class="hljs-string">"09:02 pm"</span>
      </span>}
    ]</span>,
    "<span class="hljs-attribute">first_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/notifications/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1"</span></span>,
    "<span class="hljs-attribute">from</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page</span>": <span class="hljs-value"><span class="hljs-number">1</span></span>,
    "<span class="hljs-attribute">last_page_url</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/notifications/e8d8ff905b214b6ab0a9d08da0706bfd40582395?page=1"</span></span>,
    "<span class="hljs-attribute">next_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">path</span>": <span class="hljs-value"><span class="hljs-string">"http://localhost:8000/api/notifications/e8d8ff905b214b6ab0a9d08da0706bfd40582395"</span></span>,
    "<span class="hljs-attribute">per_page</span>": <span class="hljs-value"><span class="hljs-number">10</span></span>,
    "<span class="hljs-attribute">prev_page_url</span>": <span class="hljs-value"><span class="hljs-literal">null</span></span>,
    "<span class="hljs-attribute">to</span>": <span class="hljs-value"><span class="hljs-number">2</span></span>,
    "<span class="hljs-attribute">total</span>": <span class="hljs-value"><span class="hljs-number">2</span>
  </span>}
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="notification-unread-notification-count" class="resource"><h3 class="resource-heading">unread notification count <a href="#notification-unread-notification-count" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="notification-unread-notification-count-get" class="action get"><h4 class="action-heading"><div class="name">unread notification count</div><a href="#notification-unread-notification-count-get" class="method get">GET</a><code class="uri">/unreadNotifications</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/unreadNotifications</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">count</span>": <span class="hljs-value"><span class="hljs-number">2</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="notification-delete-notification" class="resource"><h3 class="resource-heading">delete notification <a href="#notification-delete-notification" class="permalink">&nbsp;&para;</a></h3><p>send in headers token parameter with logged in user token  value</p>
<div id="notification-delete-notification-get" class="action get"><h4 class="action-heading"><div class="name">delete notification</div><a href="#notification-delete-notification-get" class="method get">GET</a><code class="uri">/deleteNotification</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/deleteNotification</span></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"deleted successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div><div id="notification-delete-one-notification" class="resource"><h3 class="resource-heading">delete one notification <a href="#notification-delete-one-notification" class="permalink">&nbsp;&para;</a></h3><div id="notification-delete-one-notification-get" class="action get"><h4 class="action-heading"><div class="name">delete one notification</div><a href="#notification-delete-one-notification-get" class="method get">GET</a><code class="uri">/deleteOneNotification/{id}</code></h4><h4>Example URI</h4><div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span class="hostname">http://m3lomhapp.com/api</span>/deleteOneNotification/<span class="hljs-attribute" title="id">id</span></span></div><div class="title"><strong>URI Parameters</strong><div class="collapse-button show"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><dl class="inner"><dt>id</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<p>unique id</p>
</dd></dl></div><div class="title"><strong>Response&nbsp;&nbsp;<code>200</code></strong><div class="collapse-button"><span class="close">Hide</span><span class="open">Show</span></div></div><div class="collapse-content"><div class="inner"><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre><div style="height: 1px;"></div><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">status</span>": <span class="hljs-value"><span class="hljs-number">200</span></span>,
  "<span class="hljs-attribute">success</span>": <span class="hljs-value"><span class="hljs-string">"deleted successfully"</span>
</span>}</code></pre><div style="height: 1px;"></div></div></div></div></div></section></div></div></div><p style="text-align: center;" class="text-muted">Generated by&nbsp;<a href="https://github.com/danielgtaylor/aglio" class="aglio">aglio</a>&nbsp;on 22 Jul 2020</p><script>/* eslint-env browser */
/* eslint quotes: [2, "single"] */
'use strict';

/*
  Determine if a string ends with another string.
*/
function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

/*
  Get a list of direct child elements by class name.
*/
function childrenByClass(element, name) {
  var filtered = [];

  for (var i = 0; i < element.children.length; i++) {
    var child = element.children[i];
    var classNames = child.className.split(' ');
    if (classNames.indexOf(name) !== -1) {
      filtered.push(child);
    }
  }

  return filtered;
}

/*
  Get an array [width, height] of the window.
*/
function getWindowDimensions() {
  var w = window,
      d = document,
      e = d.documentElement,
      g = d.body,
      x = w.innerWidth || e.clientWidth || g.clientWidth,
      y = w.innerHeight || e.clientHeight || g.clientHeight;

  return [x, y];
}

/*
  Collapse or show a request/response example.
*/
function toggleCollapseButton(event) {
    var button = event.target.parentNode;
    var content = button.parentNode.nextSibling;
    var inner = content.children[0];

    if (button.className.indexOf('collapse-button') === -1) {
      // Clicked without hitting the right element?
      return;
    }

    if (content.style.maxHeight && content.style.maxHeight !== '0px') {
        // Currently showing, so let's hide it
        button.className = 'collapse-button';
        content.style.maxHeight = '0px';
    } else {
        // Currently hidden, so let's show it
        button.className = 'collapse-button show';
        content.style.maxHeight = inner.offsetHeight + 12 + 'px';
    }
}

function toggleTabButton(event) {
    var i, index;
    var button = event.target;

    // Get index of the current button.
    var buttons = childrenByClass(button.parentNode, 'tab-button');
    for (i = 0; i < buttons.length; i++) {
        if (buttons[i] === button) {
            index = i;
            button.className = 'tab-button active';
        } else {
            buttons[i].className = 'tab-button';
        }
    }

    // Hide other tabs and show this one.
    var tabs = childrenByClass(button.parentNode.parentNode, 'tab');
    for (i = 0; i < tabs.length; i++) {
        if (i === index) {
            tabs[i].style.display = 'block';
        } else {
            tabs[i].style.display = 'none';
        }
    }
}

/*
  Collapse or show a navigation menu. It will not be hidden unless it
  is currently selected or `force` has been passed.
*/
function toggleCollapseNav(event, force) {
    var heading = event.target.parentNode;
    var content = heading.nextSibling;
    var inner = content.children[0];

    if (heading.className.indexOf('heading') === -1) {
      // Clicked without hitting the right element?
      return;
    }

    if (content.style.maxHeight && content.style.maxHeight !== '0px') {
      // Currently showing, so let's hide it, but only if this nav item
      // is already selected. This prevents newly selected items from
      // collapsing in an annoying fashion.
      if (force || window.location.hash && endsWith(event.target.href, window.location.hash)) {
        content.style.maxHeight = '0px';
      }
    } else {
      // Currently hidden, so let's show it
      content.style.maxHeight = inner.offsetHeight + 12 + 'px';
    }
}

/*
  Refresh the page after a live update from the server. This only
  works in live preview mode (using the `--server` parameter).
*/
function refresh(body) {
    document.querySelector('body').className = 'preload';
    document.body.innerHTML = body;

    // Re-initialize the page
    init();
    autoCollapse();

    document.querySelector('body').className = '';
}

/*
  Determine which navigation items should be auto-collapsed to show as many
  as possible on the screen, based on the current window height. This also
  collapses them.
*/
function autoCollapse() {
  var windowHeight = getWindowDimensions()[1];
  var itemsHeight = 64; /* Account for some padding */
  var itemsArray = Array.prototype.slice.call(
    document.querySelectorAll('nav .resource-group .heading'));

  // Get the total height of the navigation items
  itemsArray.forEach(function (item) {
    itemsHeight += item.parentNode.offsetHeight;
  });

  // Should we auto-collapse any nav items? Try to find the smallest item
  // that can be collapsed to show all items on the screen. If not possible,
  // then collapse the largest item and do it again. First, sort the items
  // by height from smallest to largest.
  var sortedItems = itemsArray.sort(function (a, b) {
    return a.parentNode.offsetHeight - b.parentNode.offsetHeight;
  });

  while (sortedItems.length && itemsHeight > windowHeight) {
    for (var i = 0; i < sortedItems.length; i++) {
      // Will collapsing this item help?
      var itemHeight = sortedItems[i].nextSibling.offsetHeight;
      if ((itemsHeight - itemHeight <= windowHeight) || i === sortedItems.length - 1) {
        // It will, so let's collapse it, remove its content height from
        // our total and then remove it from our list of candidates
        // that can be collapsed.
        itemsHeight -= itemHeight;
        toggleCollapseNav({target: sortedItems[i].children[0]}, true);
        sortedItems.splice(i, 1);
        break;
      }
    }
  }
}

/*
  Initialize the interactive functionality of the page.
*/
function init() {
    var i, j;

    // Make collapse buttons clickable
    var buttons = document.querySelectorAll('.collapse-button');
    for (i = 0; i < buttons.length; i++) {
        buttons[i].onclick = toggleCollapseButton;

        // Show by default? Then toggle now.
        if (buttons[i].className.indexOf('show') !== -1) {
            toggleCollapseButton({target: buttons[i].children[0]});
        }
    }

    var responseCodes = document.querySelectorAll('.example-names');
    for (i = 0; i < responseCodes.length; i++) {
        var tabButtons = childrenByClass(responseCodes[i], 'tab-button');
        for (j = 0; j < tabButtons.length; j++) {
            tabButtons[j].onclick = toggleTabButton;

            // Show by default?
            if (j === 0) {
                toggleTabButton({target: tabButtons[j]});
            }
        }
    }

    // Make nav items clickable to collapse/expand their content.
    var navItems = document.querySelectorAll('nav .resource-group .heading');
    for (i = 0; i < navItems.length; i++) {
        navItems[i].onclick = toggleCollapseNav;

        // Show all by default
        toggleCollapseNav({target: navItems[i].children[0]});
    }
}

// Initial call to set up buttons
init();

window.onload = function () {
    autoCollapse();
    // Remove the `preload` class to enable animations
    document.querySelector('body').className = '';
};
</script></body></html>