<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table='messages';
    protected $guarded=[];

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }
    public function medias()
    {
        return $this->hasOne(Media::class, 'message_id');
    }
    public function reply()
    {
        return $this->belongsTo(Message::class, 'reply_id');
    }
}
