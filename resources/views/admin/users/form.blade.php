
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الاسم: </label>
    <div class="col-lg-10{{ $errors->has('fullName') ? ' has-danger' : '' }}">
        {!! Form::text('fullName',old('fullName'),['class'=>'form-control m-input','autofocus','placeholder'=> 'الاسم ' ]) !!}
        @if ($errors->has('fullName'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('fullName') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم المستخدم: </label>
    <div class="col-lg-10{{ $errors->has('userName') ? ' has-danger' : '' }}">
        {!! Form::text('userName',old('userName'),['class'=>'form-control m-input','placeholder'=> 'اسم المستخدم' ]) !!}
        @if ($errors->has('userName'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('userName') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">البريد الالكترونى: </label>
    <div class="col-lg-10{{ $errors->has('email') ? ' has-danger' : '' }}">
        {!! Form::text('email',old('email'),['class'=>'form-control m-input','placeholder'=> 'البريد الالكترونى' ]) !!}
        @if ($errors->has('email'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">كلمة المرور: </label>
    <div class="col-lg-10{{ $errors->has('password') ? ' has-danger' : '' }}">
        {!! Form::password('password',['class'=>'form-control m-input','placeholder'=> 'كلمة المرور' ]) !!}
        @if ($errors->has('password'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">صورة الملف الشخصى: </label>
    <div class="col-lg-10{{ $errors->has('photo') ? ' has-danger' : '' }}">

        <input type="file" name="photo"   class="form-control uploadinput">
        @if ($errors->has('photo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($user) && $user->photo)
<div class="row">




            <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

                <img
                        data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                        alt="First slide [800x4a00]"
                        src="{{$user->photo}}"
                        style="height: 150px; width: 150px"
                        data-holder-rendered="true">
            </div>

</div>
@endif





