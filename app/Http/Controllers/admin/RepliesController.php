<?php

namespace App\Http\Controllers\admin;

use App\Models\Answer;
use App\Models\Reply;
use App\Notifications\answerQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RepliesController extends Controller
{
    public function index($answer_id)
    {
        $answer=Answer::find($answer_id);
        $replies=Reply::where('answer_id',$answer_id)->get();

        return view('admin.replies.index',compact('replies','answer'));
    }
    public function show($answer_id,$id)
    {
        $answer=Answer::find($answer_id);
        $reply=Reply::find($id);

        return view('admin.replies.show',compact('answer','reply'));
    }
    public function destroy($answer_id,$id)
    {
        $reply=Reply::where('id',$id)->first();

        $reply->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));
    }
}
