<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table='questions';
    protected $guarded=[];
    public function answers()

    {
        return $this->hasMany(Answer::class, 'question_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function cities(){
        return $this->belongsToMany(City::class,'question_cities','question_id','city_id');
    }
}
