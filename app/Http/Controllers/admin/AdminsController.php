<?php

namespace App\Http\Controllers\admin;


use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins=User::where('role',1)->withTrashed()->get();

        return view('admin.admins.index',compact('admins'));
    }
    public function admin($id){
        $admin=User::withTrashed()->where('id',$id)->first();
        if ($admin->role==0)
        {
            $admin->role = 1;
            $admin->save();







            return redirect('/webadmin/admins')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم جعله مدير بنجاح']));
        }else{
            $admin->role = 0;
            $admin->save();
            return redirect('webadmin/users')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم جعله غضوا عاديا بنجاح']));


        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admins.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'fullName' => 'required',
            'userName' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);
        $admin=User::create([
            'fullName' => $request->fullName,
            'userName' => $request->userName,
            'email' => $request->email,
            'role' => 1,
            'token'=>bin2hex(random_bytes(20)),
            'password' => Hash::make($request->password),
        ]);
        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = str_random(10).'.'.$request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $admin->photo ='/uploads/profiles/'. $imageName;
            $admin->save();

        }


        return redirect('/webadmin/admins')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة المدير بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin=User::withTrashed()->where('id',$id)->first();
        return view('admin.admins.show',compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin=User::withTrashed()->where('id',$id)->first();
        return view('admin.admins.edit',compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'fullName' => 'required',
            'userName' => 'required',

        ]);
        $admin=User::withTrashed()->where('id',$id)->first();
        $admin->update([
            'fullName'=>$request->fullName,
            'userName'=>$request->userName,
        ]);
        if ($request->email !=$admin->email)
        {
            $this->validate($request, [
                'email' => 'email|unique:users',
            ]);
            $admin->update([
                'email'=>$request->email
            ]);

        }
        if ($request->password !=''){
            $admin->update([
                'password'=>Hash::make($request->password),
            ]);
        }
        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = str_random(10).'.'.$request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $admin->photo ='/uploads/profiles/'. $imageName;
            $admin->save();

        }

        return redirect('/webadmin/admins')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم التعديل بنجاح']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin=User::where('id',$id)->first();

        $admin->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الجذف بنجاح']));
    }
}
