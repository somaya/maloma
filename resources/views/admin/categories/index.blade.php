@extends('admin.layouts.app')
@section('title')
   الاقسام
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">Home</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/categories')}}" class="m-menu__link">
            <span class="m-menu__link-text">الاقسام</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        الاقسام
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div><a href="{{route('categories.create')}}" style="margin-bottom:20px;background-color: #00a3b6" class="btn btn_primary " ><i class=" fa fa-edit"></i>اضافة قسم</a></div>




            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable search_result" id="m_table_testArea">

                <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الصورة</th>
                    <th>الادوات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $index=> $category)

                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$category->name}} </td>
                        <td><img src="{{$category->photo}}" width="90px;" height="90px;"> </td>


                        <td>


                            <a  title="تعديل" href="/webadmin/categories/{{$category->id}}/edit" ><i class="fa fa-edit"></i></a>
                            <form class="inline-form-style"
                                  action="/webadmin/categories/{{ $category->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete" />
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/testArea/script.js') !!}--}}

@endsection
