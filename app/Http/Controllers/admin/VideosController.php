<?php

namespace App\Http\Controllers\admin;

use App\Models\Video;
use App\Models\Video_code;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos=Video::all();
        return view('admin.videos.index',compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.videos.add');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([


            'video' => 'required|file|mimes:mp4,mov,wmv,avi,mpv',

        ]);
        $video = $request->file('video');
            $videoName= str_random(10).'.'.$video->getClientOriginalExtension();
            $video->move(
                base_path() . '/public/uploads/videos/', $videoName
            );

        Video::create([
            'video'=>'/uploads/videos/'.$videoName
        ]);
        return redirect('/webadmin/videos')->withFlashMessage(json_encode(['success'=>true,'msg'=>'Video Added Successfully']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $video=Video::find($id);
        $code=Carbon::now()->timestamp;
        Video_code::create([
            'video_id'=>$id,
            'code'=>$code
        ]);
        return view('admin.videos.show',compact('video','code'));
    }
    public function codes($id)
    {
        $codes=Video_code::where('video_id',$id)->get();
        $video=Video::find($id);
        return view('admin.videos.codes',compact('codes','video'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Video::destroy($id);
        return redirect('/webadmin/videos')->withFlashMessage(json_encode(['success'=>true,'msg'=>'Video Deleted Successfully']));

    }
}
