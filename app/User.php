<?php

namespace App;


use App\Models\Answer;
use App\Models\City;
use App\Models\Country;
use App\Models\educationDegree;
use App\Models\Message;
use App\Models\Question;
use App\Models\Reply;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'name', 'email', 'password','role','phone','photo'
//    ];
    protected $guarded=[];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
    public function city()

    {
        return $this->belongsTo(City::class, 'city_id');
    }
    public function educationDegree()

    {
        return $this->belongsTo(educationDegree::class, 'educationDegree_id');
    }
    public function questions()

    {
        return $this->hasMany(Question::class, 'user_id');
    }
    public function answers()

    {
        return $this->hasMany(Answer::class, 'user_id');
    }
    public function replies()

    {
        return $this->hasMany(Reply::class, 'user_id');
    }
    public function receivedMessages()
    {
        return $this->hasMany(Message::class, 'receiver_id');
    }
}
