<?php

namespace App\Http\Controllers\admin;

use App\Models\educationDegree;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DegreeController extends Controller
{
    public function index()
    {
        $degrees = educationDegree::all();
        return view('admin.degrees.index', compact('degrees'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.degrees.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
//                'name_en' => 'required',
            ]);

        educationDegree::create([
            'name' => $request->name,

        ]);

        return redirect('/webadmin/degrees')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم اضافة الدرجة بنجاح']));



    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $degree = educationDegree::find($id);
        return view('admin.degrees.edit', compact( 'degree'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $degree=educationDegree::find($id);
        $data=$request->all();
        $degree->update($data);
        return redirect('/webadmin/degrees')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل الدرجة بنجاح']));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        educationDegree::destroy($id);
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));
    }
}
