<?php

namespace App\Http\Controllers\admin;

use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionsController extends Controller
{
    public function index()
    {
        $questions=Question::all();

        return view('admin.questions.index',compact('questions'));
    }
    public function show($id)
    {
        $question=Question::where('id',$id)->first();
        return view('admin.questions.show',compact('question'));
    }
    public function destroy($id)
    {
        $question=Question::where('id',$id)->first();

        $question->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));
    }
}
