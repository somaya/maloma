@extends('admin.layouts.app')

@section('title')
     تفاصيل الرد
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">Home</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/answers'.$answer->id.'/replies')}}" class="m-menu__link">
            <span class="m-menu__link-text">الاجوبه</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تفاصيل الرد</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                       تفاصيل الرد
                    </h3>
                </div>
            </div>
        </div>
        <form method="get" action="/webadmin/answer/{{$answer->id}}/replies/{{$reply->id}}" class="m-form m-form--fit m-form--label-align-right">
        <!--begin::Form-->
{{--        {!! Form::model($answer,['route' => ['answers.show' , $answer->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}--}}
        <div class="m-portlet__body">


            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">الرد: </label>
                <div class="col-lg-10{{ $errors->has('f_name') ? ' has-danger' : '' }}">
{{--                    {!! Form::text('fullName',old('fullName'),['class'=>'form-control m-input','autofocus','disabled','placeholder'=> 'الاسم' ]) !!}--}}
                    <textarea class="form-control m-input" disabled >{{$reply->replyText}}</textarea>

                </div>

            </div>
            {{--@if($answer->photo)--}}
            {{--<div class="form-group m-form__group row">--}}
                {{--<label class="col-lg-2 col-form-label">الصوره: </label>--}}
                {{--<div class="col-lg-10{{ $errors->has('f_name') ? ' has-danger' : '' }}">--}}
                    {{--<img src="{{$answer->photo}}" width="150px" height="150px">--}}

                {{--</div>--}}

            {{--</div>--}}
            {{--@endif--}}







        </div>
    </form>

{{--    {!! Form::close() !!}--}}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

