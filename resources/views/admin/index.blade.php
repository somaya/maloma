@extends('admin.layouts.app')
@section('title')
    Dashboard
@endsection

@section('header')
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">

            <!-- Traffic sources -->
            <div class="panel panel-flat home-custom-styles">
                <div class="panel-heading">
                    <!-- <h6 class="panel-title">Traffic sources</h6> -->

                </div>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-6 block-styles">
                            <div class="all">
                                <ul class="list-inline text-center">
                                    <li>
                                        <i class="icon-user-check"></i>
                                    </li>
                                    <li class="text-center" >
                                        <div class="text-semibold" style="background-color: #00a3b6">المدراء</div>
                                        <div class="text-muted">{{$admins}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 block-styles">
                            <div class="all">
                                <ul class="list-inline text-center">
                                    <li>
                                        <i class="icon-user-check"></i>
                                    </li>
                                    <li class="text-center">
                                        <div class="text-semibold" style="background-color: #00a3b6">الاعضاء</div>
                                        <div class="text-muted">{{$users}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>


                        <div class="col-lg-3 col-md-4 col-6 block-styles">
                            <div class="all">
                                <ul class="list-inline text-center">
                                    <li>
                                        <i class="icon-user-check"></i>
                                    </li>
                                    <li class="text-center">
                                        <div class="text-semibold" style="background-color: #00a3b6">الاقسام</div>
                                        <div class="text-muted">{{$categories}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4 col-6 block-styles">
                            <div class="all">
                                <ul class="list-inline text-center">
                                    <li>
                                        <i class="icon-user-check"></i>
                                    </li>
                                    <li class="text-center">
                                        <div class="text-semibold" style="background-color: #00a3b6" >الاسئلة</div>
                                        <div class="text-muted">{{$questions}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4 col-6 block-styles">
                            <div class="all">
                                <ul class="list-inline text-center">
                                    <li>
                                        <i class="icon-user-check"></i>
                                    </li>
                                    <li class="text-center">
                                        <div class="text-semibold"style="background-color: #00a3b6" >الاجوبة</div>
                                        <div class="text-muted">{{$answers}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 block-styles">
                            <div class="all">
                                <ul class="list-inline text-center">
                                    <li>
                                        <i class="icon-user-check"></i>
                                    </li>
                                    <li class="text-center">
                                        <div class="text-semibold"style="background-color: #00a3b6">البلاغات</div>
                                        <div class="text-muted">{{$reports}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- /traffic sources -->

        </div>

        <div class="col-lg-5">


        </div>
    </div>
@endsection
@section('footer')
@endsection
