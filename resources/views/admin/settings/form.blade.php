
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> عن التطبيق: </label>
    <div class="col-lg-10{{ $errors->has('aboutUs') ? ' has-danger' : '' }}">
        <textarea name="aboutUs" class="form-control summernote " required>{{isset($setting) ? $setting->aboutUs :old('aboutUs')}}</textarea>

        @if ($errors->has('aboutUs'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('aboutUs') }}</strong>
            </span>
        @endif
    </div>

</div>
