@extends('admin.layouts.app')

@section('title')
     تفاصيل البلاغ
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">Home</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/reports')}}" class="m-menu__link">
            <span class="m-menu__link-text">البلاغات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تفاصيل البلاغ</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                       تفاصيل البلاغ
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($report,['route' => ['reports.show' , $report->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">


            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">البلاغ: </label>
                <div class="col-lg-10{{ $errors->has('f_name') ? ' has-danger' : '' }}">
{{--                    {!! Form::text('fullName',old('fullName'),['class'=>'form-control m-input','autofocus','disabled','placeholder'=> 'الاسم' ]) !!}--}}
                    <textarea class="form-control m-input" disabled >{{$report->message}}</textarea>

                </div>

            </div>







        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

